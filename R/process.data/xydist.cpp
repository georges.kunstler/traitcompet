// compile with R CMD SHLIB xydist.cpp
#include <Rcpp.h>
#include <vector>

std::vector<int> within_neighbourhood(int idx,
					 std::vector<double> x,
					 std::vector<double> y,
					 double r);
std::vector<double> 
areas_by_species_within_neighbourhood(int idx,
				      std::vector<double> x,
				      std::vector<double> y,
				      double r,
				      std::vector<double> d,
				      std::vector<int> type,
				      int n_types);
bool cmp(double x0, double y0, double x1, double y1, double r2);
double square(double x);


// NOTE: idx is in C (base 0) indexing, not R (base 1) indexing.
// [[Rcpp::export]]
std::vector<int> within_neighbourhood(int idx,
					 std::vector<double> x,
					 std::vector<double> y,
					 double r) {
  std::vector<int> ret;
  std::vector<double>::iterator xi = x.begin(), yi = y.begin();
  double r2 = square(r);
  const double x0 = x[idx], y0 = y[idx];
  for (int i = 0; i < x.size(); i++, xi++, yi++)
    if (cmp(x0, y0, *xi, *yi, r2) && i != idx)
      ret.push_back(i);
  return ret;
}

// [[Rcpp::export]]
std::vector<double> areas_by_species(std::vector<int> idx,
				     std::vector<double> d,
				     std::vector<int> type,
				     int n_types) {
  std::vector<double> ret(n_types);
  for (std::vector<int>::const_iterator i = idx.begin();
       i != idx.end(); i++) {
    ret[type[*i]] += d[*i];
  }
  return ret;
}

// [[Rcpp::export]]
std::vector<double> 
areas_by_species_within_neighbourhood(int idx,
				      std::vector<double> x,
				      std::vector<double> y,
				      double r,
				      std::vector<double> d,
				      std::vector<int> type,
				      int n_types) {
  return areas_by_species(within_neighbourhood(idx, x, y, r),
			  d, type, n_types);
}

bool cmp(double x0, double y0, double x1, double y1, double r2) {
  return square(x0 - x1) + square(y0 - y1) < r2;
}

double square(double x) {
  return x*x;
}
