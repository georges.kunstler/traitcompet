################################
## function for test.tree.CWM
## source("R/utils/plot.R")



load.processed.data <- function(path, file.name = "data.tree.tot.no.std.csv"){
require(data.table)
    fname <- file.path(path, file.name )
    if(file.exists(fname)){
        cat('loading file', path, file.name,  "\n")
        data <- fread(fname, stringsAsFactors = FALSE)
        print(warnings())
      return(data)
  }else{return(NULL)}
}

load_all_processed_data <- function(set, pathdir){
  data <- list()
  ecocodes <- list_all_processed_data(set, pathdir)
     for (ecocode.select in ecocodes) {
    data[[ecocode.select]] <- load.processed.data(file.path(pathdir, set,
                                                            ecocode.select))
     }
  data
}

list_all_processed_data <- function(set, pathdir){
sub(paste(pathdir, "/", set, "/", sep = ""), "",
    list.dirs(paste(pathdir, "/", set, sep = "")))[-1]
}



## test range of data
fun.test.one.var.q <- function(t, t.q.01, t.q.9){
qq <- quantile(t, probs = c(0.1, 0.9), na.rm = TRUE)
res <- (qq[1]<t.q.01) |(qq[2] >t.q.9)
return(res)
}

fun.test.one.var.data.q <- function(i, data, var.name, t.q.01.vec, t.q.9.vec){
fun.test.one.var.q(data[[var.name[i]]], t.q.01.vec[i], t.q.9.vec[i])
}


# test range D G BA.G
fun.test.range.vars.q <- function(data.tree, set){
vars.name <-  c( "D", "dead", "G", "BA.G")
t.q.01.vec <- c(9.9, 0, -2, -80)
t.q.9.vec <- c(200, 1, 10, 100)
vec.test <- sapply(1:4, fun.test.one.var.data.q, data.tree,
                   vars.name, t.q.01.vec, t.q.9.vec)
if(any(vec.test))
  stop(paste("Var",
        paste(vars.name[vec.test], collapse = " ") ,
       "quantile 0.1 or 0.9 out of range for set", set))
}




############################
########## TEST FUNCTIONS
#################################

##### FOR PLOT TYPE DATA


## test functions for Wood density for one indiv
fun.test.CWM.plot.r <- function(data, data.TRAITS, data.tree){
rep.count <-  0
    repeat{

      res <- fun.test.CWM.plot(data.t = data, data.TRAITS, data.tree)
      rep.count <-  rep.count +1
      if(!is.na(res)){
        break
      }
      if(rep.count>1000) stop('error no wood density data')
    }
return(res)
}

## fun compute CWM for test
fun.compute.CWM.test.plot <- function(samp.id, samp.plot, data, data.TRAITS){
    BA.a <- BA.fun(data[["D"]][data[["obs.id"]]!=samp.id &
                                  data[["plot"]] ==samp.plot] ,
                   weights =data[["weights"]][data[["obs.id"]]!=samp.id &
                                  data[["plot"]] ==samp.plot])
    sp.a.f <- factor(data[["sp"]])[data[["obs.id"]]!=samp.id &
                                  data[["plot"]] ==samp.plot]
    sp.id <- factor(data[["sp"]])[data[["obs.id"]]==samp.id]
    BA.n <- tapply(BA.a, INDEX = sp.a.f, FUN = sum, na.rm = TRUE)
    BA.n.no0 <- BA.n[!is.na(BA.n)]
    sp.n <-  as.character(names(BA.n.no0))
    mat.t <- fun.trait.format(trait = "Wood.density",
                              traits.data = data.TRAITS,
                              vec.sp = sp.n)
    mat.t.id <- fun.trait.format(trait = "Wood.density",
                              traits.data = data.TRAITS,
                              vec.sp = sp.id)
    res.fill <- sum(BA.n.no0*abs(mat.t.id[,3] - mat.t[, 3]))/sum(BA.n.no0)
return(res.fill)
}

sample.safe <- function(x, ...){
if (length(x)==1){
    res <- sample(c(x, x), ...)
}else{
res <- sample(x, ...)
 }
return(res)
}


# test one random plot
fun.test.CWM.plot <- function(data.t, data.TRAITS, data.tree) {
     res <- NA
     # randomly select a census
     data <- subset(data.t,
                    subset = data.t[["census"]] ==
                             sample.safe(unique(data.t[["census"]]), 1))
     data.tree.t <- subset(data.tree,
                           subset = data.tree[["census"]] ==
                                    unique(data[["census"]]))
     # randomly select an individual
     samp.id <- sample.safe(data[["obs.id"]], 1)
     samp.sp <-  data[["sp"]][data[["obs.id"]] == samp.id]
     samp.plot <-  data[["plot"]][data[["obs.id"]] == samp.id]
     if(!is.na(data.TRAITS[data.TRAITS[["sp"]] == samp.sp, "Wood.density.mean"]) &
          !data.TRAITS[data.TRAITS[["sp"]] == samp.sp, "Wood.density.genus"] &
          !is.na(data[["Wood.density.CWM.fill"]][data[["obs.id"]] == samp.id])) {
          test.focal <- all.equal(data.TRAITS[data.TRAITS[["sp"]] == samp.sp,
                                              "Wood.density.mean"],
                                 data[["Wood.density.focal"]][data[["obs.id"]]
                                                              == samp.id])
          res.fill <- fun.compute.CWM.test.plot(samp.id, samp.plot, data.tree.t,
                                                data.TRAITS)
          test.cwm.fill <- all.equal(res.fill,
                                     data[["Wood.density.abs.CWM.fill"]][
                                                     data[["obs.id"]]==samp.id])
          res <- all(c(test.focal, test.cwm.fill) == TRUE)
          if (is.na(res.fill)) res <- NA
          }
     return(res)
     }
#################################
## test function XY type data set



fun.test.CWM.XY.r <- function(data, data.TRAITS, data.tree, Rlim){
rep.count <-  0
    repeat{
      res <- fun.test.CWM.XY(data.r = data, data.TRAITS, data.tree, Rlim = Rlim)
      rep.count <-  rep.count +1
      if(!is.na(res)){
        break
      }
     if(rep.count>1000) stop('error no Wood density data')
    }
return(res)
}

 fun.compute.test <- function(obs.id.t, obs.id, xy.table, diam, sp, Rlim) {
        # compute distance to the focal tree
        dist.t <- as.vector(((outer(xy.table[obs.id == obs.id.t, "x"],
                                        xy.table[,"x"], FUN = "-"))^2
                                 + (outer(xy.table[obs.id == obs.id.t, "y"],
                                          xy.table[,"y"], FUN = "-"))^2))
        select <- (dist.t < Rlim^2) & obs.id !=obs.id.t
        ### compute BA in radius Rlim
        res.BA.t <- tapply(BA.fun(diam[select], weights = 1/(pi * Rlim^2)),
                           INDEX = sp[select],
            FUN = sum, na.rm = TRUE)
        res.BA.t[is.na(res.BA.t)] <- 0
        names.sp <- names(res.BA.t)
        res.BA.t <- as.vector(res.BA.t)
        names(res.BA.t) <- names.sp
        return((res.BA.t))
    }

fun.compute.CWM.trait.test <-  function(samp.id, data, Rlim, data.TRAITS){

    BA.n <- fun.compute.test(obs.id.t = samp.id, obs.id = data[["obs.id"]],
                             xy.table = as.matrix(subset(data,
                                                  select = c("x", "y"))),
                   diam = data[["D"]], sp = data[["sp"]], Rlim = Rlim)
    sp.n <- as.character(names(BA.n)[BA.n>0])
    sp.id <- as.character(data[["sp"]][data[["obs.id"]] == samp.id])
    BA.n <- BA.n[BA.n>0]
    mat.t <- fun.trait.format(trait = "Wood.density",
                              traits.data = data.TRAITS,
                              vec.sp = sp.n)
    mat.t.id <- fun.trait.format(trait = "Wood.density",
                              traits.data = data.TRAITS,
                              vec.sp = sp.id)
    res.fill <- sum(BA.n * abs(mat.t.id[,3] - mat.t[, 3]))/sum(BA.n)
return(res.fill)
}


###################

fun.test.CWM.XY <- function(data.r, data.TRAITS, data.tree, Rlim){
    res <- NA
data.t <- subset(data.r,
                 subset = data.r[["cluster"]] ==
                          sample.safe(unique(data.r[["cluster"]]), 1))
data.tree.t <- subset(data.tree,
                      subset = data.tree[["cluster"]]
                               == unique(data.t[["cluster"]]))
# randomly select a census
data <- subset(data.t,
               subset = data.t[["census"]] ==
                        sample.safe(unique(data.t[["census"]]), 1))
data.tree.d <- subset(data.tree.t,
                      subset = data.tree.t[["census"]] ==
                               unique(data[["census"]]))

# randomly select an individual
samp.id <- sample.safe(data[["obs.id"]], 1)
samp.sp <- data[["sp"]][data[["obs.id"]]==samp.id]
if(!is.na(data.TRAITS[data.TRAITS[["sp"]]==samp.sp, "Wood.density.mean"]) &
   !data.TRAITS[data.TRAITS[["sp"]]==samp.sp, "Wood.density.genus"] &
   !is.na(data[["Wood.density.CWM.fill"]][data[["obs.id"]]==samp.id])){
    test.focal <- all.equal(data.TRAITS[data.TRAITS[["sp"]]==samp.sp,
                                        "Wood.density.mean"] ,
                            data[["Wood.density.focal"]][data[["obs.id"]]==samp.id])
    res.fill <- fun.compute.CWM.trait.test(samp.id, data.tree.d,
                                           Rlim, data.TRAITS)
    test.cwm.fill <- all.equal(res.fill,
                               data[["Wood.density.abs.CWM.fill"]][data[["obs.id"]]==samp.id])
    res <- all(c(test.focal, test.cwm.fill)==TRUE)
    if(is.na(res.fill)) res <- NA
 }
 return(res)
}


### FUNCTION TO TEST IF CWM VALUE OK
fun.test.value.one.ecoregion.I <- function(data.CWM, set, ecocode.select,
                                           path.formatted = "output/formatted" ){
    require(dplyr)
    data.tree <- read.csv(file.path(path.formatted,  set,  "tree.csv"),
                          stringsAsFactors = FALSE)
    data.traits <- read.csv(file.path(path.formatted,  set,  "traits.csv"),
                            stringsAsFactors = FALSE)
    # remove nas
    data.tree <- filter(data.tree, !is.na(D))
    data.tree <- filter(data.tree, ecocode==ecocode.select)
    # test if same dim
    if (nrow(data.CWM) !=length(data.tree[["obs.id"]]))
        stop(paste("data.CWM not good dim", nrow(data.CWM), 'vs',length(data.tree[["obs.id"]])) )
    ### test CWM for Wood.density
    for (i in 1:20){
        if (!fun.test.CWM.plot.r(data.CWM, data.TRAITS = data.traits,
                                 data.tree = data.tree))
        stop(paste("CWM index for Wood.density WRONG for", set))
    }
}

fun.test.value.one.ecoregion.B <- function(data.CWM, set, ecocode.select,
                                           path.formatted = "output/formatted" ){
    data.tree <- read.csv(file.path(path.formatted,  set,  "tree.csv"),
                          stringsAsFactors = FALSE)
    data.traits <- read.csv(file.path(path.formatted,  set,  "traits.csv"),
                            stringsAsFactors = FALSE)
    # remove nas
    data.tree <- subset(data.tree, subset = !is.na(data.tree[["D"]]))
    data.tree <- subset(data.tree, subset = data.tree$ecocode==ecocode.select)
    ### test CWM for Wood.density
   for (i in 1:20){ test.res <- fun.test.CWM.XY.r(data.CWM,
                                                  data.TRAITS = data.traits,
                                                  data.tree = data.tree,
                                                  Rlim = 15)
    if (!test.res) stop(paste("CWM index for Wood.density WRONG for", set))
                }
}


#### compute description of each table

fun.perc.sup.treshold.sp <- function(i, var.name, var.names.perc, data, treshold){
return(  sum(data[[var.names.perc[i]]]>treshold &
     !is.na(data[[var.names.perc[i]]]) &
     !is.na(data[[var.name[i]]]))/length(data[[var.names.perc[i]]]))
}


fun_div <- function(sp){
p_i <- table(factor(sp))/sum(table(factor(sp)))
shannon <- -sum(p_i*log(p_i))
simpson <- 1-sum(p_i^2)
return(data.frame(shannon = shannon, simpson = simpson))
}

fun.compute.percentage.species.genus <- function(data, treshold = 0.8){
perc.gymno <- sum( data[['Phylo.group']]=='Gymnosperm' &
                 !is.na(data[['Phylo.group']]))/sum(!is.na(data[['Phylo.group']]))
perc.ev <- sum(data[['Pheno.T']]=='EV'
               &!is.na(data[['Pheno.T']]))/sum(!is.na(data[['Pheno.T']]))
traits.name <- c("Leaf.N", "Seed.mass", "SLA", "Wood.density", "Max.height")
traits.focal <- paste(c("Leaf.N", "Seed.mass", "SLA",
                        "Wood.density", "Max.height"), "focal", sep = ".")

var.name.fill <- paste(traits.name, "abs.CWM.fill", sep = ".")
var.name.perc.genus <- paste(traits.name, "perc.genus", sep = ".")
var.name.perc.species <- paste(traits.name, "perc.species", sep = ".")
perc.T.non.missing.sp <- lapply(1:length(var.name.fill),
                                fun.perc.sup.treshold.sp, var.name.fill,
                             var.name.perc.species, data, treshold = treshold)
perc.T.non.missing.genus <- lapply(1:length(var.name.fill),
                                   fun.perc.sup.treshold.sp, var.name.fill,
                                var.name.perc.genus, data, treshold = treshold)
mean.trait <- lapply(traits.focal, function(x, data) c(x = mean(data[[x]],
                                                           na.rm = TRUE)),
                     data)

perc.genus <- lapply(var.name.perc.genus,
                     function(i, data) mean(data[[i]], na.rm = TRUE),
                     data)
perc.species <- lapply(var.name.perc.species,
                       function(i, data) mean(data[[i]], na.rm = TRUE),
                       data)
BATOT.m <- mean(data$BATOT, na.rm = TRUE)
## compute the shannon & simpson index

div.vec <- apply(do.call("rbind", tapply(data$sp, INDEX = data$plot, fun_div)),
      MARGIN = 2, FUN = mean, na.rm = TRUE)

vec.res <- c(nrow(data),
             BATOT.m,
             div.vec,
             perc.gymno,
             perc.ev,
             perc.T.non.missing.sp,
             perc.T.non.missing.genus,
             perc.genus, perc.species,
             mean.trait)
names(vec.res) <- c('num.obs', 'BATOT.m', 'shannon', 'simpson',
                    'perc.gymno', 'perc.ev',
                    paste(traits.name, "perc.above", treshold,
                          "sp", sep = "."),
                    paste(traits.name, "perc.above", treshold,
                          "genus", sep = "."),
                    var.name.perc.genus, var.name.perc.species,
                    traits.focal)
return(vec.res)
}


fun.test.set.ecocode.tree.CWM.I <- function(data, set, ecocode.select){
fun.test.value.one.ecoregion.I(data.CWM = data, set, ecocode.select)
fun.test.range.vars.q(data = data, set)
cat(set, ecocode.select, "OK \n")
}

fun.test.set.tree.CWM.I <- function(set, filedir){
  ecocodes <- list_all_processed_data(set, filedir)
  print(ecocodes)
  list.perc <- list()
     for (ecocode.select in ecocodes) {
    data.temp <- load.processed.data(file.path(filedir,  set, ecocode.select))
    if(!is.data.frame(data.temp)) next;
    fun.test.set.ecocode.tree.CWM.I(data.temp, set, ecocode.select)
    list.perc[[ecocode.select]] <- fun.compute.percentage.species.genus(data.temp)
    fun.test.range.vars.q(data.temp, paste(set, ecocode.select))
     }
  mat.perc <- do.call("rbind", list.perc)
  df.perc <-  data.frame(set = rep(set, nrow(mat.perc)),
                       ecocode = rownames(mat.perc),
                       mat.perc, stringsAsFactors  =  FALSE)
return(df.perc)
cat(set, "OK \n")
}

fun.test.set.ecocode.tree.CWM.B <- function(data, set, ecocode.select){
fun.test.value.one.ecoregion.B(data.CWM = data, set, ecocode.select)
fun.test.range.vars.q(data = data, set)
cat(set, ecocode.select, "OK \n")
}


fun.test.set.tree.CWM.B <- function(set, filedir){
  ecocodes <- list_all_processed_data(set, filedir)
  list.perc <- list()
     for (ecocode.select in ecocodes) {
    data.temp <- load.processed.data(file.path(filedir,  set, ecocode.select))
    if(!is.data.frame(data.temp)) next;
    fun.test.set.ecocode.tree.CWM.B(data.temp, set, ecocode.select)
    list.perc[[ecocode.select]] <- fun.compute.percentage.species.genus(data.temp)
    fun.test.range.vars.q(data.temp, paste(set, ecocode.select))
     }
mat.perc <- do.call("rbind", list.perc)
df.perc <-  data.frame(set = rep(set, nrow(mat.perc)),
                       ecocode = rownames(mat.perc), mat.perc)
return(df.perc)
cat(set, "OK \n")
}

## FUNCTION TO LOAD ALL SET IN ONE BIG DATA SET "data.tree.tot.no.std.csv"
fun.load.set.in.big.file <- function(set, filedir, type,
                                     file.to.load  = "data.tree.tot.no.std.csv"){
 ecocodes <- list_all_processed_data(set, filedir)
 print(set)
 # load first ecoregion
  ecocode.select <- ecocodes[1]
  data.temp <- load.processed.data(file.path(filedir,  set, ecocode.select),
                                   file.to.load)
 data.all <- data.frame(set = rep(set, nrow(data.temp)),
                        data.temp)
 ## other
 if (length(ecocodes)>1){
     for (ecocode.select in ecocodes[-1]) {
        data.temp <- load.processed.data(file.path(filedir,  set, ecocode.select),
                                         file.to.load)
        data.temp <- data.frame(set = rep(set, nrow(data.temp)),
                        data.temp)
        data.all <- rbind(data.all, data.temp)
       }
 }


 if (type=='B'){
   data.all <- data.all[, !names(data.all) %in% c( "x" ,    "y" )]
  }
if (type=='I'){
  data.all <- data.all[, !names(data.all) %in% "weights"]
  }
return(data.all)
}


#### PLOT VAR PER SET

## plot hist of each var
fun.hist.var <- function(data, var, ...){
tryCatch(hist(data[[var]], xlab = var, main = unique(data[['set']]), ...),
         warning = function(w) print('warnings'),
         error = function(e){print(paste(unique(data[['set']]),
             'do not have value for', var));
                           plot(0, 0, main = unique(data[['set']]))})
 }

fun.hist.var.set <- function(data, var, ...){
par(mfrow = c(5, 3), mar = c(4.5, 3.5, 0.5, 0.5), mgp = c(1.5, 0.5, 0))
by(data, INDICES = data[['set']], FUN = fun.hist.var, var = var,
   xlim = range(data[[var]], na.rm = TRUE), ...)
}

fun.plot.xy <- function(data, var.x, var.y, ...){
tryCatch(plot(data[[var.x]], data[[var.y]], xlab = var.x, ylab = var.y,
              main = unique(data[['set']]), ...),
         warning = function(w) print('warnings'),
         error = function(e){print(paste(unique(data[['set']]),
             'do not have value for', var.x, 'or', var.y));
                           plot(0, 0, main = unique(data[['set']]))})

 }


fun.plot.xy.set <- function(data, var.x, var.y, ...){
       par(mfrow = c(5, 3), mar = c(4.5, 3.5, 0.5, 0.5), mgp = c(1.5, 0.5, 0))
       by(data, INDICES = data[['set']], FUN = fun.plot.xy,
          var.x = var.x, var.y = var.y,
          xlim = range(data[[var.x]], na.rm = TRUE),
          ylim = range(data[[var.y]], na.rm = TRUE), ...)
      }


fun.plot.hist.trait.per.set <- function(data){
trait.name <- c("Leaf.N", "Seed.mass", "SLA", "Wood.density", "Max.height")
name.var <- c("focal",
              "CWM.fill",
              "abs.CWM.fill")
   for (i in name.var){
      for (t in trait.name){
       var.temp <- paste(t, i, sep = ".")
       to.pdf(fun.hist.var.set(data, var = var.temp),
              paste("figs/test.processed/fig", t, i, "pdf", sep = "."))
      }
    }
}




#####
## function compute FD per plot
## TODO
fun.apply.fun.by.cluster <- function(DT, expr){
e <- substitute(expr)
DT[, eval(e), by = cluster.id]
}


##
fun.compute.sd.var.cluster <- function(DT, var){
cluster.unique.id <- paste(data[['set']], data[['ecocode']], data[['cluster']])

DT[, list(mean = mean(v, na.rm = TRUE), sd = sd(v, na.rm = TRUE)),
   by = cluster.id]
return(tapply(data[[var]], INDEX = cluster.unique.id, FUN = sd, na.rm = TRUE))
}

##
fun.compute.mean.var.cluster <- function(data, var){
cluster.unique.id <- paste(data[['set']], data[['ecocode']], data[['cluster']])
return(tapply(data[[var]], INDEX = cluster.unique.id, FUN = mean, na.rm = TRUE))
}

##
fun.compute.perc.var.cluster <- function(data, var){
cluster.unique.id <- paste(data[['set']], data[['ecocode']], data[['cluster']])
return(tapply(data[[var]], INDEX = cluster.unique.id,
              FUN = function(x) sum(x, na.rm = TRUE)/length(x[!is.na(x)])))
}

##
fun.compute.numSP.var.cluster <- function(data){
cluster.unique.id <- paste(data[['set']], data[['ecocode']], data[['cluster']])
data$sp2 <- paste(data$set, data$sp, sep = '.')
return(tapply(data[['sp.2']], INDEX = cluster.unique.id,
              FUN = function(x) nlevels(factor(x))))
}

##
fun.apply.fun.by.cluster <- function(DT, expr){
e <- substitute(expression(expr))
DT[, eval(e), by = cluster.id]
}


fun_div1 <- function(sp.id){
    fun_div(sp.id)[1, 1]
}
fun_div2 <- function(sp.id){
    fun_div(sp.id)[1, 2]
}

fun.perc <- function(x, value){
sum(x==value)/sum(!is.na(x))
}

first2 <-  function(x){
    x[1]
}

fun.compute.all.var.cluster <- function(data){
require(dplyr)
data$plot.id <- paste(data[['plot']], data[['census']])
data$sp.id <- paste( data[['sp']])
data$set <- as.character(data$set)
data$biomes <- as.character(data$biomes)
data$ecocode <- as.character(data$ecocode)
DF <- tbl_df(data)
by_plot<- group_by(DF,  plot.id)
summarise_by_cluster<- summarise(by_plot,
  count  =  length(sp.id),
  n_sp = n_distinct(sp.id),
  shannon = fun_div1(sp.id),
  simpson = fun_div2(sp.id),
  set = first(set),
  ecocode = first(ecocode),
  biomes = first(biomes),
  BATOT  =  mean(BATOT,  na.rm  =  TRUE),
  BAintra  =  mean(BAintra,  na.rm  =  TRUE),
  G  =  mean(G,  na.rm  =  TRUE),
  MAT  =  mean(MAT,  na.rm  =  TRUE),
  MAP  =  mean(MAP,  na.rm  =  TRUE),
  sd.Leaf.N =  sd(Leaf.N.focal, na.rm = TRUE),
  sd.SLA =  sd(SLA.focal, na.rm = TRUE),
  sd.Wood.density =  sd(Wood.density.focal, na.rm = TRUE),
  sd.Seed.mass =  sd(Seed.mass.focal, na.rm = TRUE),
  sd.Max.height =  sd(Max.height.focal, na.rm = TRUE),
  mean.Leaf.N =  mean(Leaf.N.focal, na.rm = TRUE),
  mean.SLA =  mean(SLA.focal, na.rm = TRUE),
  mean.Wood.density =  mean(Wood.density.focal, na.rm = TRUE),
  mean.Seed.mass =  mean(Seed.mass.focal, na.rm = TRUE),
  mean.Max.height =  mean(Max.height.focal, na.rm = TRUE),
  per.deciduous = fun.perc(Pheno.T, value = 'D'),
  per.angio = fun.perc(Phylo.group, value = 'Angiosperm') )
# compute
return(summarise_by_cluster)
}


fun.compute.all.var.cluster2 <- function(data){
by_plot <- paste(data[['plot']], data[['census']])
summarise_by_cluster<- data.frame(
  plot.id = tapply(by_plot,by_plot, first2),
  count  =  tapply(data$sp, by_plot, length),
  n_sp = tapply(data$sp, by_plot, n_distinct),
  shannon = tapply(data$sp, by_plot, fun_div1),
  simpson = tapply(data$sp, by_plot, fun_div2),
  set = tapply(data$set,by_plot, first2),
  ecocode = tapply(data$ecocode,by_plot, first2),
  biomes = tapply(data$biomes,by_plot, first2),
  BATOT  =  tapply(data$BATOT, by_plot, mean, na.rm  =  TRUE),
  BAintra  =  tapply(data$BAintra, by_plot, mean, na.rm  =  TRUE),
  G  =  tapply(data$G, by_plot, mean, na.rm  =  TRUE),
  MAT  =  tapply(data$MAT, by_plot, mean, na.rm  =  TRUE),
  MAP  =  tapply(data$MAP, by_plot, mean, na.rm  =  TRUE),
  sd.Leaf.N =  tapply(data$Leaf.N.focal, by_plot, sd, na.rm  =  TRUE),
  sd.SLA =  tapply(data$SLA.focal, by_plot, sd, na.rm  =  TRUE),
  sd.Wood.density =  tapply(data$Wood.density.focal, by_plot, sd, na.rm  =  TRUE),
  sd.Seed.mass =  tapply(data$Seed.mass.focal, by_plot, sd, na.rm  =  TRUE),
  sd.Max.height =  tapply(data$Max.height.focal, by_plot, sd, na.rm  =  TRUE),
  mean.Leaf.N =  tapply(data$Leaf.N.focal, by_plot, mean, na.rm  =  TRUE),
  mean.SLA =  tapply(data$SLA.focal, by_plot, mean, na.rm  =  TRUE),
  mean.Wood.density =  tapply(data$Wood.density.focal, by_plot, mean, na.rm  =  TRUE),
  mean.Seed.mass =  tapply(data$Seed.mass.focal, by_plot, mean, na.rm  =  TRUE),
  mean.Max.height =  tapply(data$Max.height.focal, by_plot, mean, na.rm  =  TRUE),
  per.deciduous = tapply(data$Pheno.T, by_plot, function(x) sum(x=='D')/sum(!is.na(x))),
  per.angio =  tapply(data$Phylo.group, by_plot, function(x) sum(x=='Angiosperm')/sum(!is.na(x))))
# compute
return(summarise_by_cluster)
}



fun.compute.all.var.cluster3 <- function(data){
require(data.table)
data$plot.id <- paste(data[['plot']], data[['census']])
DT <-  data.table(data)
setkey(DT, plot.id)
sss <- DT[,list(count = length(sp),
                n_sp = n_distinct(sp),
                shannon = fun_div1(sp),
                simpson = fun_div2(sp),
                set = first2(set),
                ecocode = first2(ecocode),
                biomes = first2(biomes),
                BATOT  =  mean(BATOT,  na.rm  =  TRUE),
                G  =  mean(G,  na.rm  =  TRUE),
                MAT  =  mean(MAT,  na.rm  =  TRUE),
                MAP  =  mean(MAP,  na.rm  =  TRUE),
                sd.Leaf.N =  sd(Leaf.N.focal, na.rm = TRUE),
                sd.SLA =  sd(SLA.focal, na.rm = TRUE),
                sd.Wood.density =  sd(Wood.density.focal, na.rm = TRUE),
                sd.Seed.mass =  sd(Seed.mass.focal, na.rm = TRUE),
                sd.Max.height =  sd(Max.height.focal, na.rm = TRUE),
                mean.Leaf.N =  mean(Leaf.N.focal, na.rm = TRUE),
                mean.SLA =  mean(SLA.focal, na.rm = TRUE),
                mean.Wood.density =  mean(Wood.density.focal, na.rm = TRUE),
                mean.Seed.mass =  mean(Seed.mass.focal, na.rm = TRUE),
                mean.Max.height =  mean(Max.height.focal, na.rm = TRUE),
                per.deciduous = fun.perc(Pheno.T, value = 'D'),
                per.angio = fun.perc(Phylo.group, value = 'Angiosperm')),
                by = plot.id]
return(sss)
}



predict.lmer.ci <-  function(var.y, data){
require(lme4)
fm1 <- lmer(formula = eval(parse(text = paste(var.y,' ~shannon*biomes +(1|set)'))),
            data = data)
newdat <- expand.grid( 0,
    shannon=seq(0, 3.5, length.out = 20)
    , biomes= levels(data$biomes))
names(newdat) <- c(var.y, 'shannon', 'biomes')
mm <- model.matrix(terms(fm1),newdat)
newdat[[var.y]] <- mm %*% fixef(fm1)
pvar1 <- diag(mm %*% tcrossprod(vcov(fm1),mm))
tvar1 <- pvar1+VarCorr(fm1)$Subject[1]
newdat <- data.frame(
    newdat
    , plo = newdat[[var.y]]-2*sqrt(pvar1)
    , phi = newdat[[var.y]]+2*sqrt(pvar1)
)
return(newdat)
}


plot.line.by <- function(level, DF, col.vec, var.y,
                         var.x = 'shannon', cat = 'biomes', ...){
    lines(DF[[var.x]][DF[[cat]] == level],
          DF[[var.y]][DF[[cat]] == level],
          col = col.vec[level], ...)
}

plot.poly.by <- function(level, DF, col.vec,
                         var.x = 'shannon', cat = 'biomes', ...){
polygon(c(DF[[var.x]][DF[[cat]] == level], rev(DF[[var.x]][DF[[cat]] == level])),
        c(DF$plo[DF[[cat]] == level], rev(DF$phi[DF[[cat]] == level])) ,
        col = adjustcolor(col.vec[level], alpha.f = 0.5),
        ...)
}

pred.shannon.vs.x.ci.lines <-  function(DF, var, ...){
pred.dat <- predict.lmer.ci(var.y= var, data = DF)
plot(DF$shannon, DF[[var]], cex = 0, ...)
lapply(levels(DF$biomes), plot.line.by, DF= pred.dat,
       col.vec = fun.col.pch.biomes()$col.vec[levels(DF$biomes)],
       var.y = var, lwd= 3)
lapply(levels(DF$biomes), plot.poly.by, DF= pred.dat,
       col.vec = fun.col.pch.biomes()$col.vec[levels(DF$biomes)],
       lwd = 1, lty = 2)
}

fun.select.quantile <-  function(var, probs = c(0.1, 0.9)){
qq <- quantile(var, probs , nar.rm = TRUE)
return(!is.na(var) & var > qq[1] & var < qq[2])
}



fun.plot.var.MAT <- function(var, ...){
plot((data.summarise$MAT[fun.select.quantile(data.temp$MAT)]),
     data.summarise[[var]][fun.select.quantile(data.temp$MAT)],
     cex = 0,
     xlab = expression(paste('MAT ', (~degree~C))), ...)
fun.boxplot.breaks((data.summarise$MAT[fun.select.quantile(data.temp$MAT)]),
                   data.summarise[[var]][fun.select.quantile(data.temp$MAT)],
                   Nclass = 15, add = TRUE)
}

fun.plot.var.MAP <- function(var, ...){
plot(log(data.summarise$MAP[fun.select.quantile(data.temp$MAP)]),
     data.summarise[[var]][fun.select.quantile(data.temp$MAP)],
     cex = 0,
     xlab = expression(paste('MAP ',  (mm/yr))), xaxt = 'n', ...)
axis(1, at =  log(c(200, 500, 1000, 3000)),
     label =  c(200, 500, 1000, 3000))
fun.boxplot.breaks(log(data.summarise$MAP[fun.select.quantile(data.temp$MAP)]),
                   data.summarise[[var]][fun.select.quantile(data.temp$MAP)],
                   Nclass = 15, add = TRUE)
}

