
# CHECK PACKAGES are installed and load
check_packages <- function(packages=c("data.table", "dismo", "doParallel", "foreign", "knitr", "lme4", "maptools", "markdown", "MASS", "mvoutlier", "pander", "quantreg", "RColorBrewer", "reshape", "rgdal", "rjson", "rworldmap", "sp","Rcpp")){
	

	#install if needed
	new.packages <- packages[!(packages %in% installed.packages()[,"Package"])]
	if(length(new.packages)) install.packages(new.packages,repos='http://cran.csiro.au/')
	#load
	for(p in packages)
		suppressMessages(library(p, quietly=TRUE, character.only =TRUE)	)	
}
