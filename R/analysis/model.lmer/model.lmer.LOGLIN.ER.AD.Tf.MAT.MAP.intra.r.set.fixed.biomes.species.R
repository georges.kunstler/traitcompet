load.model <- function () {
    list(name="lmer.LOGLIN.ER.AD.Tf.MAT.MAP.intra.r.set.fixed.biomes.species",
         var.BLUP = 'set.id',
         lmer.formula.tree.id=formula("logG~1+biomes.id+MAT+MAP+Tf+Tf:biomes.id+logD+sumBn.inter+sumBn.inter:biomes.id+sumBn.intra+sumBn.intra:biomes.id+sumTfBn+sumTfBn:biomes.id+sumTnBn+sumTnBn:biomes.id+sumTnTfBn.abs+sumTnTfBn.abs:biomes.id +(1+logD+sumBn.inter+sumBn.intra||species.id)+(1|plot.id)+(1+Tf+sumBn.inter+sumBn.intra+sumTfBn+sumTnBn+sumTnTfBn.abs||set.id)"))
}

