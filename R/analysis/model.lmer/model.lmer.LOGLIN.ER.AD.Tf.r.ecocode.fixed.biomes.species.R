load.model <- function () {
    list(name="lmer.LOGLIN.ER.AD.Tf.r.ecocode.fixed.biomes.species",
         var.BLUP = 'ecocode.id',
         lmer.formula.tree.id=formula("logG~1+(1|ecocode.id)+(1|species.id)+(1|plot.id)+biomes.id+Tf+Tf:biomes.id+logD+sumBn+sumBn:biomes.id+sumTfBn+sumTfBn:biomes.id+sumTnBn+sumTnBn:biomes.id+sumTnTfBn.abs+sumTnTfBn.abs:biomes.id +(logD-1|species.id) +(sumBn-1|species.id)+(sumBn-1|ecocode.id)+(Tf-1|ecocode.id)+(sumBn-1|ecocode.id)+(sumTfBn-1|ecocode.id)+(sumTnBn-1|ecocode.id)+(sumTnTfBn.abs-1|ecocode.id)"))
}

