load.model <- function () {
    list(name="lmer.LOGLIN.ER.AD.Tf.intra.r.set.species",
         var.BLUP = 'set.id',
         lmer.formula.tree.id=formula("logG~1+Tf+logD+sumBn.inter+sumBn.intra+sumTfBn+sumTnBn+sumTnTfBn.abs +(1+logD+sumBn.inter+sumBn.intra||species.id)+(1|plot.id)+(1+Tf+sumBn.inter+sumBn.intra+sumTfBn+sumTnBn+sumTnTfBn.abs||set.id)"))
}



