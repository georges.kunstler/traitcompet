

load.model <- function(){

	list(
		name  = "LOGLIN.ER.AD.Tf.r.biomes",
		## parameters to save
		pars = c('intercept' , 'mean_logD', 'mean_Tf',
  'mean_sumBn', 'mean_sumTfBn', 'mean_sumTnBn',
  'mean_sumTnTfBn_abs', 'sigma_inter_species',
  'sigma_inter_set', 'sigma_inter_plot',
  'sigma_inter_tree', 'sigma_logD_species',
  'sigma_Tf_biomes', 'sigma_sumBn_biomes',
  'sigma_sumBn_species', 'sigma_sumTfBn_biomes',
  'sigma_sumTnBn_biomes', 'sigma_sumTnTfBn_abs_biomes',
  'sigma', 'param_Tf_biomes',
  'param_sumBn_biomes', 'param_sumTfBn_biomes',
  'param_sumTnBn_biomes', 'param_sumTnTfBn_abs_biomes'),
		bug =
"#######################################################
 ######################## growth model with JAGS #######
 model {
     ############ Likelihood ###################
for (i in 1:N_indiv) {
logG[i] ~ dnorm(theo_g[i],tau)
theo_g[i] <- inter[i] + DD[i] +TTf[i] + SUMBBN[i] +SUMTFBBN[i] + SUMTNBBN[i] + SUMABSBBN[i]

inter[i] <-  intercept + intercept_species[species_id[i]] + intercept_tree[tree_id[i]]*tree_01[i]  + intercept_plot[plot_id[i]]  + intercept_set[set_id[i]]
DD[i] <- (mean_logD + param_logD_species[species_id[i]])*logD[i]
TTf[i] <- (mean_Tf + param_Tf_biomes[biomes_id[i]])*Tf[i]
SUMBBN[i] <-(mean_sumBn + param_sumBn_biomes[biomes_id[i]] + param_sumBn_species[species_id[i]])*sumBn[i]
SUMTFBBN[i] <- (mean_sumTfBn + param_sumTfBn_biomes[biomes_id[i]])*sumTfBn[i]
SUMTNBBN[i] <- (mean_sumTnBn + param_sumTnBn_biomes[biomes_id[i]])*sumTnBn[i]
SUMABSBBN[i] <- (mean_sumTnTfBn_abs + param_sumTnTfBn_abs_biomes[biomes_id[i]])*sumTnTfBn_abs[i]
}

 ################################################
 ########### Hierarchical parameters ########

### species random param
for (n in 1:N_species)
{
    param_logD_species[n] ~ dnorm(0,tau_logD_species)
    param_sumBn_species[n] ~ dnorm(0,tau_sumBn_species)
    intercept_species[n] ~ dnorm(0,tau_inter_species)
}

## plot effect
for (j in 1:N_plot)
  {
     intercept_plot[j] ~ dnorm(0,tau_inter_plot)
  }
## tree effect
for (j in 1:N_tree)
  {
  intercept_tree[j] ~ dnorm(0,tau_inter_tree)
  }

## tree effect
for (j in 1:N_set)
  {
  intercept_set[j] ~ dnorm(0,tau_inter_set)
  }

## biomes
for (j in 1:N_biomes)
  {
  param_Tf_biomes[j] ~ dnorm(0, tau_Tf_biomes)T(-5, 5)
  param_sumBn_biomes[j] ~ dnorm(0, tau_sumBn_biomes)T(-5, 5)
  param_sumTfBn_biomes[j] ~ dnorm(0 ,tau_sumTfBn_biomes)T(-5, 5)
  param_sumTnBn_biomes[j] ~ dnorm(0 ,tau_sumTnBn_biomes)T(-5, 5)
  param_sumTnTfBn_abs_biomes[j] ~ dnorm(0 ,tau_sumTnTfBn_abs_biomes)T(-5, 5)
  }
###############################################
########### Non-hierarchical parameters ########

# constants for prior
tau0 <- 1.0E-4

  intercept ~ dnorm(0,tau0)T(-5, 5)
  mean_logD ~ dnorm(0,tau0)T(-5, 5)
  mean_Tf ~ dnorm(0,tau0)T(-5, 5)
  mean_sumBn ~ dnorm(0,tau0)T(-5, 5)
  mean_sumTfBn ~ dnorm(0,tau0)T(-5, 5)
  mean_sumTnBn ~ dnorm(0,tau0)T(-5, 5)
  mean_sumTnTfBn_abs ~ dnorm(0,tau0)T(-5, 5)


# variance error
tau <-  pow(sigma,-2)
sigma ~ dunif(0.00001,5)
tau_inter_plot <-  pow(sigma_inter_plot,-2)
sigma_inter_plot ~ dunif(0.00001,3)
tau_inter_tree <-  pow(sigma_inter_tree,-2)
sigma_inter_tree ~ dunif(0.00001,3)
tau_inter_set <-  pow(sigma_inter_set,-2)
sigma_inter_set ~ dunif(0.00001,3)
tau_inter_species <-  pow(sigma_inter_species,-2)
sigma_inter_species ~ dunif(0.00001,3)
tau_logD_species <-  pow(sigma_logD_species,-2)
sigma_logD_species ~ dunif(0.00001,3)
tau_sumBn_species <-  pow(sigma_sumBn_species,-2)
sigma_sumBn_species ~ dunif(0.00001,3)
tau_Tf_biomes <-  pow(sigma_Tf_biomes,-2)
sigma_Tf_biomes ~ dunif(0.00001,3)
tau_sumBn_biomes <-  pow(sigma_sumBn_biomes,-2)
sigma_sumBn_biomes ~ dunif(0.00001,3)
tau_sumTfBn_biomes <-  pow(sigma_sumTfBn_biomes,-2)
sigma_sumTfBn_biomes ~ dunif(0.00001,3)
tau_sumTnBn_biomes <-  pow(sigma_sumTnBn_biomes,-2)
sigma_sumTnBn_biomes ~ dunif(0.00001,3)
tau_sumTnTfBn_abs_biomes <-  pow(sigma_sumTnTfBn_abs_biomes,-2)
sigma_sumTnTfBn_abs_biomes ~ dunif(0.00001,3)


} # End of the jags model
 ")
}
