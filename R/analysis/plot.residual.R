### Function to plot the residual of a model
source('R/utils/plot.R')

fun.load.data.for.model <-  function(res){
   ## get details
   trait <- res$files.details[['trait']]
   cat.TF <- grepl('CAT', res$files.details[['model']])
   set <- res$files.details[['set']]
   if(set == 'all.no.log') fname <- 'data.all.no.log.rds'
   if(set == 'all.log') fname <- 'data.all.log.rds'

   ## load results
   df.lmer <- load.data.for.lmer(trait,fname = fname,
                                  cat.TF = cat.TF,
                                  sample.size. = NA,
                                  var.sample. = NA,
                                  select.biome. = NA)
return(df.lmer)
}



fun.points.colors.density <- function(x,y,...){
mat   <- cbind(x,y)
data <- as.data.frame(mat)
colors.dens  <- densCols(mat)
points(x,y, col=colors.dens, ...)
}


fun.boxplot.breaks <-  function(x,y,Nclass=15,add=TRUE,...){
x1 <- x[!is.na(x) & !is.na(y)]
y1 <- y[!is.na(x) & !is.na(y)]
x <- x1
y <- y1
breakss <- seq(min(x),max(x),length=Nclass+1)
x.cut <- cut(x,breakss)
if(add)  mid.points <-  NA
if(!add) mid.points <- round(breakss[-(Nclass+1)]+abs(breakss[2]-breakss[1])/2, 1)
boxplot(y~x.cut,at=mid.points,names=mid.points,
        boxwex = abs(breakss[2]-breakss[1])*0.8,
        outline = FALSE,
        add = add,
        col = grey(1-table(x.cut)/length(x.cut)), axes = TRUE,
        ...)
}


plot.qqplot <-  function(fm1){
par(mfrow = c(1,2))
qqnorm(resid(fm1), main='Normality Check for Residuals')
qqline(resid(fm1))
hist(resid(fm1), breaks = 100, xlab = 'residual')
}

fun.plot.resid.fitted <-  function(fm1){
 require(ggplot2)
 a <- fun.hexbin.with.smooth.ggplot(fitted(fm1), resid(fm1), xname = 'fitted',
                                   yname = 'residual', cor.TF = FALSE)
}

fun.plot.resid.var <-  function(var, fm1){
require(mgcv)
var.data <- model.matrix(fm1)[, var]
 a <- fun.hexbin.with.smooth.ggplot(var.data, resid(fm1), xname = var,
                                   yname = 'residual', cor.TF = FALSE)
}


plot.all.resid.to.png <-  function(fm1, data.type, trait, model ,
                                   output.dir = 'output/lmer'){
vars <- names(fixef(fm1))[-1]
name <- paste( trait, model,
              'residual.plot%03d.png', sep = '.')
png(file.path(output.dir,data.type,name))
plot.qqplot(fm1)
a <- fun.plot.resid.fitted(fm1)
multiplot(a)
b <- lapply(vars, fun.plot.resid.var, fm1)
multiplot(plotlist = b, cols =2)
dev.off()
}

read.and.plot.resid <-  function(name){

fm1 <- read.lmer.output(name)
details <- files.details.all(name)
plot.all.resid.to.png(fm1[[1]],
                      data.type = details['data.type'],
                      trait = details['trait'],
                      model = details['model'] )
}

## library(lme4)

## hdp <- read.csv("http://www.ats.ucla.edu/stat/data/hdp.csv")
## hdp <- within(hdp, {
##   Married <- factor(Married, levels = 0:1, labels = c("no", "yes"))
##   DID <- factor(DID)
##   HID <- factor(HID)
## })

## head(hdp)
## fm1 <- lmer(tumorsize ~ co2 + Age +WBC +  (1 | DID), data = hdp)
## plot.qqplot(fm1)
## a <- fun.plot.resid.fitted(fm1)
## multiplot(a)
## b <- lapply(vars, fun.plot.resid.var, fm1)
## multiplot(plotlist = b, cols =2)

