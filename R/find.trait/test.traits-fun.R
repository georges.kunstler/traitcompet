###########
## FUNCTION TO TEST AND PLOTS TRAITS



## Fun to test that the range of traits is ok
## Leaf N mg g-1 between 2 and 50 Kattge  et al 2011
## Seed mass mg between 0.0001 and 100000 (Moles et al. 2005)
## Coco nucifera 600000
## SLA mm2mg-1 between 1 and 100 Katgge et al 2011
## Wood.density.mean mg mm-3 0.1 1.5 (Chave et al. 2009)
## Max.height.mean m max 100 (115 max height)
fun.test.one.trait <- function(t, t.min, t.max) {
     if(sum(!is.na(t))) { res <- (max(t, na.rm = TRUE) >t.max ) |
                                 (min(t, na.rm = TRUE)<t.min) }
     else{ res <- FALSE }
     return(res)
     }

fun.test.one.trait.data <- function(i, data, trait.name, t.min, t.max) {
     fun.test.one.trait(data[[trait.name[i]]], t.min[i], t.max[i])
     }

fun.test.range.traits <- function(data.traits, set) {
     trait.name <- c("Leaf.N.mean", "Seed.mass.mean", "SLA.mean",
                     "Wood.density.mean", "Max.height.mean")
     t.min <- c(2, 0.0001, 0.9, 0.1, 1)
     t.max <- c(60, 600000, 100, 1.5, 100)
     vec.test <- sapply(1:5, fun.test.one.trait.data,
                        data.traits, trait.name, t.min, t.max)
     if(any(vec.test))
          stop(paste("Trait", paste(trait.name[vec.test], collapse = " "),
                     "out of range for set", set))
     else { print("All traits within suitable range") }
     }

## test type of variable
fun.test.type.data <- function(t.name, data, type.t, set) {
    t <- data[[t.name]]
     if(sum(!is.na(t))) {
          if(!(mode(t)==type.t)) stop(paste("In set", set, "variable", t,
                                            "is not", type.t, sep = " ")) }
     }

fun.test.data.type.traits <- function(data.traits, set) {
     trait.n <- c("Leaf.N", "Seed.mass", "SLA", "Wood.density", "Max.height")
     character.var <- c("sp", "Latin_name", 'Phylo.group', 'Pheno.T')
     numeric.var <- c(paste(trait.n, ".mean", sep = ""),
                      paste(trait.n, ".sd", sep = ""))
     TF.var <- c(paste(trait.n, ".genus", sep = ""))

     lapply(character.var,  fun.test.type.data,  data.traits,
            "character",  set)
     lapply(numeric.var,  fun.test.type.data,  data.traits,  "numeric",  set)
     lapply(TF.var,  fun.test.type.data,  data.traits,  "logical",  set)
     }


## plot fun
## plots xy for each traits combination
fun.plot.xy.traits <- function(get.traits, k) {
     pdf(file.path("figs/", paste(k, "traits.XY.pdf", sep = ".")),
         height = 25, width = 10)
     par(mfrow = c(5, 2))
     for (i in 1:4) {
          for (j in (i+1):5) {
          if (sum(!is.na(get.traits[[trait.name[i]]] ) &
                  !is.na(get.traits[[trait.name[j]]] ))>0) {
               plot(get.traits[[trait.name[i]]] , get.traits[[trait.name[j]]],
                    xlab = trait.name[i], ylab = trait.name[j],
                    log = "xy", main = k)
               }
          else{plot(1 , 1, xlab = trait.name[i], ylab = trait.name[j],
                    log = "xy", main = k)}
          } }
     dev.off()
     }


## fun to run all test
fun.test.set <- function(set, filedir) {
     get.traits <- read.csv(file.path(filedir,  set,  "traits.csv"),
                            header = T,  stringsAsFactors =FALSE)
     trait.n <- c("Leaf.N", "Seed.mass", "SLA", "Wood.density", "Max.height")
     character.var <- c("sp", "Latin_name", 'Phylo.group', 'Pheno.T')
     numeric.var <- c(paste(trait.n, ".mean", sep = ""),
                      paste(trait.n, ".sd", sep = ""))
     TF.var <- c(paste(trait.n, ".genus", sep = ""))
     if(sum(!c(character.var, numeric.var, TF.var) %in% names(get.traits)) >0)
         stop(paste("not good variables names", set))
     ## fun.plot.xy.traits(get.traits, set)
     fun.test.range.traits(get.traits, set)
     fun.test.data.type.traits(get.traits, set)
     cat(set, "OK \n")
  return(data.frame(set = rep(set, nrow(get.traits)), get.traits))
  }



fun.ellipsoid.hull <- function(set, x, y, sets.v, col.vec){
require(cluster)
xy <- cbind((x[sets.v==set]), (y[sets.v==set]))
xy <- xy[!is.na(xy[, 1]) & !is.na(xy[, 2]), ]
xy <- xy[xy[, 1]>quantile(xy[, 1], 0.1) &xy[, 1]<quantile(xy[, 1], 0.9) &
         xy[, 2]>quantile(xy[, 2], 0.1) &xy[, 2]<quantile(xy[, 2], 0.9), ]

if (nrow(xy)>10){
   exy <- ellipsoidhull(xy)
   lines(predict(exy), col = col.vec[set])
   }
}


### function to compute perc ref in try


fun.perc.ref <- function(data.TRY.std, all.traits, var){
    data.try <- data.TRY.std[!(is.na(data.TRY.std$SLA) &
                               is.na(data.TRY.std$Wood.density) &
                               is.na(data.TRY.std$height)),]
    select.try <- sapply(data.try$Latin_name,fun.get.genus) %in%
                               sapply(all.traits$Latin_name,fun.get.genus)
    vec.ref.used <- data.try[[var]][select.try]

    perc.ref <- sort(table(vec.ref.used) / length(vec.ref.used) )
    print(names(perc.ref)[perc.ref > 0.005])
    ref <- names(perc.ref)
    Encoding(ref) <- 'latin1'
    names(perc.ref) <- ref
    table.ref <- data.frame(ref = ref, perc = perc.ref,
                            stringsAsFactors = FALSE)
    return(table.ref)
}
