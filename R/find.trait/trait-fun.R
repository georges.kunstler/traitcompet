################# FUNCTION TO EXTRACT DECTED OUTLIER AND FORMAT TRY DATA
## Georges Kunstler
############################################ 14/06/2013


### install all unstallled packages
source("R/packages.R")
check_packages(c("MASS", "doParallel", "mvoutlier", "plyr", "gdata"))



## fun detect outlier
fun.detect.outlier <- function(x.na){
  x <- x.na[!is.na(x.na)]
  x.num <- (1:length(x.na))[!is.na(x.na)]
  TF.vec <- rep(FALSE, length(x.na))
  fit.dist <- fitdistr((na.omit(x)), "normal")
  high.bound <- fit.dist$estimate["mean"] + 2 * (fit.dist$estimate["sd"] + 
  fit.dist$sd["sd"])
  low.bound <- fit.dist$estimate["mean"] - 2 *
               (fit.dist$estimate["sd"] + fit.dist$sd["sd"])
  TF.vec[x.num[(x) > high.bound | (x) < low.bound]] <- TRUE
return(TF.vec)
}
## outlier detection based on Kattage et al 2011
##' Detection of univar outlier based on method of Kattge et al. 2011
##'
##' 
##' @title detect outlier
##' @param x.na 
##' @param log 
##' @return TRUE FALSE vector to identify outlier TRUE : outlier
##' @author Kunstler
fun.out.TF2 <- function(x.na, log = TRUE) {
    if (log) {
        TF.vec <- fun.detect.outlier(log10(x.na))
    } else {
        TF.vec <- fun.detect.outlier((x.na))
    }
    return((TF.vec))
}

######################## FUNCTION TO COMPUTE QUANTILE FOR HEIGHT
f.quantile <- function(x, ind, probs) {
    quantile(x[ind], probs = probs, na.rm = TRUE)
}

f.quantile.boot2 <- function(x, R, probs = 0.99) {
    require(boot, quietly = TRUE)
    if (length(na.exclude(x)) > 0) {
        quant.boot <- boot(x, f.quantile, R = R, probs = probs)
        return(c(mean = mean(quant.boot$t), sd = sd(quant.boot$t),
                 nobs = length(na.exclude(x))))
    } else {
        return(c(mean = NA, sd = NA, nobs = NA))
    }
}

## FUNcCTION TO COMPUTE MEAN SD AND NOBS
## WITH OR WITHOUT OUTLIER
fun.mean.sd.nobs.out <- function(x, i) {
    if (length(x) > 50) {
        ## if more than 50 obs remove outlier
        outlier <- fun.out.TF2(x.na = x, log = TRUE)
        res.temp <- c(mean((x[!outlier])),
                      sd((x[!outlier])),
                      length(x[!outlier]))
    } else {
            res.temp <- c(mean((x)), sd((x)), length(x))
    }
    return(res.temp)
}

## TRY mean no experimental data
fun.mean.per.sp.noexp <- function(i, data,species.syno){
x <- data[[i]][data$Latin_name %in% species.syno & (!is.na(data[[i]])) & 
                  (!data[["TF.exp.data"]])]
res.temp <- fun.mean.sd.nobs.out(x, i)
names(res.temp) <- c('mean', 'sd', 'nobs')
return(data.frame(t(res.temp), exp = FALSE, genus = FALSE))
}
## TRY mean with experimental data
fun.mean.per.sp.exp <- function(i, data,species.syno){
x <- data[[i]][data$Latin_name %in% species.syno & (!is.na(data[[i]]))]
res.temp <- fun.mean.sd.nobs.out(x, i)
names(res.temp) <- c('mean', 'sd', 'nobs')
return(data.frame(t(res.temp), exp = TRUE, genus = FALSE))
}
## Genus mean if species not available
fun.mean.per.genus.exp <- function(i, data, genus){
x <- data[[i]][grepl(genus, data$Latin_name, fixed = TRUE) & 
                  (!is.na(data[[i]]))]
res.temp <- fun.mean.sd.nobs.out(x, i)
names(res.temp) <- c('mean', 'sd', 'nobs')
return(data.frame(t(res.temp), exp = TRUE, genus = TRUE))
}
## apply noexp exp or genus depending on the available data
fun.mean.if.sp.exp.genus <- function(i,data, species.syno){
    res.temp <- data.frame(mean = NA, sd = NA, nobs = NA,
                           exp = NA, genus = FALSE)
        if (sum((data$Latin_name%in% species.syno) & !is.na(data[[i]])) > 0) {
            ## if data for this species or syno 
            if (sum((data$Latin_name %in% species.syno) & (!is.na(data[[i]])) & 
                (!data[["TF.exp.data"]])) > 0) {
                #if data with out experiments
                res.temp <- fun.mean.per.sp.noexp(i, data, species.syno)
            } else {
                ### include experimental data
                res.temp <- fun.mean.per.sp.exp(i, data, species.syno)
            }
        } else {
            ### compute data at genus level if no data for the species
            genus <- unique(sub(" .*", "", species.syno))
            if (sum(grepl(genus, data$Latin_name) & (!is.na(data[[i]]))) > 0) {
                res.temp <- fun.mean.per.genus.exp(i, data,genus)
            }
        }
        return(res.temp)
    }

## extract mean sd per species or genus added
## species synonyme
fun.species.traits <- function(species.code, species.table, col.sp = "sp",
                               col.sp.syno = "Latin_name_syn", 
    traits, data) {
    species.syno <- species.table[species.table[[col.sp]] == species.code,
                                  col.sp.syno]
    DF <- do.call("rbind", lapply(traits, fun.mean.if.sp.exp.genus,
                                  data,species.syno))
    return(list(mean = DF[["mean"]], sd = DF$sd, exp = DF$exp,
                genus = DF$genus, nobs = DF$nobs))
}


####################### FUNCTIONS TO Manipulate species names
fun.get.genus <- function(x)
    gsub(paste(" ", gsub("^([a-zA-Z]* )", "", x),
                                        sep = ""), 
         "", x, fixed = TRUE)
trim.trailing <- function(x) sub("\\s+$", "", x)


fun.compute.mean.genus <- function(latin_name, data,var){
 genus <- unique(sub(" .*", "", latin_name))
return(fun.mean.per.genus.exp(var,data,genus)[,c(1,2,5)])
}

################################################################################
####################################### FUN TO EXTRACT FOR A GIVEN DATA BASE

# function top turn the result of lapply in a data frame with good structure
fun.turn.list.in.DF <- function(sp, res.list) {
    data.mean <- t(sapply(sp, FUN = function(i, res.list) res.list[[i]]$mean,
                          res.list = res.list))
    data.sd <- t(sapply(sp, FUN = function(i, res.list) res.list[[i]]$sd,
                        res.list = res.list))
    data.exp <- t(sapply(sp, FUN = function(i, res.list) res.list[[i]]$exp,
                         res.list = res.list))
    data.genus <- t(sapply(sp, FUN = function(i, res.list) res.list[[i]]$genus,
                           res.list = res.list))
    data.nobs <- t(sapply(sp, FUN = function(i, res.list) res.list[[i]]$nobs,
                          res.list = res.list))
    ## create data.frame withh all observation
    extract.species.try <- data.frame(sp, data.mean, data.sd, data.exp,
                                      data.genus, data.nobs,
                                      stringsAsFactors = FALSE)
    names(extract.species.try) <- c("sp",
                                    paste(c("Leaf.N", "Seed.mass",
                                            "SLA", "Wood.density"),
                                          "mean", sep = "."),
                                    paste(c("Leaf.N", "Seed.mass",
                                            "SLA", "Wood.density"),
                                          "sd", sep = "."),
                                    paste(c("Leaf.N", "Seed.mass",
                                            "SLA", "Wood.density"),
                                          "exp", sep = "."),
                                    paste(c("Leaf.N", "Seed.mass",
                                            "SLA", "Wood.density"),
                                          "genus", sep = "."),
                                    paste(c("Leaf.N", "Seed.mass",
                                            "SLA", "Wood.density"),
                                          "nobs", sep = "."))
    return(extract.species.try)
}

##########################
##### FUNCTION TO EXTRACT TRY DATA FOR A SPECIES NEED TO DOCUMENT 
fun.extract.format.sp.traits.TRY <- function(sp, sp.syno.table, data) {  
    ### test data sp and sp.syno.table match
    require(gdata)
    sp.syno.table[["Latin_name_syn"]]  <-
        trim(as.character(sp.syno.table[["Latin_name_syn"]]) )
    data[["Latin_name"]] <- trim(as.character(data[["Latin_name"]]))
    sp.syno.table[["Latin_name_syn"]][
                          is.na(sp.syno.table[["Latin_name_syn"]])] <- "missing"
    sp.syno.table[["Latin_name"]][
                          is.na(sp.syno.table[["Latin_name"]])] <- "missing"
    if (sum(!(sp %in% sp.syno.table[["sp"]])) > 0) 
        stop("not same species name in sp and sp.syno.table")
    if (sum((sp.syno.table[["Latin_name_syn"]] %in% data[["Latin_name"]])) == 
        0) 
        stop("not a single similar species name in sp and TRY")
    ## traits to extract
    traits <- c("Leaf.N", "Seed.mass", "SLA", "Wood.density")
    # lapply to extract
    res.list <- lapply(sp, FUN = fun.species.traits,
                       species.table = sp.syno.table, 
                       traits = traits, data = data)
    names(res.list) <- sp 
    ##### TRANSFORM LIST INTO A TABLE
    extract.species.try <- fun.turn.list.in.DF(sp , res.list)
    ##### TEST OF GOOD EXTRACTION OF TRAITS
     test.num <- sample((1:length(sp))[
                               !is.na(extract.species.try[["SLA.mean"]])],1)
    if( all.equal(extract.species.try[test.num,"SLA.mean"] ,
       fun.species.traits(sp[test.num], species.table = sp.syno.table, 
        traits = traits, data = data)$mean[grep("SLA",traits)]) != TRUE)
        stop('traits value not good for the  species in extraction from TRY')
## keep only a subset of var
    vars.keep <- c( "Leaf.N.mean", "Seed.mass.mean",
                   "SLA.mean", "Wood.density.mean", 
                    "Leaf.N.sd", "Seed.mass.sd",
                   "SLA.sd", "Wood.density.sd", 
                    "Leaf.N.genus", "Seed.mass.genus",
                   "SLA.genus", "Wood.density.genus")
    extract.species.try <- subset(extract.species.try, select=vars.keep)
    data.frame.TRY <- data.frame(sp = sp,
                                 Latin_name = sp.syno.table[["Latin_name_syn"]], 
                                 extract.species.try,
                                 stringsAsFactors =FALSE)
    if (sum(!data.frame.TRY[["sp"]] == sp) > 0) 
        stop("Wrong order of species code")
    return(data.frame.TRY)
}


##############################
##############################
### NO TRY TRAITS
### function to return mean and sd of traits (species or genus)  in a data.frame

fun.sp.mean.noTRY <- function(traits.mean.t, Latin_name,
                              data, name.match.traits){
        mean.vec <-mean(data[data[[name.match.traits]] %in% Latin_name,
                             traits.mean.t])
        genus.vec <- FALSE
        sd.vec <- NA
        return(data.frame(mean = mean.vec, genus = genus.vec, sd = sd.vec))
    }

fun.genus.mean.noTRY <- function(traits.mean.t, Latin_name,
                                 data, name.match.traits){
       genus <- sub(" .*", "", Latin_name)
       genus.species <- sub(" .*", "", data[[name.match.traits]])
       mean.vec <-mean( (data[genus.species %in% genus, traits.mean.t]))
       genus.vec <- TRUE
       sd.vec <- NA
       return(data.frame(mean = mean.vec, genus = genus.vec, sd = sd.vec))
    }

fun.sp.or.genus.mean.noTRY <- function(traits.mean.t, Latin_name,
                                       data.tot, name.match.traits){
if(sum(!is.na(data.tot[[traits.mean.t]]))>0){    
   data <- data.tot[!is.na(data.tot[[traits.mean.t]]), ]
    if(Latin_name %in% data[[name.match.traits]] ){
        res <- fun.sp.mean.noTRY(traits.mean.t, Latin_name,
                                 data, name.match.traits)
    }else{## do genus mean
        res <- fun.genus.mean.noTRY(traits.mean.t, Latin_name,
                                    data, name.match.traits)
    }
 }else{
      res <- data.frame(mean = NA, genus = NA, sd = NA)
 }
return(res)
}   

fun.spe.traits.notry <-  function(Latin_name, data.tot, traits.mean, traits.sd, 
                                  name.match.traits = "Latin_name", SD.TF){
require(gdata)
Latin_name <- trim(Latin_name)
data.tot[[name.match.traits]] <- trim(data.tot[[name.match.traits]])
DF.temp <- do.call("rbind", lapply(traits.mean, fun.sp.or.genus.mean.noTRY,
                                   Latin_name, data.tot, name.match.traits))
mean.vec <- DF.temp$mean
names(mean.vec) <-  traits.mean
sd.vec <- DF.temp$sd
names(sd.vec) <-  traits.sd
genus.vec <- DF.temp$genus
names(genus.vec) <-  sub("sd", "genus", traits.sd)
extract.species.traits <- data.frame(t(mean.vec), t(sd.vec), t(genus.vec),
                                     stringsAsFactors = FALSE)
return(extract.species.traits)
}



###########
### FUNCTION TO EXTRACT ALL SPECIES 

fun.extract.format.sp.traits.NOT.TRY <- function(sp,  Latin_name,  data,
                                                 name.match.traits = "Latin_name") {
require(plyr)
    ### test data sp and sp.syno.table match
    if (sum((Latin_name %in% data[[name.match.traits]])) == 
        0) 
        stop("not a single similar species name in sp and Traits data")
    ## traits to extract
    traits <- c("Leaf.N", "Seed.mass", 
        "SLA", "Wood.density", "Max.height")

    ### NEED TO ADD TEST IF SD available in the data
    if(sum(grepl("sd", names(data)))>0) SD.TF <- TRUE


traits.mean <- paste(traits, "mean", sep = ".")
traits.genus <- paste(traits, "genus", sep = ".")
if (SD.TF)     traits.sd <-  paste(traits, "sd", sep = ".")

## extract data
extract.species.traits <- rbind.fill(lapply(Latin_name,
                                            FUN = fun.spe.traits.notry ,
                                            data, traits.mean,
                                            traits.sd,
                                            name.match.traits, SD.TF))

    data.frame.TRAITS <- data.frame(sp = sp, Latin_name = Latin_name , 
        extract.species.traits, stringsAsFactors  = FALSE)
    if (sum(!data.frame.TRAITS[["sp"]] == sp) > 0) 
        stop("Wrong order of species code")
    return(data.frame.TRAITS)
}

## FUNCTION TO GET angio /confi genus is enough FROM TRY and Zanne
fun.get.cat.var.from.try <- function(sp, data.traits, try.cat, Pheno.Zanne){
    data.traits$Latin_name <- trim.trailing(data.traits$Latin_name)
 if (is.na(data.traits[data.traits$sp==sp, "Latin_name"]))
     data.traits[data.traits$sp==sp, "Latin_name"] <- "missing"
 if(sum(data.traits[data.traits$sp==sp, "Latin_name"] == try.cat$AccSpeciesName)>0){
     if(sum(data.traits[data.traits$sp==sp, "Latin_name"] ==
                           Pheno.Zanne$Binomial)>0){
        data.res <-
        data.frame(sp = sp, 
                   Latin_name = data.traits[data.traits$sp==sp, "Latin_name"], 
                   Phylo.group = try.cat[data.traits[data.traits$sp==sp, "Latin_name"] ==
                       try.cat$AccSpeciesName, "PhylogeneticGroup"][1], 
                   Pheno.T = try.cat[data.traits[data.traits$sp==sp, "Latin_name"] ==
                       try.cat$AccSpeciesName, "LeafPhenology"][1], 
                   LeafType.T = try.cat[data.traits[data.traits$sp==sp, "Latin_name"] ==
                       try.cat$AccSpeciesName, "LeafType"][1], 
                   Pheno.Z = Pheno.Zanne[data.traits[data.traits$sp==sp, "Latin_name"] ==
                       Pheno.Zanne$Binomial, 'Phenology'][1] , 
                   stringsAsFactors = FALSE)
        }else{
            data.res <- data.frame(sp = sp, 
                       Latin_name = data.traits[data.traits$sp==sp, "Latin_name"], 
                       Phylo.group = try.cat[data.traits[data.traits$sp==sp, "Latin_name"] ==
                           try.cat$AccSpeciesName, "PhylogeneticGroup"][1], 
                       Pheno.T = try.cat[data.traits[data.traits$sp==sp, "Latin_name"] ==
                           try.cat$AccSpeciesName, "LeafPhenology"][1], 
                       LeafType.T = try.cat[data.traits[data.traits$sp==sp, "Latin_name"] ==
                           try.cat$AccSpeciesName, "LeafType"][1], 
                       Pheno.Z = NA , 
                       stringsAsFactors = FALSE)
        }
 }else{
     if(sum(data.traits[data.traits$sp==sp, "Latin_name"] ==
                           Pheno.Zanne$Binomial)>0){
            data.res <- data.frame(sp = sp, 
                       Latin_name = data.traits[data.traits$sp==sp, "Latin_name"], 
                       Phylo.group = NA, 
                       Pheno.T = NA, 
                       LeafType.T = NA,
                       Pheno.Z = Pheno.Zanne[data.traits[data.traits$sp==sp, "Latin_name"] ==
                           Pheno.Zanne$Binomial, 'Phenology'][1] ,
                       stringsAsFactors = FALSE)
        }else{
            data.res <- data.frame(sp = sp,
                       Latin_name = data.traits[data.traits$sp==sp, "Latin_name"],
                       Phylo.group = NA,
                       Pheno.T = NA,
                       LeafType.T = NA,
                       Pheno.Z = NA ,
                       stringsAsFactors = FALSE)
        }
 }    

## if missing value for Phylo.group check genus
    if(is.na(data.res$Phylo.group)) {
      genus <- sub(" .*", "", data.traits[data.traits$sp==sp, "Latin_name"])
      genus.vec <- sub(" .*", "", try.cat$AccSpeciesName)
      data.res$Phylo.group <- try.cat[genus == genus.vec,
                                      "PhylogeneticGroup"][1]
    }
  return(data.res)    
}


## change factor of categorical variable try

fun.change.factor.pheno.try <- function(data.cat.extract){
data.cat.extract[data.cat.extract$Pheno.T=='deciduous'
                 & !is.na(data.cat.extract$Pheno.T), 'Pheno.T'] <- 'D'
data.cat.extract[data.cat.extract$Pheno.T=='evergreen'
                 & !is.na(data.cat.extract$Pheno.T), 'Pheno.T'] <- 'EV'
data.cat.extract[data.cat.extract$Pheno.T=='deciduous/evergreen'
                 & !is.na(data.cat.extract$Pheno.T), 'Pheno.T'] <- 'D_EV'
return(data.cat.extract)
}

fun.change.factor.angio.try <- function(data.cat.extract){
data.cat.extract[data.cat.extract$Phylo.group=='Angiosperm_Magnoliid'
                 & !is.na(data.cat.extract$Phylo.group), 'Phylo.group'] <-
                     'Angiosperm'
data.cat.extract[data.cat.extract$Phylo.group=='Angiosperm_Eudicotyl'
                 & !is.na(data.cat.extract$Phylo.group), 'Phylo.group'] <-
                     'Angiosperm'
data.cat.extract[data.cat.extract$Phylo.group=='Angiosperm_Monocotyl'
                 & !is.na(data.cat.extract$Phylo.group), 'Phylo.group'] <-
                     'Angiosperm'
return(data.cat.extract)
}

fun.change.factor.leaftype.try <- function(data.cat.extract){
data.cat.extract[data.cat.extract$LeafType.T=='broadleaved'
                 & !is.na(data.cat.extract$LeafType.T), 'LeafType.T'] <-
                     'broadleaved'
data.cat.extract[data.cat.extract$LeafType.T!='broadleaved'
                 & !is.na(data.cat.extract$LeafType.T), 'LeafType.T'] <-
                     'No.broadleaved'
return(data.cat.extract)
}


# fill missing TRY deciduous/evergreen by Zanne
fun.fill.pheno.try.with.zanne <- function(data.cat.extract){
data.cat.extract[is.na(data.cat.extract$Pheno.T), 'Pheno.T'] <-
    data.cat.extract[is.na(data.cat.extract$Pheno.T), 'Pheno.Z']
return(data.cat.extract)
}

## compute perc of traits cover per species
fun.compute.perc.cover.one.trait <- function(trait,  data) {
t.mean <- paste(trait, "mean", sep = ".")
t.genus <- paste(trait, "genus", sep = ".")
sp.perc <- sum(!is.na(data[[t.mean]]) & !data[[t.genus]])/nrow(data)
genus.perc <- sum(!is.na(data[[t.mean]]))/nrow(data)
return( c(sp.perc = sp.perc, genus.perc = genus.perc) )
}

#### function to combine non-try with try data
fun.combine.nontry.and.try <- function(trait,  data1, data2) {
t.mean <- paste(trait, "mean", sep = ".")
t.genus <- paste(trait, "genus", sep = ".")
for (i in 1:length(t.mean)) {
    data1[is.na(data1[[t.mean[i]]]), 
          c(t.mean[i], t.genus[i])] <- data2[is.na(data1[[t.mean[i]]]), 
                                            c(t.mean[i], t.genus[i])]
    }

return( data1 )
}


## fun to divide in three cat

fun.three.cat.C.A_EV.A_D <-  function(vec.pheno, vec.phylo){
    if (length(vec.pheno) != length(vec.phylo))
        stop('vec.pheno and vec.phylo does not have the same length')
    cat <- rep(NA, length(vec.pheno))
    cat[vec.phylo == 'Gymnosperm' &
        !is.na(vec.phylo)] <- 3
    cat[vec.phylo == 'Pteridophytes' &
        !is.na(vec.phylo)] <- 3 ## put the three species of pteridophy in conif
    cat[vec.phylo == 'Angiosperm' &
        !is.na(vec.phylo) &
        vec.pheno %in% c('EV', 'D_EV') &
        !is.na(vec.pheno)] <- 1
    cat[vec.phylo == 'Angiosperm' &
        !is.na(vec.phylo) &
        vec.pheno %in% c('D') &
        !is.na(vec.pheno)] <- 2
    return(cat)
}
