source("R/packages.R")
check_packages(c("maptools", "rgdal", "rgeos", "raster"))


### function to get ecoregion from wwf ecoregion map

GetEcoregions <- function(lons,lats,var.extract = 'eco_code') {
 require(rgdal)
  #read in ecoregions and transform shapefile to lat/lon
  wwf.ecoregions <- readOGR(dsn = "data/raw/wwf_ecoregion",
                            layer = "wwf_terr_ecos")
  geo.proj <- proj4string(wwf.ecoregions)
  #create SpatialPoints object for plots
  pts <- SpatialPoints(cbind(lons,lats), proj4string = CRS(geo.proj))


  #creates object that assigns each plot index to an ecoregion
  plot.ecocodes <- over(pts, wwf.ecoregions)[[var.extract]]

  #now need to lookup closest ecoregion for NAs and Lake
  ec.list <- unique(plot.ecocodes)

  #get the subset of ecoregions that have been matched to these plots
  ec.subset <- wwf.ecoregions[wwf.ecoregions[[var.extract]] %in% ec.list,]

  ec.na <- which(plot.ecocodes == "Lake" | plot.ecocodes == 98 |
                 is.na(plot.ecocodes))
                  #get all NAs and Lake regions
  gdis <- gDistance(pts[ec.na,],ec.subset,byid = T)
                  #calculate distance from na points to each polygon
  gdis[which(ec.subset[[var.extract]] == "Lake"),] <- 99999
                  #calculate distance from na points to each polygon
  gdis[which(ec.subset[[var.extract]] == 98),] <- 99999
                  #exclude lakes by making them seem very far away
  na.ind <- apply(gdis,2,FUN = which.min)
                  #find index of minimum distance for each point
  plot.ecocodes[ec.na] <- ec.subset[[var.extract]][na.ind]
                  #assign back to ecocodes

  return(plot.ecocodes)

} #GetEcoregions
#

## read csv table of code of biome
## 1 Tropical and Subtropical Moist Broadleaf forests
## 2 Tropical and Subtropical Dry Broadleaf forests
## 3 Tropical and Subtropical Coniferous forests
## 4 Temperate Broadleaf and Mixed forests
## 5 Temperate Coniferous forests
## 6 Boreal forests/Taiga
## 7 Tropical and Subtropical Grasslands, Savannas, and Shrublands
## 8 Temperate Grasslands, Savannas, and Shrublands
## 9 Flooded Grasslands and Savannas
## 10 Montane Grasslands and Savannas
## 11 Tundra
## 12 Mediterranean Forests, Woodlands, and Scrub
## 13 Deserts and Weric Shrublands
## 14 Mangroves
## 98 Lake
## 99 Rock and Ice


get.buffer.domi.type <- function(x){
    if (sum(!is.na(x))>0){
    res <- as.numeric(names(sort(table(x), decreasing = TRUE))[1])
    }else{
    res <-  NA
    }
return(res)
}

## get Koppen Geiger climate zone from http://koeppen-geiger.vu-wien.ac.at/present.htm
GetKoppen <-  function(lons,lats) {
 require(raster)
 koppen.raster <- raster('data/raw/Koeppen-Geiger-GIS/WC05_1975H_Koppen_ESRIGrid/wc05_1975h_ko/w001001.adf')
 codes <-
     data.frame(code.ascii = as.numeric(names(sort(table(getValues(koppen.raster))))),
                      let =names(sort(table(as.data.frame(koppen.raster)$KOPPEN))))
 codes <- read.csv("data/raw/Koeppen-Geiger-GIS/codes.csv")
 proj4string(koppen.raster) <- CRS("+init=epsg:4326")
 pts <- SpatialPoints(cbind(lons,lats), proj4string = CRS("+init=epsg:4326"))
 plot.ecocodes <- extract(koppen.raster, pts, na.rm = FALSE)
 if (sum(is.na(plot.ecocodes))>0){
 ec.na <- which(is.na(plot.ecocodes))
                  #get all NAs
 plot.ecocodes.na <- extract(koppen.raster, pts[ec.na, ], na.rm = FALSE,
                             buffer = 80000,
                             fun = get.buffer.domi.type)
 plot.ecocodes[ec.na] <- plot.ecocodes.na
 }
 require(dplyr)
 df <- left_join(data.frame(code.ascii = plot.ecocodes), codes,
             by = 'code.ascii')

 return(as.character(df$let))
}

fun.load.biomes.code <- function(){
code.data <- read.csv("R/utils/biomes.WWF.code.csv", stringsAsFactors = FALSE)
return(code.data)
}
GetBiomes <-  function(Lon, Lat, data = fun.load.biomes.code()){
er <- GetEcoregions(Lon, Lat, var.extract = 'BIOME')
merge.biomes <-  merge(data.frame( code = er), data, by = 'code',
      all.x = TRUE, all.y = FALSE, sort = FALSE)
return(merge.biomes$short_name)
}

GetBiomes.from.wwf <-  function(wwf){
df.descrip <-  read.csv('data/raw/wwf_ecoregion/wwf.descrip.csv', stringsAsFactors = FALSE)
df.biomes <-  fun.load.biomes.code()
merge.biomes.code <-  merge(data.frame( eco_code = wwf), df.descrip, by = 'eco_code',
      all.x = TRUE, all.y = FALSE, sort = FALSE, stringsAsFactors = FALSE)$BIOME
merge.biomes <-  merge(data.frame( code = merge.biomes.code), df.biomes, by = 'code',
      all.x = TRUE, all.y = FALSE, sort = FALSE, stringsAsFactors = FALSE)
return(merge.biomes$short_name)
}
