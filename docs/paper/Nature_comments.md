% Some elements for the reply to the referees of Nature.
% Georges Kunstler


# Referee 1

## Competitive effect is not important for population level competitive outcomes
>***The authors state in the introduction that the existing gap in the
>literature is the translation between species traits and the
>competitive outcomes between species. I could not agree more. But not
>all the metrics tested in this study are drivers of competitive
>outcomes. In most models of competition, competitive outcomes between
>a pair of species will be determined by maximal growth, and tolerance
>of competition, so I am fine with these metrics. I am also fine with
>the authors analysis of competitive effect, but they should
>acknowledge that in nearly all models of competition, competitive
>effect (separated from competitive tolerance) almost never determines
>competitive dominance (i.e. if one species casts deep shade, this does
>not favor that species in competition unless individuals of its
>species can tolerate the shade it casts- that is why tolerance is the
>key trait).***

The core of our analysis is to predict how competition reduces individual tree basal area growth. Our results have implication for population growth and coexistence, but basal area growth is only a part of the population processes. So our results cannot provides definitive answers at the population level. We need to clarify in the main text this distinction between individual basal area growth and population growth level. I propose to have a section at the end of the main text on the population level consequences of our results. In this section we could discuss the point about competitive effect raised by the referee.

Competitive effect is clearly important for the basal area growth. Its
role on population dynamics may be less important, at least in
relatively simple population models but it is unclear whether this
hold true for more complex models. For instance, this is true for the
classical $R^*$, as $R^*$ (which determines the winner) do not vary with
consumption rate. Goldberg 1996 also states the same idea
  for a Mac Arthur consumer-resource model[^Goldberg]. An approach based on the invasion criteria for coexistence would
indicate that the competitive effect of the invader is not important
for his success but the one of the resident maybe. For instance,
Tilman (1990 in Grace book) shows that for alternative more complex R*
models, the parameters related to effect on resource influences the
$R^*$. For size-structured model, the $Z^*$ paper (Adams et al. 2007)
based on the PPA model, provides some information. The response to
light in term of growth and survival are crucial for the competition
outcomes ($Z^*$), but tree height allometry is also important, then
light transmission, and crown radius can also have an influences (all
these last parameters influence competition effect on light).

In this last section on population level consequences, I propose thus to discuss the role of competitive effect along these lines (but short). Then we can have a detail reply for the referee along the ideas presented above showing that there is a tendency for no strong importance of competitive effect, but this may depend of the model. If you have more ideas on this points let me know.


[^Goldberg]:  "Consistent with this argument, using Mac- Arthur's
(1972) consumer-resource equations, J. H. Vandermeer & D. E.  Goldberg
(unpublished results) show that ability of individuals to deplete
resources is irrelevant to the equilibrium outcome of competition for
a single resource and only ability to tolerate low levels of the
resource determines the outcome."

## Need to estimate stabilising niche differences

>***My much bigger concern is with the authors' test of limiting similarity. I think asking whether the trait difference causes the interspecific interaction coefficient to decline is really only part of the problem. The reason we expect limiting similarity in communities is because trait differences translate into the niche differences that stabilize the interaction between competitors. And while the interaction coefficients tested in this study are certainly part of the niche difference, what determines the outcome of competition is the strength of the interspecific interactions relative to the intraspecific interactions, not the interspecific interaction alone.***


This comment as the previous one is focusing on the population level consequence of our results as it focus on coexistence.

The stabilising niche differences as defined by Chesson (and used in Kraft et al. 2014 and Godoy and Levine 2014) can only be evaluated at the population level, so our individual basal area growth will only provides part of the answer. The stabilising niche differences is defined as $\rho = \sqrt{\frac{\alpha_{ij}\alpha_{ji}}{\alpha_{jj} \alpha_{ii}}}$ in Chesson (2012) as the ratio of competition intra- vs inter-specific ($\frac{\alpha_{ij}}{\alpha_{jj}}$) determines the coexistence.

This is related to the classical stabilising vs equalising processes of Chesson. He shows that for a pair of species, the criteria for both of them having positive fitness when rare invader can be related to two key quantities: the stabilising niche overlap (stabilising process: $\rho = \sqrt{\frac{\alpha_{ij}\alpha_{ji}}{\alpha_{jj} \alpha_{ii}}}$) and the fitness differences ($\kappa_i / \kappa_j$ :equalising process in Chesson). The pdf show how this is computed within in Lotka-Volterra model, to see how this connects with our analysis.

As acknowledge by the referee computing that is impossible for our model as it is not a population growth rate model but just an individual tree basal area growth model. So it is not possible to compute the fitness of a rare invader. We can try to apply the same approach instead of computing $\rho$ as the ratio of population level inter intra alpha compute $\rho$ as the ratio of inter intra alpha based on basal area growth (alpha estimated as the basal area growth reduction in intra or inter).

The question is how to estimate these inter intra alpha.

1)	The first solution is to predicted the inter intra alpha based on the traits model we used to predict alpha in our analysis. The attached pdf show that this results to $\rho = e^{-\alpha_s  \vert t_i - t_j \vert}$.
2)	The second solution is to have the same approach but using the
alternative model proposed by the referee 3 where $\alpha_0$ is
divided in $\alpha_{0 intra}$ and $\alpha_{0 inter}$. The attached pdf
show that this results in $\rho = e^{-(\alpha_{0,inter} -
\alpha_{0,intra} + \alpha_s \vert t_j - t_i \vert)}$. This alternative
model show that the stabilising niche process are in fact not related
to the three traits analysed.
3)	Then this sentence of the referee *“if so, only analyse the sample pairs with the best fit parameters”* make me wander if he is not asking to estimate all the $\alpha_{ij}$ separately and then compute $\rho$ for each pairs and model $\rho$ as a function of traits as in Kraft et al (2014) (not Kraft et al. is based on an experiment on annual plant fully replicated per pair of species). This would be almost impossible as the number of co-occurrence between pairs of species can be very low particularly in species rich forest. I have tried at the start of the project to handle that with random effect in $\alpha$ in HB but never get the model to work well. It is not totally clear for me if the results can be very different with this approach. In addition the originality of our approach is to be able to directly relate alpha to traits.

So I propose to add the derivation of $\rho$ based on the approach 1
and 2 above and a figure of this prediction in Suppl Mat. Then in the
main text in the section on population level consequences discuss the
fact that intra/inter and alpha_s show that there is evidence of lower
inter than intra-specific competition for basal area growth, but that this is largely
disconnected from traits. The impact at the population level is more complex  In the reply to the referee explain why estimating the $\alpha_{ij}$ separately is not possible and that our approach is a good solution for that.


## Fitness inequality

> ***Is there a way to show the net effect of trait differences on competitive outcomes? One can certainly do this with a defined model of competition (which I do not expect the authors to have), but it would be nice for the reader to have a sense that though the trait difference per se leads to lower interaction coefficients, the species with the optimal trait value for tolerating competition will still win, or something like that. Otherwise, statements suggesting that the limiting similarity effect is weak are harder to back up.***

The new analysis proposed by the referee 3 with different $\alpha_0$
for intra inter clearly show that the effect of trait similarity is
weak. Estimating the fitness inequality effect on the population
growth rate is not possible as discuss above for niche stabilising process.

I have try to come up with a solution by comparing the growth of species I in a stand of
species J to growth of species J in a stand of species J. This would
be based on the idea that ranking of their growth will influence their
competitive success. The attached pdf show a derivation showing how
this ratio can be decomposed in a function of $\rho$ and basal area
and quantity related to growth differences (in function the basal
area). The issue is that this growth ratio changes with the the amount of
basal area of competitor considered. So I'm not planning to include
that in the MS.

## Hmax

> ***In Fig. 2, taller plants do not seem to have greater competitive effects than shorter plants. Given the general sense in the literature that light competition is important in forests worldwide, I would spend some time explaining this. Similarly, what could explain limiting similarity based on maximum height (light competition does not lend itself to niche differentiation)?***

I propose to add some lines on that in the main text. In an analysis on short-term effect of competition on tree growth with a competition index that is already accounting for tree size this is not surprising that there is few effect of Hmax. However in a size-structured population model this maximum height would have a strong effect, on the population competitive interaction, see the Z* from the PPA.

For the limiting similarity effect of Hmax this is less clear. The
fact that the limiting similarity effect shrink when $\alpha_0$ is
divided in intra inter support the idea that this effect is not
directly related to the trait similarity but to its correlation to
intra inter and that processes such as species specific pathogen and
herbivore may be the key.


# Referee 2

# HB vs ML

>***My main comment relates to the modelling approach taken. The authors use linear mixed models to fit the parameter. It seems that a hierarchical Bayes approach could be more suitable here, as followed by Lasky et al. (2014) who addressed similar questions, but focused on tree mortality rather than growth. Can the authors provide justification for the approach used?***

 In my view, the main arguments for using HB is when it is impossible
 to do that in ML (for instance because we need to use non linear
 form, complex error distribution, ...), or it is much more easy to
 get confidence interval (the estimate of the confidence interval may
 vary between ML and HB as there is no normal assumption in
 HB). Estimating the same model in maximum likelihood (ML) or
 hierarchical Bayesian (HB) with vague prior should give
 (asymptotically) exactly the same results (uniformative normal prior
 with normal error distribution are conjugate and gives normal
 posterior). In our case I did try to fit the model in HB (jags or
 stan) but never get a good convergence with the full data. With a
 subsample of data ML and HB estimation result in almost identical
 results (see figure below), but I’m reluctant to add that in Suppl Mat.

![Comparison of jags vs lmer estimates of the parameters for the simple global model.](../../figs/lmer_vs_jags.pdf)

\pagebreak

>***line 114-122: this is essentially the framework presented in Lasky
>et al. (2014) in a similiar study on tree survival, but that
>reference (#10) is one of those mentioned in this section.***

I clarified that in the method and added this footnote.

"There has been different approach to model $\alpha$ from traits. In one of the first study Uriarte et al. (2010) modelled $\alpha$ as $\alpha =
\alpha_0 + \alpha_s \vert t_f-t_c \vert$. Then in Kunstler et al. (2012) used two different models: $\alpha = \alpha_0 + \alpha_s \vert t_f-t_c \vert$ or $\alpha =
\alpha_0 + \alpha_h ( t_f-t_c )$. Finally, Lasky et al. (2014) developped one single model inculding multiple-processes as $\alpha =
\alpha_0 + \alpha_t t_f +\alpha_h ( t_f-t_c ) + \alpha_s \vert t_f-t_c
\vert$. In this study, we extended this last model by considering that it was more clear to split
$\alpha_h (t_f - t_c)$ in $\alpha_t t_f + \alpha_e t_c$, which is
equivalent to the hierarchical distance if $\alpha_t = - \alpha_e$
(thus avoiding replication of $t_f$ effect through both $\alpha_h$ and
$\alpha_t$)."


## Succession

>***line 125-128: What successional stages of forests were included in
>this study? Only old-growth forests, The Luquillo plot is partially
>second-growth forests and I suspect this is also the case for some of
>the Japan plots. As the results focus on successional trade-offs,
>this aspect is very important to mention and account for in the
>results. Successional state of the forest will substantially affect
>tree growth rates and levels of neighborhood competition.***


Most of these forests are probably experiencing successional dynamics
due to some disturbance. We don't have quantitative information to
support that for all data set. We added
field evidences of disturbances and succession dynamics in the
presentation of the data set in the Suppl Mat and also age
distribution of the forest for several data set where it is available
(see below).

####Panama

Gap disturbances are common in the large 50ha BCI plot [see @Young-1991; @Hubbell-1999; @Lobo-2014]. Hubbell et al.[@Hubbell-1999] estimated that less than 30% of the plot experienced no disturbance over a 13-year period.


####Japan

The network of plot comprise 50% of old growth forest, 17% of old secondary forest and 33% of young secondary forest.


####Luquillo

The plot has been struck by hurricanes in 1989 and in 1998[@Uriarte-2009]. In addition, two-third of the plot is a secondary forest on land previously used for agriculture and logging[Uriarte-2009].


####M'Baiki

The plot network was established with three levels of harvesting and one control [@Ouedraogo-2013].


####Fushan

Fushan experienced several Typhoon disturbances in 1994 with tree fall events, the main effect was trees defoliation[@Lin-2011].


####Paracou

The plot network was established with three levels of harvesting and one control (Herault et al. 2010).


####France

French forests monitored by the French National Forest Inventory experience several types of natural disturbances (such as wind, forest fire, and bark beetles) and harvesting. The age structure reconstructed by Vilen et al.[@Vilen-2012] shows that young forests represents a significant percentage of the forested area (see age distribution below).


####Spain

Spanish forests monitored by the Spanish National Forest Inventory experience several types of natural disturbances (such as wind, forest fire, and bark beetles) and harvesting. No data are available on the age structure of the plots."""


####Swiss

Swiss forests monitored by the Swiss National Forest Inventory experience several types of natural disturbances (such as wind, forest fire, and bark beetles) and harvesting. The age structure reconstructed by Vilen et al.[@Vilen-2012] shows that young forests represents a significant percentage of the forested area (see age distribution below)."""


####Sweden

Swedish forests monitored by the Swedish National Forest Inventory experience several types of natural disturbances (such as wind, forest fire, and bark beetles) and harvesting. The age structure reconstructed by Vilen et al.[@Vilen-2012] shows that young forests represents a significant percentage of the forested area (see age distribution below)."""


####USA

US forests monitored by the FIA experience several types of natural disturbances (such as wind, forest fire, and bark beetles) and harvesting. The age structure reconstructed by Pan et al.[@Pan-2011] shows that young forests represents a significant percentage of the forested area (see age distribution below).


#### Canada

Canadian forests monitored by the regional forest monitoring programs experience several types of natural disturbances (such as wind, forest fire, and bark beetles) and harvesting. The age structure reconstructed by Pan et al.[@Pan-2011] shows that young forests represents a significant percentage of the forested area (see age distribution below).


#### NZ

New Zealand forests are experiencing disturbance by earthquake,
landslide, storm and volcanic eruptions. According to Holdaway et
al.[@Holdaway-2014] having been disturbed during their measurement
interval.


#### NSW

The plot network was initially established in the 60s with different level of selection harvesting[@Kariuki-2006].


\pagebreak

#### Age distribution of some forest data analysed.


![Age distribution of forest area in 20-year age class for France, Switzerland and Sweden, estimated by Vilen et al.[@Vilen-2012]. The last class plotted at 150 years is for age > 140 years (except for Sweden where the last class 110 is age > 100 years).](../../figs/age_europe.pdf)

![Age distribution of forest area in 20-year age class for North America (USA and Canada), estimated by Pan et al.[@Pan-2011]. The last class plotted at 150 years is for age > 140 years.](../../figs/age_na.pdf)

I addition will add a sentence on the main text to state this idea.

\pagebreak

# Referee 3

## Intra Inter


>***As I understand the methods, conspecifics and heterospecifics are distinguished only on the basis of their traits. That means that a plot dominated by conspecifics will have very high trait similarity. As a result, estimates of the trait similarity effect, alpha_s, may not reflect trait differences as much as the relative abundance of conspecifics (and the many sources of conspecific density dependence unrelated to functional traits). I think the solution may be straightforward: replace alpha_0 with two terms, an alpha_0,conspecific and an alpha_0,heterospecific. The remaining alpha terms should only reflect the influence of heterospecific neighbors (the trait similarity calculation should not include the conspecific neighbors). An alternative would be to keep the current terms but add a term for relative abundance of conspecifics. However, this covariate will probably be highly correlated with the trait similarity covariate.***

>***If this alternative model gives qualitatively similar results to the original model, it could be described only in the supplementary information. But I suspect that the estimates of alpha_s from this model will be much smaller than in the current model. This result would actually strengthen the main argument of the paper, which states that the limiting similarity effects are much weaker than the competitive impact/response effects. To my eye, this difference does NOT appear clearly in Fig. 2, and the argument seems like a stretch. In fact, for maximum height, alpha_s is the only effect with a confidence interval that does not overlap zero. Again, I expect this result will change once the conspecific-heterospecific contrast is disentangled from the trait similarity effects.***

The results of the new analysis proposed by the referee 3 is presented below.


![Result with alpha0 for intra inter (model including MAT and MAP see below).](../../figs/figres12_TP_intra.pdf)

As guessed by the referee the trait similarity effect disappear. My
proposition is to still keep the original results in the main text and
add this figure in Extended data. Then in the main text use this
additional figure to explain that the traits similarity is not related
to a direct effect of traits similarity but only to the distinction
between intra inter (and thus processes independant of the traits).

In my view most field and theorethical studies have approached the
effect of traits similarity by lumping together intra- inter (but see
Scheffer and Van Nes (2006 PNAS) for a theorethical example of intra
inter competition kernel), so this important to keep this more
classical analysis, but use the intra inter one to show the meaning of $\alpha_s$.

## Interactions between environment and competition

> ***Jim Clark's recent papers on high-dimensional coexistence and Simpson's paradox argue that simple trade-offs should be elusive because of the importance of complex interactions between species (or traits), competition, and the environment which operate at the level of the individual. Yet the main result of this paper is amazing consistency in trait-mediated trade-offs. It is surprising to me that the results are not more context-dependent. Why don't certain environments favor some trait values, while different environments favor the opposite values? If nothing else, this lack of context dependence should be a discussion point. But in order to accept the authors' interpretation without reservations I would like some reassurance that the aggregated responses featured in this paper are not masking more complex interactions.***

>***Let me make two observations about the analysis that are related to this issue. First, the analysis makes the implicit assumption that differences in neighborhood composition (i.e. high vs low competition) within a data set are not related to the environment. In other words, if one individual tree is growing in a dense neighborhood and another tree is growing in an uncrowded neighborhood, that difference must reflect disturbance history and not some unobserved difference in abiotic factors, such as shallow soils that limit productivity on the uncrowded site. This assumption is the basis for the argument that the trait-mediated trade-offs are acting on variation in successional status. Couldn't the same argument be made for other more static, less dynamic sources of spatial heterogeneity?***

This is a correlative study, so we cannot be certain whether these are
direct mechanistic effects of the traits, versus effects of something
else that is correlated with the traits. We included a random plot
effect in Gmax to account for spatial variability in local
productivity.

I re-ran a model where the direct effect of MAT and MAP was
include, to show that our results are robust to accounting for
additional abiotic variables (see the
following figure).

![Result with MAT and MAP effect on Gmax.](../../figs/figres12_TP.pdf)

These new results are very close to the previsous one, the main
differences are that the CI of direct effect of SLA intercept zero (it
was very close before) and that the CI of the competitive effect of wood density
do not intercept zero any more, it was close to zero before. I rpopose
to use this new analysis with MAT MAP. This will require some
modification of the text to account for that (and change the figure
for the trade-off as it occur now only for wood density and not any
more for SLA).
<!-- Then we can add a figure with a boxplot of the total basal area per -->
<!-- plot/quadrat for each data set, as presented below to show that we -->
<!-- have a large range in all data set, going close to -->
<!-- zero. -->

<!-- ![Boxplot of total basal area per data set.](../../figs/boxplot_batot_set.pdf) -->

In addition the new informations on disturbance and forest age in
Suppl Mat provided for the referee 2 support a strong role of
disturbance and succession dynamics in most forests.

## Random effects

>***My second, related observation has to do with the structure of the random effects. The model does include random effects for species, plot and data set on the maximum growth rate. This accounts for latent environmental variation among plots that could influence intrinsic growth rates, but not in the effects of competition. There are also random effects on competitive effect and tolerance at the level of the data set, but not plot. So the assumption is that competition will operate in a consistent way across plots within a data set. As a result, I think it would be impossible to observe trait by environment interactions; competition on low neighborhood plots is assumed to operate the same way that it does on more crowded plots.***

The analysis with a fixed bionmes effect is already exploring this
question of variation of the competitive effect between biomes, but
this is at a large scale. The proposition of the referee of including
a plot random effect in the competition parameters is not possible as to few
plot have multiple census with large variation of local basal area
between census to
estimate an alpha per plot. I re-ran an analysis where we added a random effect
in the alphas parameters with both data set and local ecoregion
(Koppen-Geiger climatic zones which is much more narrow than the
biomes from Kriticos, D.J., Webber, B.L., Leriche, A., Ota, N.,
Macadam, I., Bathols, J. & Scott, J.K. (2012) CliMond: global high
resolution historical and future scenario climate surfaces for
bioclimatic modelling. Methods in Ecology & Evolution 3: 53-64.). The
results below show that the mean effect is similar.

![Result with koppen x data set random effect in $\alpha$ parameters.](../../figs/figres12_ecocode_TP.pdf)

I'm not sure if we need to add this in Suppl Mat or just in the reply
to the referee.

\pagebreak

>My wandering argument should make it clear that I have not thought
>this all the way through. I am just worried that the conclusions of
>the analysis might be sensitive to these kinds of assumptions. I
>would be appeased by more information on model selection. Are models
>with plot level random effects on the competition terms any better?
>If model convergence is a problem, perhaps this alternative model
>could be tested on the richest data sets? This type of model
>comparison could offer additional evidence for the impressive lack of
>context dependence in the results.

I propose to add a few sentence highlighting this surpising lack of
contex dependence and proposing that this may be related to the
dominant effect of competition for light in all these biomes.

