## # Extend data

## ```{r options-chunk, echo = FALSE, results = 'hide', message=FALSE}
## opts_chunk$set(dev= c('pdf','svg'), fig.width= 10, fig.height = 5)
## ```


## ![**Map of the plot locations of all data sets analysed.** LPP plots are represented with a large points and NFI plots with small points (The data set of Panama comprises both a 50ha plot and a network of 1ha plots). World map is from the R package *rworldmap*[^South].](../../figs/world_map.pdf)

## [^South]: South, A. Rworldmap: A new r package for mapping global data. The R Journal 3, 35–43 (2011).


## \newpage


##+ This deals with some path issues, echo = FALSE, results = 'hide'
git.root <- function() {
    system("git rev-parse --show-toplevel", intern=TRUE)
}
source.root <- function(filename) {
    source(file.path(git.root(), filename), chdir=TRUE)
}
readRDS.root <- function(filename) {
    readRDS(file.path(git.root(), filename))
}

##+ Load script, echo = FALSE, results = 'hide', message=FALSE
path.root <- git.root()


## # Data description

##+  kable2, echo = FALSE, results="asis", message=FALSE
library(pander)
data.set <-read.csv(file.path(path.root, 'output', 'data.set.csv'), stringsAsFactors = FALSE)
dat.2 <- data.set[, -(2)]
dat.2[dat.2$set == 'NVS',1] <- 'New Zealand'
dat.2[dat.2$set == 'NSW',1] <- 'Australia'
dat.2[dat.2$set == 'Swiss',1] <- 'Switzerland'
dat.2[dat.2$set == 'BCI',1] <- 'Panama'
dat.2[dat.2$set == 'Fushan',1] <- 'Taiwan'
dat.2[dat.2$set == 'Luquillo',1] <- 'Puerto Rico'
dat.2[dat.2$set == 'Mbaiki',1] <- 'Central African Republic'
dat.2[dat.2$set == 'Paracou',1] <- 'French Guiana'

var.names <- colnames(dat.2)
var.names[2] <- '# of trees'
var.names[3] <- '# of species'
var.names[4] <- '# of plots/quadrats'
var.names[5] <- '% of angiosperm'
var.names[6] <- '% of evergreen'
var.names[7] <- '% cover Leaf N'
var.names[8] <- '% cover Seed mass'
var.names[9] <- '% cover SLA'
var.names[10] <- '% cover Wood density'
var.names[11] <- '% cover Max height'
colnames(dat.2) <-  var.names
dat.2 <-  as.data.frame(dat.2)
rownames(dat.2) <-  NULL
dat.2 <- dat.2[, 1:11]
dat.2[,5:11] <- dat.2[,5:11]*100

pandoc.table(dat.2[, 1:6],
             caption = "**Trees data description.** For each site is given the number of individual trees, species and plots in NFI data and quadrats in LPP data, and the percentage of angiosperm and evergreen species.",
             digits = c(3,3,3,0,0), split.tables = 200, split.cells = 35,
             justify = c('left', rep('right', 5)), keep.trailing.zeros = TRUE)

pandoc.table(dat.2[, c(1,9:11)],
             caption = "**Traits data description.** The coverage in each site is given with the percentage of species with species level trait data.",
             digits = 1, split.tables = 200, split.cells = 25,
             justify = c('left', rep('right', 3)),
             keep.trailing.zeros = TRUE)



##+ Describe data, echo = FALSE, results = 'hide', message=FALSE
source.root("R/analysis/lmer.output-fun.R")
source.root("R/analysis/lmer.run.R")
source.root("R/utils/plot.R")
library(pander)

## ## Species traits correlation

##+ Read Table_cor, echo = FALSE, results = 'hide', message=FALSE
cor.mat <-  read.csv(file.path(path.root,'output',
                               'formatted', 'cor.mat.traits.csv'),
                     row.names = 1)
cor.mat <- round(cor.mat,3)
cor.mat[is.na(cor.mat)] <-  ''
colnames(cor.mat)  <- row.names(cor.mat) <-  c("Wood density", "SLA",
                                               "Max height")
##+ Table1_cor, echo = FALSE, results='asis', message=FALSE
pandoc.table(cor.mat,
             caption = "**Pairwise functional trait correlations**. Pearson's r correlations for the three traits.",
             digits = 3)

## \newpage

## # Model results

## ![**Variation of trait-independent inter and intraspecific competition, trait dissimilarity ($|t_f - t_c| \, \alpha_d$), competitive effect ($t_c \, \alpha_e$), tolerance to competition ($t_f \, \alpha_t$) and maximum growth ($t_f \, m_1$) with wood density (respectively a, b, c, d and e), specific leaf area (respectively f, g, h, i and j) and maximum height (respectively k, l, m, n and o).** Trait varied from their quantile at 5\% to their quantile at 95\%. The shaded area represents the 95% confidence interval of the prediction (including uncertainty associated with $\alpha_0$ or $m_0$). $\alpha_{0 \, intra}$ and $\alpha_{0 \, inter}$, which do not vary with traits are represented with their associated confidence intervals.](../../figs/figres4b_TP_intra.pdf)


## ![**Average difference between interspecific and intraspecific competition predicted with estimates of trait-independent and trait-dependent processes influencing competition for models fitted for wood density (a), specific leaf area (b) or maximum height (c).** The average differences between interspecific and intraspecific competition are influenced by $\alpha_{0 \, intra}$, $\alpha_{0 \, inter}$ and $\alpha_d$ coefficients (see Extended Methods for details). Negative value indicates that intraspecific competition is stronger than interspecific competition.](../../figs/rho_set_TP_intra.pdf)

## ![**Trait-dependent and trait-independent effects on
## maximum growth and competition across the globe and their variation among biomes for models without separation of $\alpha_0$ between intra and interspecific competition for wood density (a), specific
## leaf area (b) and maximum height (c).** See Figure 2 in the main text for parameters description and see Fig 1a in the main text for biome definition.](../../figs/figres12_TP.pdf)




##+ ComputeTable_Effectsize, echo = FALSE, results = 'hide', message=FALSE
list.all.results <-
    readRDS.root('output/list.lmer.out.all.NA.intra.set.rds')

mat.param <- do.call('cbind',
               lapply(c('Wood.density', 'SLA', 'Max.height'),
                   extract.param, list.res = list.all.results,
                   model = 'lmer.LOGLIN.ER.AD.Tf.MAT.MAP.intra.r.set.species',
                   param.vec = c("(Intercept)", "logD","Tf", 'MAT', 'MAP',
                                 "sumBn.intra","sumBn.inter",
                                 "sumTnBn","sumTfBn", "sumTnTfBn.abs"),
                   data.type = 'intra'))

mat.param[!row.names(mat.param) %in% c("(Intercept)", "logD",
                                       "Tf", 'MAT', 'MAP', "sumTfBn"),] <-
    -mat.param[!row.names(mat.param) %in% c("(Intercept)", "logD", "Tf",
                                            'MAT', 'MAP',
                                             "sumTfBn"),]

mat.param.sd <- do.call('cbind',
                lapply(c('Wood.density', 'SLA', 'Max.height'),
                    extract.param.sd, list.res = list.all.results,
                    model = 'lmer.LOGLIN.ER.AD.Tf.MAT.MAP.intra.r.set.species',
                    param.vec = c("(Intercept)", "logD", "Tf",'MAT', 'MAP',
                                  "sumBn.intra","sumBn.inter",
                                  "sumTnBn","sumTfBn", "sumTnTfBn.abs"),
                    data.type = 'intra'))

mat.R2c <- do.call('cbind',
              lapply(c('Wood.density', 'SLA', 'Max.height'),
                   extract.R2c, list.res = list.all.results,
                   model = 'lmer.LOGLIN.ER.AD.Tf.MAT.MAP.intra.r.set.species',
                   data.type = 'intra'))
mat.R2m <- do.call('cbind',
              lapply(c('Wood.density', 'SLA', 'Max.height'),
                   extract.R2m, list.res = list.all.results,
                   model = 'lmer.LOGLIN.ER.AD.Tf.MAT.MAP.intra.r.set.species',
                   data.type = 'intra'))
mat.AIC <- do.call('cbind',
              lapply(c('Wood.density', 'SLA', 'Max.height'),
                   extract.AIC, list.res = list.all.results,
                   model = 'lmer.LOGLIN.ER.AD.Tf.MAT.MAP.intra.r.set.species',
                   data.type = 'intra'))
mat.AIC.0 <- do.call('cbind',
                     lapply(c('Wood.density', 'SLA', 'Max.height'),
                            extract.AIC, list.res = list.all.results,
                            model = 'lmer.LOGLIN.MAT.MAP.intra.r.set.species',
                            data.type = 'intra'))

bold.index <- which(((mat.param - 1.96*mat.param.sd) >0 & mat.param > 0) |
                    ((mat.param + 1.96*mat.param.sd) <0 & mat.param <0),
                    arr.ind = TRUE)
mat.param.mean.sd <- matrix(paste0(round(mat.param, 3),
                            ' (',
                            round(mat.param.sd, 3),
                            ')'), ncol = 3)
mat.param <- rbind(mat.param.mean.sd,
                   round(mat.R2m, 4),
                   round(mat.R2c, 4),
                   round(mat.AIC- apply(rbind(mat.AIC,mat.AIC.0), MARGIN = 2, min), 0),
                   round(mat.AIC.0- apply(rbind(mat.AIC,mat.AIC.0), MARGIN = 2, min), 0))
colnames(mat.param) <- c('Wood density', 'SLA', 'Maximum height')
row.names(mat.param) <-  c('$m_0$', '$\\gamma$', '$m_1$', '$m_2$','$m_3$',
                           '$\\alpha_{0 \\, intra}$','$\\alpha_{0 \\, inter}$',
                           '$\\alpha_e$', '$\\alpha_t$',
                           '$\\alpha_d$', '$R^2_m$*', '$R^2_c$*',
                           '$\\Delta$ AIC', '$\\Delta$ AIC no trait')

##+ Table2_Effectsize, echo = FALSE, results='asis', message=FALSE
pandoc.table(mat.param[c(1:14), ], caption = "**Standardized coefficient estimates from models fitted for each traits.**  Estimates and standard error (in bracket) estimated for each trait, $R^2$* of models and $\\Delta$ AIC of the model and of a model with no trait effect. Best model have a $\\Delta$ AIC of zero. See section Method for explanation of parameters",
             digits = 3,  justify = c('left', rep('right', 3)),
             emphasize.strong.cells = bold.index, split.tables = 200)

## \* We report the conditional and marginal $R^2$ of the models using the methods of reference[^1], modified by reference[^2]. $\Delta$ AIC is the difference in AIC between the model and the best model (lowest AIC). AIC is the Akaike's Information Criterion (as defined by reference[^3]), and the best-fitting model was identified as the one with a $\Delta$ AIC of zero. $\Delta$ AIC greater than 10 shows strong support for the best model^3^.

## [^1]: Nakagawa, S. & Schielzeth, H. A general and simple method for obtaining R2 from generalized linear mixed-effects models. Methods in Ecology and Evolution 4, 133–142 (2013).
## [^2]: Johnson, P. C. D. Extension of Nakagawa and Schielzeth’s R2GLMM to random slopes models. Methods in Ecology and Evolution 5, 944–946 (2014).
## [^3]: Burnham, K. P. & Anderson, D. R. Model selection and multimodel inference: A practical information-theoretic approach. (Springer-Verlag, New-York, 2002).


