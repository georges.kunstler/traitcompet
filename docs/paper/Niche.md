% Derivation of $\rho$.
<!-- % Georges Kunstler -->



# Appendix derivation of $\rho$

To start, I will describe briefly what was
the approach of Kraft et al. (2014) based on Chesson to see how our analysis can
fit with that.

### The stabilising niche similarity (and equalising) of Chesson 2012

The approach of Chesson (2000, 2012) and then Kraft et al. (2015) (and many
other, see Godoy and Levine 2014) to estimate the stabilising niche difference and average fitness
difference rely on estimating the **population** growth rate of a rare
invader (species $i$) in an equilibrium community of a resident
species (species $j$). To understand this approach the simplest
solution is to take the Lotka-Volterra equation (as presented in
Godoy and Levine 2014 Ecology):

\begin{equation}
\frac{dN_i}{dt} =  N_i \times r_i \times (1 - \alpha_{ii} N_i -
\alpha_{ij} N_j)
\end{equation}

The criteria for invasion of species $i$ in resident community of species $j$ is (at equilibrium population of single resident $j$ is
$\overline{N_j} = \frac{1}{\alpha_{jj}}$):

\begin{equation}
(1 - \frac{\alpha_{ij}}{\alpha_{jj}})
\end{equation}

Thus if $\frac{\alpha_{ij}}{\alpha_{jj}} <1$ invasion $i$ in $j$ is possible (same approach for $j$ in $i$).

The stabilising niche process for species $i$ in regard to species $j$ is thus defined as
$\frac{\alpha_{ij}}{\alpha_{jj}}$. Then Chesson (2012) split the likelihood of coexistence in
two processes: stabilising processes and equalising processes. For that Chesson define the average
stabilising niche
overlap between species $i$ and $j$ (both way) as:

\begin{equation}
\rho = \sqrt{\frac{\alpha_{ij} \alpha_{ji}}{\alpha_{jj} \alpha_{ii}}}
\end{equation}

The average fitness difference measuring the equalising processes ($\frac{\kappa_j}{\kappa_i}$) is
then:

\begin{equation}
\frac{\alpha_{ij}}{\alpha_{jj}} = \frac{\kappa_j}{\kappa_i} \, \rho
\end{equation}

Thus $\frac{\kappa_j}{\kappa_i} = \sqrt{\frac{\alpha_{ij} \alpha_{ii}}{\alpha_{jj} \alpha_{ji}}}$

With this approach coexistence of $i$ and $j$ requires that :

\begin{equation}
\frac{\alpha_{ij}}{\alpha_{jj}} = \frac{\kappa_j}{\kappa_i} \, \rho <1
\end{equation}

and

\begin{equation}
\frac{\alpha_{ji}}{\alpha_{ii}} = \frac{\kappa_i}{\kappa_j} \, \rho <1
\end{equation}

thus:

\begin{equation}
\rho < \frac{\kappa_i}{\kappa_j} < \frac{1}{\rho}
\end{equation}


#### Derivation of $\rho$ for our basal area growth model

Our growth model is (see extended method for details):
\begin{equation}
G_{i,f,p,s,t} = G_{\textrm{max} \, f,p,s} \, D_{i,f,p,s,t}^{\gamma_f} \,  e^{\left(\sum_{c=1}^{N_i} {-\alpha_{c,f} B_{i,c,p,s}}\right)}
\end{equation}

Clearly we can not use an approach based on the fitness of a rare
invader, as our model is not a population growth model. What we can do
is compare the basal area growth reduction by intra- vs inter-specific
competition[^note1].
The growth rate reduction via competition for a unit of competitor basal area[^scale] in our growth model can be defined by

\begin{equation}
e^{-\alpha_{c,f}}
\end{equation}

[^scale]: As we used scaled variable for the basal area of competitor this is not in $m^2/ha$ but this doesn't matter.

Following Chesson (2012) we can define $\rho$ as[^note1]:

\begin{equation}
\rho = \sqrt{\frac{e^{-\alpha_{ij}} e^{-\alpha_{ji}}}{e^{-\alpha_{jj}} e^{-\alpha_{ii}}}}
\end{equation}

[^note1]: But the meaning of this $\rho$ is not as clear because it is not based on the population growth of a rare invader.

If we develop our model of link between $\alpha$ and the traits within
the interspecific competition we get:
\begin{equation}
e^{\alpha_{i,j}} = e^{-(\alpha_{0,i} + \alpha_t t_i + \alpha_e t_j +
\alpha_s \vert t_i - t_j \vert)}
\end{equation}

For intra-specific competition we get:
\begin{equation}
e^{\alpha_{i,i}} = e^{-(\alpha_{0,i} + \alpha_t t_i + \alpha_e t_j)}
\end{equation}

Thus $\rho$ simplify to

\begin{equation}
\rho = \sqrt{\frac{e^{-\alpha_{ij}} e^{-\alpha_{ji}}}{e^{-\alpha_{jj}}
e^{-\alpha_{ii}}}} = e^{-\alpha_s  \vert t_i - t_j \vert}
\end{equation}

So the stabilising niche similarity $\rho$ is directly related to $\alpha_s$. Whereas the referee 1 was not convince by that.

## Intra- *vs.* inter-specific competition

The referee 3 is asking to split the parameter $\alpha_0$ in two: 1)
one related to intra-specific competition $\alpha_{0,intra}$ and 2) one related to the inter-specific competition $\alpha_{0,inter}$. This point is raised because he is concern that a large proportion of the $\alpha_s$ parameter could be related only to the difference between intra- *vs* inter-specific competition rather than the traits similarity.

This approach will consider that intra-specific competition is:

\begin{equation}
\alpha_{f,f} = \alpha_{0,intra} + \alpha_t \, t_f + \alpha_e \, t_f
\end{equation}

And inter-specific competition is:

\begin{equation}
\alpha_{f,c} = \alpha_{0,inter} + \alpha_t \, t_f + \alpha_e \, t_c + \alpha_s \, \vert t_c - t_f \vert
\end{equation}


Thus this would require to have competition index splitting conspecific and heterospecific.

\begin{equation}
\sum_{c=1}^{N_i} (\alpha_{f,c} B_c)  = \alpha_{0,intra} B_f + \alpha_{0,inter} \sum_{c \neq f} B_c + \alpha_t t_f \sum_{c=1}^{N_i}(B_c) + \alpha_e \sum_{c=1}^{N_i}(t_c B_c) + \alpha_s  \sum_{c \neq f}(\vert t_c - t_f \vert B_c)
\end{equation}


If we plug this into the estimation of the $\rho$ presented above this gives:

\begin{equation}
\rho = e^{-(\alpha_{0,inter} - \alpha_{0,intra} + \alpha_s \vert t_j - t_i \vert)}
\end{equation}



#### Estimation of fitness inequality

The idea would be to estimate what is driving species growth rate
differences and if these differences attenuate in inter-specific
rather than in intra-specific competition for two species $i$ and $j$.

The growth of a single tree of size ($D$) and species $i$ in a community of species $j$ with a given basal area ($B_j$) is given by:

\begin{equation}
G_{i \, j} = G_{\textrm{max} \, i} \, D^{\gamma_i} \,  e^{-\alpha_{i,j} B_j}
\end{equation}

For $G_{j,j}$ we just need to replace $i$ per $j$ for the growth of species $j$ in the same community. Then the ratio of these to growth (developing the $\alpha_{i,j}$ as in the previous approach) is:

\begin{equation}
\frac{G_{i \, j}}{G_{j \, j}} = \frac{G_{\textrm{max} \, i}}{G_{\textrm{max} \, j}}  \,  e^{- \alpha_t (t_j - t_i) B_j} \, e^{- \alpha_s \vert t_j- t_i \vert B_j}
\end{equation}

Fitness inequality part would be defined by:

\begin{equation}
e^{m_1 (t_i-t_j) -(\alpha_t (t_i - t_j)) B_j}
\end{equation}

The problem is that this is a function $B_j$ the local basal area of competitor.

The stabilising process part would be related to $\rho$ defined above:

\begin{equation}
e^{- \alpha_s \vert t_j- t_i \vert B_j}
\end{equation}

Note that as pointed out by the referee 1 the parameter for
competitive effect $\alpha_e$ do not appears in any of this
equations. But this doesn't mean that it will have no impact on the
population dynamics.


## References

* Chesson, P. 2000. Mechanisms of maintenance of species diversity. -
  Annual Review of Ecology, Evolution, and Systematics 31: 343–366.

* Chesson, P. 2012. Species competition and predation. - In:
  Ecological Systems. Springer, pp. 223–256.

* Godoy, O. and Levine, J. M. 2014. Phenology effects on invasion success: insights from coupling field experiments to coexistence theory. - Ecology 95: 726–736.

* Kraft, N. J. B. et al. 2015. Plant functional traits and the multidimensional nature of species coexistence. - Proceedings of the National Academy of Sciences 112: 797–802.

