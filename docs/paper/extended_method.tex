\documentclass[a4paper,11pt]{article}
\usepackage{lmodern}
\usepackage{amsmath}
\usepackage{ifxetex,ifluatex}
\usepackage{fixltx2e} % provides \textsubscript
\ifnum 0\ifxetex 1\fi\ifluatex 1\fi=0 % if pdftex
  \usepackage[T1]{fontenc}
  \usepackage[utf8]{inputenc}
\else % if luatex or xelatex
  \ifxetex
    \usepackage{mathspec}
    \usepackage{xltxtra,xunicode}
  \else
    \usepackage{fontspec}
  \fi
  \defaultfontfeatures{Mapping=tex-text,Scale=MatchLowercase}
  \newcommand{\euro}{€}
\fi
\usepackage{ms}
% use upquote if available, for straight quotes in verbatim environments
\IfFileExists{upquote.sty}{\usepackage{upquote}}{}
% % use microtype if available
% \IfFileExists{microtype.sty}{%
% \usepackage{microtype}
% \UseMicrotypeSet[protrusion]{basicmath} % disable protrusion for tt fonts
% }{}

\usepackage[numbers,super,sort&compress]{natbib}
\ifxetex
  \usepackage[setpagesize=false, % page size defined by xetex
              unicode=false, % unicode breaks when used with xetex
              xetex]{hyperref}
\else
  \usepackage[unicode=true]{hyperref}
\fi
\hypersetup{breaklinks=true,
            bookmarks=true,
            pdfauthor={Kunstler},
            pdftitle={Methods},
            colorlinks=true,
            citecolor=blue,
            urlcolor=blue,
            linkcolor=magenta,
            pdfborder={0 0 0}}
\urlstyle{same}  % don't use monospace font for urls
\setlength{\parindent}{0pt}
\setlength{\parskip}{6pt plus 2pt minus 1pt}
\setlength{\emergencystretch}{3em}  % prevent overfull lines
\setcounter{secnumdepth}{0}

\usepackage{fancyhdr}
\pagestyle{fancy}
\rhead{Methods}
\title{Methods}
\date{}

\begin{document}
\maketitle

\section{Model and analysis}\label{model-and-analysis}

To examine the link between competition and traits we used a
neighbourhood modelling
framework\citep{Canham-2006, Uriarte-2010, Ruger-2012, Kunstler-2012, Lasky-2014}
to model the growth of a focal tree of species \(f\) as a product of its
maximum growth (determined by its traits and size) together with
reductions due to competition from individuals growing in the local
neighbourhood (see definition below). Specifically, we assumed a relationship of the form

\begin{equation} \label{G1}
G_{i,f,p,s,t} = G_{\textrm{max} \, f,p,s} \, D_{i,f,p,s,t}^{\gamma_f} \,  \exp\left(\sum_{c=1}^{N_i} {-\alpha_{f,c} B_{i,c,p,s}}\right),
\end{equation}
where:
\begin{itemize}
\itemsep1pt\parskip0pt\parsep0pt
\item
  \(G_{i,f,p,s,t}\) and \(D_{i,f,p,s,t}\) are the annual basal area
  growth and diameter at breast height of individual \(i\) from species
  \(f\), plot or quadrat (see below) \(p\), data set \(s\), and census $t$,
\item
  \(G_{\textrm{max} \, f,p,s}\) is the maximum basal area growth for species \(f\) on plot or quadrat \(p\) in data set \(s\), i.e.~in
  absence of competition,
\item
  \(\gamma_f\) determines the rate at which growth changes with size for
  species \(f\), modelled with a normally distributed random effect of
  species \(\varepsilon_{\gamma, f}\) {[}as
  \(\gamma_f = \gamma_0 + \varepsilon_{\gamma, f}\) where
  \(\varepsilon_{\gamma, f} \sim \mathcal{N} (0,\sigma_{\gamma})\) -- a
  normal distribution of mean 0 and standard deviation $\sigma_{\gamma}${]}
\item
  \(\alpha_{f,c}\) is the per unit basal area effect of individuals from
  species \(c\) on growth of an individual in species \(f\),
\item
  \(B_{i,c,p,s}= 0.25\, \pi \, \sum_{j \neq i} w_j \, D_{j,c,p,s,t}^2\) is
  the sum of basal area of all individuals competitor trees \(j\) of the species
  \(c\) within the local neighbourhood
  of the tree $i$ in
  plot \(p\), data
  set \(s\) and census $t$, where \(w_j\) is a constant based on
  neighboorhood size for tree $j$ depending on the data set (see
  below). Note that \(B_{i,c,p,s}\) include all trees of species $c$
  in the local neighbourhood excepted the tree
  \(i\), and
\item
  \(N_i\) is the number of competitor species in the local
  neighbourhood of focal tree $i$.
\end{itemize}

Values of \(\alpha_{f,c}> 0\) indicate competition, whereas
\(\alpha_{f,c}\) \textless{} 0 indicates facilitation.

Log-transformation of equ. \ref{G1} leads to a linearised model of the
form

\begin{equation} \label{logG1}
\log{G_{i,f,p,s,t}} = \log{G_{\textrm{max} \, f,p,s}} + \gamma_f \, \log{D_{i,f,p,s,t}} +  \sum_{c=1}^{N_i} {-\alpha_{f,c} B_{i,c,p,s}}.
\end{equation}

To include the effects of traits on the parameters of the growth model we build on previous studies that explored the role of traits for tree performances and tree competition\citep{Uriarte-2010, Kunstler-2012, Lasky-2014}. We modelled the effect of traits, one trait at a time.
The effect of a focal species' trait value, \(t_f\), on its
maximum growth was included as:

\begin{equation} \label{Gmax}
\log{G_{\textrm{max} \, f,p,s}} = m_{0} + m_1 \, t_f + m_2 \, MAT +
m_3 \, MAP + \varepsilon_{G_{\textrm{max}}, f} + \varepsilon_{G_{\textrm{max}}, p} + \varepsilon_{G_{\textrm{max}}, s}.
\end{equation}

Here \(m_0\) is the average maximum growth, \(m_1\) gives the effect of
the focal species trait, $m_2$ and $m_3$ the effects of mean annual temperature
$MAT$ and sum of annual precipitation $MAP$ respectively, and \(\varepsilon_{G_{\textrm{max}}, f}\),
\(\varepsilon_{G_{\textrm{max}}, p}\), \(\varepsilon_{G_{\textrm{max}}, s}\)
are normally distributed random effects for species \(f\), plot or
quadrat \(p\) (see below), and data set \(s\) {[}where
\(\varepsilon_{G_{\textrm{max}, f}} \sim \mathcal{N} (0,\sigma_{G_{\textrm{max}, f}})\);
\(\varepsilon_{G_{\textrm{max}, p}} \sim \mathcal{N} (0,\sigma_{G_{\textrm{max}, p}})\)
and
\(\varepsilon_{G_{\textrm{max}, s}} \sim \mathcal{N} (0,\sigma_{G_{\textrm{max}, s}})\){]}.

Previous studies have proposed various decompositions of the competition parameter into key trait-based processes\footnote{Different approaches have been proposed to model $\alpha$ from traits. In one of the first studies Uriarte et al.\citep{Uriarte-2010} modelled $\alpha$ as $\alpha =
\alpha_0 + \alpha_d \vert t_f-t_c \vert$. Then Kunstler et al.\citep{Kunstler-2012} used two different models: $\alpha = \alpha_0 + \alpha_d \vert t_f-t_c \vert$ or $\alpha =
\alpha_0 + \alpha_h ( t_f-t_c )$. Finally, Lasky et
al.\citep{Lasky-2014} developed a single model including multiple processes as $\alpha =
\alpha_0 + \alpha_t t_f +\alpha_h ( t_f-t_c ) + \alpha_d \vert t_f-t_c
\vert$. In our study, we extended this last model. We considered that it was clearer to split
$\alpha_h (t_f - t_c)$ into $\alpha_t t_f + \alpha_e t_c$, which is
equivalent to the hierarchical distance if $\alpha_t = - \alpha_e$
(thus avoiding replication of $t_f$ effect through both $\alpha_h$ and
$\alpha_t$). We also included two $\alpha_0$, one for intra and one for
interspecific
competition.}, and here we extended the approach of the most recent study\citep{Lasky-2014}. As presented in Fig. 1, competitive
interactions were modelled using an equation of the form\footnote{For
  fitting the model, the equation of \(\alpha_{f,c}\) was developed with
  species basal area in term of community weighted means of (i) trait values of competitors and (i) absolute trait distance between focal species and its competitors.}:

\begin{equation} \label{alpha}
\alpha_{f,c}= \alpha_{0,f,intra} \, C + \alpha_{0,f,inter} \, (1-C) - \alpha_t \, t_f + \alpha_e \, t_c + \alpha_d \, \vert t_c-t_f \vert
\end{equation}

where:

\begin{itemize}
\itemsep1pt\parskip0pt\parsep0pt
\item
  $\alpha_{0,f,intra}$ and $\alpha_{0,f,inter}$ are respectively
  \textbf{intraspecific and average interspecific trait independent competition} for the focal
  species \(f\), modelled with a normally distributed random effect of
  species \(f\) and each with normally distributed random effect of data set
  \(s\) {[}as
  \(\alpha_{0,f} = \alpha_0 + \varepsilon_{\alpha_0, f}+ \varepsilon_{\alpha_0, s}\),
  where \(\varepsilon_{\alpha_0, f} \sim \mathcal{N} (0,\sigma_{\alpha_0, f})\) and
  \(\varepsilon_{\alpha_0, s} \sim \mathcal{N} (0,\sigma_{\alpha_0, s})\){]}. $C$ is a binary variable taking the value one for $f=c$ (conspecific) and zero for $f \neq c$ (heterospecific),
\item
  \(\alpha_t\) is the \textbf{tolerance of competition} by the focal
  species, i.e.~change in competition tolerance due to traits \(t_f\) of
  the focal tree with a normally distributed random effect of data set
  \(s\) included
  {[}\(\varepsilon_{\alpha_t,s} \sim \mathcal{N} (0,\sigma_{\alpha_t})\){]},
\item
  \(\alpha_{e}\) is the \textbf{competitive effect}, i.e.~change in
  competition effect due to traits \(t_c\) of the competitor tree with a
  normally distributed random effect of data set \(s\) included
  {[}\(\varepsilon_{\alpha_i,s} \sim \mathcal{N} (0,\sigma_{\alpha_i})\){]}, and
\item
  \(\alpha_d\) is the effect of \textbf{trait dissimilarity}, i.e.~change
  in competition due to absolute distance between traits
  \(\vert{t_c-t_f}\vert\) with a normally distributed random effect of
  data set \(s\) included
  {[}$\varepsilon_{\alpha_d,s} \sim \mathcal{N} (0,\sigma_{\alpha_d})${]}.
\end{itemize}

Estimating separate $\alpha_0$ for intra and interspecific competition allowed us to account for trait-independent differences in interactions with conspecifics and heterospecifics. We also explored a simpler version of the model where trait-independent competitive effects were pooled (i.e. there was a single value for $\alpha_0$), as most previous studies have generally not made this distinction, using the following equation:

\begin{equation} \label{alpha2}
\alpha_{f,c}= \alpha_{0,f} - \alpha_t \, t_f + \alpha_e \, t_c + \alpha_d \, \vert t_c-t_f \vert
\end{equation}

In this alternative model any differences between intra and interspecific competition do enter into trait dissimilarity effects, with a trait dissimilarity of zero attached to them. This may lead to an overestimation of the trait dissimilarity effect. Results for this model
are presented in Supplementary Results.

Eqs. \ref{logG1}-\ref{alpha} were then fitted to empirical estimates of
growth based on change in diameter between census $t$
and $t+1$ (respectively at year $y_t$ and $y_{t+1}$), given by

\begin{equation} \label{logGobs} G_{i,f,p,s,t} = 0.25 \pi
  \left(D_{i,f,p,s,t+1}^2 - D_{i,f,p,s,t}^2\right)/(y_{t+1} - y_t).
\end{equation}

To estimate standardised coefficients (one type of standardised effect
size)\citep{Schielzeth-2010}, response and explanatory variables
were standardized (divided by their standard deviations) prior to
analysis. Trait and diameter were also centred to facilitate
convergence. The models were fitted using the \(lmer\) routine in
the lme4 package \citep{Bates-2014}
in the R statistical environment\citep{RTeam-2014}. We fitted two
versions of each model. In the first
version parameters \(m_{0}, m_1, \alpha_0,\alpha_t,\alpha_i,\alpha_d\)
were estimated as constant across all biomes. In the second version, we
allowed
different fixed estimates of these parameters for each biome. This
enabled us to explore variation among biomes. Because some biomes had
few observations, we merged those with biomes with similar climates. Tundra was
merged with taiga, tropical rainforest and tropical seasonal forest were
merged into tropical forest, and deserts were not included in this final
analysis as too few plots were available. To evaluate whether our results
were robust to the random effect structure we also explored a model
with a random effect
attached to parameters both for the data set and for a local ecoregion using
the K{\"o}ppen-Geiger ecoregion\citep{Kriticos-2012} (see
Supplementary Results).

\subsection{Estimating the effect of traits on the average differences between intra and interspecific competition}\label{intrainter}

Differences between inter and intraspecific competition have long been considered key to community assembly and species coexistence\citep{Connell-1983, Chesson-2000, Chesson-2012, Kraft-2015, Godoy-2014}. Our estimated growth model allowed us to estimate the average inter and intraspecific competition from trait-independent and trait-dependent processes. Using eqn. \ref{alpha}, we can predict for any combination of two values of trait $t_i$ and $t_j$ the interspecific
($\alpha_{t_i,t_j}$ and $\alpha_{t_j,t_i}$) and
intraspecific ($\alpha_{t_i,t_i}$ and $\alpha_{t_j,t_j}$)
competition competition of typical (or mean) species (thus leaving out the random species effects). We can then estimate the average differences between interspecific and intraspecific competition over the two combinations using the following expression:

\begin{equation} \label{rhoequ}
\frac{(\alpha_{t_i,t_j} - \alpha_{t_i,t_i}) + (\alpha_{t_j,t_i} - \alpha_{t_j,t_j})}{2}
\end{equation}

Substituting in from eqn. \ref{alpha} (leaving out the species random
effect) this simplifies as:
\begin{equation} \label{rhoequ2}
\alpha_{0,inter} - \alpha_{0,intra} + \alpha_d \vert t_j - t_i \vert
\end{equation}

Thus, the average differences between inter and intraspecific competition are affected only by the difference between $\alpha_{0, intra}$ and $\alpha_{0, inter}$ and by trait dissimilarity via $\alpha_d$ (see Figure 3. in Extended Data for the results). 


\section{Data}\label{data}

\subsection{Growth data}\label{growth-data}

Our main objective was to collate data sets spanning the dominant forest
biomes of the world. Data sets were included if they (i) allowed both
growth of individual trees and the local abundance of competitors
to be estimated, and (ii) had good (\textgreater{}40\%) coverage for at
least one of the traits of interest (SLA, wood density, and maximum
height).

The data sets collated fell into two broad categories: (1) national
forest inventories (NFI), in which trees above a given diameter were
sampled in a network of small plots (often on a regular grid) covering
the country (references for NFI data used\citep{-b, Kooyman-2012, -e, Wiser-2001, -c, Villaescusa-1998, Villanueva-2004, Fridman-2001, -a, -d}); (2) large permanent plots (LPP) ranging in size from
0.5-50ha, in which the x-y coordinates of all trees above a given
diameter were recorded (references for LPP data used\citep{Condit-2013, Condit-1993, Lasky-2013, Ishihara-2011, Thompson-2002, Ouedraogo-2013, Herault-2010, Herault-2011} ). LPP were mostly located in tropical
regions. The minimum diameter of recorded trees varied among sites from
1-12cm. To allow comparison between data sets, we restricted our
analysis to trees greater than 10cm. Moreover, we excluded from the
analysis any plots with harvesting during the growth measurement period,
that were identified as plantations, or that overlapped a forest edge.
Finally, we randomly selected only two consecutive census dates per
plot or quadrat to
avoid having to account for repeated measurements (less than a third
of the data had repeated measurements). Because human and natural disturbances are present in all these forests (see Supplementary Methods), they probably all experience successional dynamics (as indicated by the forest age distribution available in some of these sites in Supplementary Methods). See Supplementary Methods and
Extended Data Table 1 for more details on individual data sets.

Basal area growth was estimated from diameter measurements recorded
between the two censuses. For the French NFI, these data were
obtained from short tree cores. For all other data sets, diameter at
breast height (\(D\)) of each individual was recorded at multiple census
dates. We excluded trees (i) with extreme positive or negative diameter
growth measurements, following criteria developed at the BCI site
\citep{Condit-1993} (see the R package
\href{http://ctfs.arnarb.harvard.edu/Public/CTFSRPackage/}{CTFS R}),
(ii) that were palms or tree ferns, or (iii) that were
measured at different heights in two consecutive censuses.

For each individual tree, we estimated the local abundance of competitor
species as the sum of basal area for all individuals \textgreater{} 10cm
diameter within a specified neighbourhood. For LPPs, we defined the
neighbourhood as being a circle with 15m radius. This value was selected
based on previous studies showing the maximum radius of interaction to
lie in the range
10-20m\citep{Uriarte-2004, Uriarte-2010}. To avoid
edge effects, we also excluded trees less than 15m from the edge of a
plot. To account for variation of abiotic conditions within the LPPs, we
divided plots into regularly spaced 20x20m quadrats and included a
random quadrat effect in the model (see above).

For NFI data coordinates of individual trees within plots were generally
not available, thus neighbourhoods were defined based on plot size. In
the NFI from the United States, four sub-plots of 7.35m located within
20m of one another were measured. We grouped these sub-plots to give a
single estimate of the local competitor abundance. Thus, the
neighbourhoods used in the competition analysis ranged in size from
10-25 m radius, with most plots 10-15 m radius. We included variation in
neighbourhood size in the constant $w_j$ to compute competitor basal
area in $m^2/ha$.

We extracted mean annual temperature (MAT) and mean annual sum of
precipitation (MAP) from the \href{http://www.worldclim.org/}{worldclim}
data base \citep{Hijmans-2005}, using the plot latitude and
longitude. MAT and MAP data were then used to classify plots into
biomes, using the diagram provided by Ricklefs\citep{Ricklefs-2001}
(after Whittaker).

\subsection{Traits}\label{traits}

Data on species functional traits were extracted from existing sources.
We focused on wood density, species specific leaf area (SLA) and maximum
height, because these traits have previously been related to competitive
interactions and are available for large numbers of species
\citep{Wright-2010, Uriarte-2010, Ruger-2012, Kunstler-2012, Lasky-2014}
(see Extended Data Table 2 for trait coverage). Where available we used
data collected locally (references for the local trait data used in this analysis\citep{Wright-2010, Swenson-2012, Gourlet-Fleury-2011, Lasky-2013, Baraloto-2010}); otherwise we sourced data from the
\href{http://www.try-db.org/}{TRY} trait data base
\citep{Kattge-2011} (references for the data extracted from the TRY database used in this analysis\citep{Ackerly-2007, Castro-Diez-1998, Chave-2009, Cornelissen-1996, Cornelissen-1996a, Cornelissen-1997, Cornelissen-2004, Cornelissen-2003, Cornwell-2009, Cornwell-2006, Cornwell-2007, Cornwell-2008, Diaz-2004, Fonseca-2000, Fortunel-2009, Freschet-2010, Freschet-2010a, Garnier-2007, Green-2009, Han-2005, He-2006, He-2008, Hoof-2008, Kattge-2009, Kleyer-2008, Kurokawa-2008, Laughlin-2010, Martin-2007, McDonald-2003, Medlyn-1999a, Medlyn-1999, Medlyn-2001, Messier-2010, Moles-2005b, Moles-2005a, Moles-2004, Niinemets-2001, Niinemets-1999, Ogaya-2003, Ogaya-2006, Ogaya-2007, Ogaya-2007a, Onoda-2011, Ordonez-2010, Ordonez-2010a, Pakeman-2008, Pakeman-2009, Penuelas-2010, Penuelas-2010a, Poorter-2006, Poorter-2009, Poorter-2009a, Preston-2006, Pyankov-1999, Quested-2003, Reich-2008, Reich-2009, Sack-2004, Sack-2005, Sack-2006, Sardans-2008, Sardans-2008a, Shipley-2002, Soudzilovskaia-2013, Willis-2010, Wilson-2000, Wright-2007, Wright-2006, Wright-2010, Wright-2004, Zanne-2010}).
 Local data were available for most tropical
sites and species (see Supplementary Methods). Several of the NFI data
sets also provided tree height measurements, from which we computed a
species' maximum height as the 99\% quantile of observed values (for
France, US, Spain, Switzerland). For Sweden we used the estimate from
the French data set and for Canada we used the estimate from the US data
set. Otherwise, we extracted height measurements from the TRY database. We were
not able to account for trait variability within species.

For each focal tree, our approach required us to also account for the
traits of all competitors present in the neighbourhood. Most of our
plots had good coverage of competitors, but inevitably there were some
trees where trait data were lacking. In these cases we estimated trait
data as follows. If possible, we used the genus mean, and if no genus
data was available, we used the mean of the species present in the
country. However, we restricted our analysis to plots where (i) the
percentage of basal area contributed by trees with no species level trait data was
less than 10\%, and (ii) the
percentage of basal area of trees with neither species nor genus level trait data was
less than 5\%.

\newpage
\clearpage

% \section{References}\label{references}

\bibliographystyle{naturemag}
\bibliography{references}

\end{document}
