% Supplementary Information


# Details on growth data used in analysis

Two main data types were used: national forest inventories -- NFI, large permanent plots -- LPP.

### Panama

- Data set name: Panama
- Data type: LPP
- Plot size: 1 to 50 ha
- Diameter at breast height threshold: 1 cm
- Number of plots: 42
- Traits: Wood density, SLA, and Maximum height
- Source trait data: local
- Evidence of disturbances and succession dynamics: Gap disturbances are common in the large 50ha BCI plot [see @Young-1991; @Hubbell-1999; @Lobo-2014]. Hubbell et al.[@Hubbell-1999] estimated that less than 30% of the plot experienced no disturbance over a 13-year period.
- Contact of person in charge of data formatting: Plot data: R. Condit (conditr@gmail.com),  Trait data: J. Wright (wrightj@si.edu)
- Comments: The data used include both the 50 ha plot of BCI and the network of 1 ha plots from Condit et al. (2013). The two first censuses of BCI plot were excluded.
- References: 
	- Condit, R. (1998). Tropical forest census plots. Springer, Berlin, Germany.
	- Condit, R., Engelbrecht, B.M.J., Pino, D., Perez, R., Turner, B.L., (2013).  Species distributions in response to individual soil nutrients and seasonal drought across a community of tropical trees. Proceedings of the National Academy of Sciences 110: 5064-5068.
	- Wright, S.J., Kitajima, K., Kraft, N.J.B., Reich, P.B., Wright, I.J., Bunker, D.E., Condit, R., Dalling, J.W., Davies, S.J., DÍaz, S., Engelbrecht, B.M.J., Harms, K.E., Hubbell, S.P., Marks, C.O., Ruiz-Jaen, M.C., Salvador, C.M. & Zanne, A.E. (2010) Functional traits and the growth-mortality trade-off in tropical trees. Ecology 91: 3664-3674.


### Japan

- Data set name: Japan
- Data type: LPP
- Plot size: 0.35 to 1.05 ha
- Diameter at breast height threshold: 2.39 cm
- Number of plots: 16
- Traits: Wood density, SLA, and Maximum height
- Source trait data: local
- Evidence of disturbances and succession dynamics: The network of plot comprise 50% of old growth forests, 17% of old secondary forests and 33% of young secondary forests.
- Contact of person in charge of data formatting: Plot data: M. I. Ishihara (moni1000f_networkcenter@ fsc.hokudai.ac.jp),  Trait data: Y Onoda (yusuke.onoda@gmail.com)
- Comments: 
- References: 
	- Yakushima Forest Environment Conservation Center, Ishihara, M.I., Suzuki, S.N., Nakamura, M., Enoki, T., Fujiwara, A., Hiura, T., Homma, K., Hoshino, D., Hoshizaki, K., Ida, H., Ishida, K., Itoh, A., Kaneko, T., Kubota, K., Kuraji, K., Kuramoto, S., Makita, A., Masaki, T., Namikawa, K., Niiyama, K., Noguchi, M., Nomiya, H., Ohkubo, T., Saito, S., Sakai, T., Sakimoto, M., Sakio, H., Shibano, H., Sugita, H., Suzuki, M., Takashima, A., Tanaka, N., Tashiro, N., Tokuchi, N., Yoshida, T., Yoshida, Y., (2011). Forest stand structure, composition, and dynamics in 34 sites over Japan. Ecological Research 26: 1007-1008. 


### Puerto Rico

- Data set name: Luquillo
- Data type: LPP
- Plot size: 16 ha
- Diameter at breast height threshold: 1 cm
- Number of plots: 1
- Traits: Wood density, SLA, and Maximum height
- Source trait data: local
- Evidence of disturbances and succession dynamics: The plot has been struck by hurricanes in 1989 and in 1998[@Uriarte-2009]. In addition, two-third of the plot is a secondary forest on land previously used for agriculture and logging[@Uriarte-2009].
- Contact of person in charge of data formatting: Plot data: J. Thompson (jiom@ceh.ac.uk) and J. Zimmerman (esskz@ites.upr.edu),  Trait data: N. Swenson (swensonn@msu.edu )
- Comments: 
- References: 
	- Thompson, J., N. Brokaw, J. K. Zimmerman, R. B. Waide, E. M. Everham III, D. J. Lodge, C. M. Taylor, D. GarciaMontiel, and M. Fluet. (2002). Land use history, environment, and tree composition in a tropical forest. Ecological Applications 12: 1344-1363.
	- Swenson, N.G., J.C. Stegen, S.J. Davies, D.L. Erickson, J. Forero-Montana, A.H. Hurlbert, W.J. Kress, J. Thompson, M. Uriarte, S.J. Wright and J.K. Zimmerman. (2012). Temporal turnover in the composition of tropical tree communities: functional determinism and phylogenetic stochasticity. Ecology 93: 490-499.


### Central African Republic

- Data set name: M'Baiki
- Data type: LPP
- Plot size: 4 ha
- Diameter at breast height threshold: 10 cm
- Number of plots: 10
- Traits: Wood density and SLA
- Source trait data: local
- Evidence of disturbances and succession dynamics: The plot network was established with three levels of harvesting and an unharvested control [@Gourlet-Fleury-2013].
- Contact of person in charge of data formatting: G. Vieilledent (ghislain.vieilledent@cirad.fr)
- Comments: 
- References: 
	- Ouadraogo, D.-Y., Mortier, F., Gourlet-Fleury, S., Freycon, V., and Picard, N. (2013). Slow-growing species cope best with drought: evidence from long-term measurements in a tropical semi-deciduous moist forest of Central Africa. Journal of Ecology 101: 1459-1470.
	- Gourlet-Fleury, S., V. Rossi, M. Rejou-Mechain, V. Freycon, A. Fayolle, L. Saint-Andr_, G. Cornu, J. Gerard, J. M. Sarrailh, and O. Flores. (2011). Environmental Filtering of Dense-Wooded Species Controls above-Ground Biomass Stored in African Moist Forests. Journal of Ecology 99: 981-90.


### Taiwan

- Data set name: Fushan
- Data type: LPP
- Plot size: 25 ha
- Diameter at breast height threshold: 1 cm
- Number of plots: 1
- Traits: Wood density and SLA
- Source trait data: local
- Evidence of disturbances and succession dynamics: Fushan experienced several Typhoon disturbances in 1994 with tree fall events. The main disturbance effect was trees defoliation[@Lin-2011].
- Contact of person in charge of data formatting: I-F. Sun (ifsun@mail.ndhu.edu.tw)
- Comments: 
- References: 
	- Lasky, J.R., Sun, I., Su, S.-H., Chen, Z.-S., and Keitt, T.H. (2013). Trait-mediated effects of environmental filtering on tree community dynamics. Journal of Ecology 101: 722-733.


### French Guiana

- Data set name: Paracou
- Data type: LPP
- Plot size: 6.25 ha
- Diameter at breast height threshold: 10 cm
- Number of plots: 15
- Traits: Wood density and SLA
- Source trait data: local
- Evidence of disturbances and succession dynamics: The plot network was established with three levels of harvesting and unharvested control[@Herault-2011].
- Contact of person in charge of data formatting: Plot data: B. Herault (bruno.herault@cirad.fr),  Trait data: C. Baraloto (Chris.Baraloto@ecofog.gf)
- Comments: 
- References: 
	- Herault, B., Bachelot, B., Poorter, L., Rossi, V., Bongers, F., Chave, J., Paine, C.E., Wagner, F., and Baraloto, C. (2011). Functional traits shape ontogenetic growth trajectories of rain forest tree species. Journal of Ecology 99: 1431-1440.
	- Herault, B., Ouallet, J., Blanc, L., Wagner, F., and Baraloto, C. (2010). Growth responses of neotropical trees to logging gaps. Journal of Applied Ecology 47: 821-831.
	- Baraloto, C, P.C.E. Timothy, L. Poorter, J. Beauchene, D. Bonal, AM Domenach, B. Herault, S. Patido, JC Roggy, and Jerome Chave. (2010). Decoupled Leaf and Stem Economics in Rain Forest Trees. Ecology Letters 13: 1338-47.


### France

- Data set name: France
- Data type: NFI
- Plot size: 0.017 to 0.07 ha
- Diameter at breast height threshold: 7.5 cm
- Number of plots: 41503
- Traits: Wood density, SLA, and Maximum height
- Source trait data: TRY
- Evidence of disturbances and succession dynamics: French forests monitored by the French National Forest Inventory experienced several types of natural disturbance[@Seidl-2014] (such as wind, forest fire, and insect attacks) and harvesting. The age structure reconstructed by Vilen et al.[@Vilen-2012] shows that young forests represent a significant percentage of the forested area (see age distribution below).
- Contact of person in charge of data formatting: G. Kunstler (georges.kunstler@gmail.com)
- Comments: The French NFI is based on temporary plots, but 5 years tree radial growth is estimated with a short core. All trees with dbh > 7.5 cm, > 22.5 cm and > 37.5 cm were measured within a radius of 6 m, 9 m and 15 m, respectively. Plots are distributed over forest ecosystems on a 1x1-km grid
- References: 
	- IFN. (2011). Les resultats issus des campagnes d'inventaire 2006, 2007, 2008, 2009, 2010 et 2011. Inventaire Forestier National, Nogent-sur-Vernisson, FR.
	- http://inventaire-forestier.ign.fr/spip/spip.php?rubrique153


### Spain

- Data set name: Spain
- Data type: NFI
- Plot size: 0.0078 to 0.19 ha
- Diameter at breast height threshold: 7.5 cm
- Number of plots: 49855
- Traits: Wood density, SLA, and Maximum height
- Source trait data: TRY
- Evidence of disturbances and succession dynamics: Spanish forests monitored by the Spanish National Forest Inventory experienced several types of natural disturbance[@Seidl-2014] (such as wind, forest fire, and insect attacks) and harvesting. No data are available on the age structure of the plots.
- Contact of person in charge of data formatting: M. Zavala (madezavala@gmail.com)
- Comments: Each SFI plot included four concentric circular sub-plots of 5, 10, 15 and 25-m radius. In these sub-plots, adult trees were sampled when diameter at breast height (d.b.h.) was 7.5-12.4 cm, 12.5-22.4 cm, 22.5-42.5 cm and >= 42.5 cm, respectively.
- References: 
	- Villaescusa, R. & Diaz, R. (1998) Segundo Inventario Forestal Nacional (1986-1996), Ministerio de Medio Ambiente, ICONA, Madrid.
	- Villanueva, J.A. (2004) Tercer Inventario Forestal Nacional (1997-2007). Comunidad de Madrid.  Ministerio de Medio Ambiente, Madrid.
	- http://www.magrama.gob.es/es/desarrollo-rural/temas/politica-forestal/inventario-cartografia/inventario-forestal-nacional/default.aspx


### Switzerland

- Data set name: Swiss
- Data type: NFI
- Plot size: 0.02 to 0.05 ha
- Diameter at breast height threshold: 12 cm
- Number of plots: 2665
- Traits: Wood density, SLA, and Maximum height
- Source trait data: TRY
- Evidence of disturbances and succession dynamics: Swiss forests monitored by the Swiss National Forest Inventory experienced several types of natural disturbance (such as wind, forest fire, fungi and insect attacks) and harvesting. The age structure reconstructed by Vilen et al.[@Vilen-2012] shows that young forests represent a significant percentage of the forested area (see age distribution below).
- Contact of person in charge of data formatting: M. Hanewinkel & N. E. Zimmermann (niklaus.zimmermann@wsl.ch)
- Comments: All trees with dbh > 12 cm and > 36 cm were measured within a radius of 7.98 m and 12.62 m, respectively.
- References: 
	- http://www.lfi.ch/index-en.php
	- Brandli, U.-B. (Red.) 2010: Schweizerisches Landesforstinventar. Ergebnisse der dritten Erhebung 2004-2006. Birmensdorf, Eidgenossische Forschungsanstalt fur Wald, Schnee und Landschaft WSL. Bern, Bundesamt fur Umwelt, BAFU. 312 S.


### Sweden

- Data set name: Sweden
- Data type: NFI
- Plot size: 0.0019 to 0.0314 ha
- Diameter at breast height threshold: 5 cm
- Number of plots: 22904
- Traits: Wood density, SLA, and Maximum height
- Source trait data: TRY
- Evidence of disturbances and succession dynamics: Swedish forests monitored by the Swedish National Forest Inventory experienced several types of natural disturbance[@Seidl-2014] (such as wind, forest fire, and insect attacks) and harvesting. The age structure reconstructed by Vilen et al.[@Vilen-2012] shows that young forests represent a significant percentage of the forested area (see age distribution below).
- Contact of person in charge of data formatting: G. Stahl (Goran.Stahl@slu.se)
- Comments: All trees with dbh > 10 cm were measured on circular plots of 10 m radius.
- References: 
	- Fridman, J., and Stahl, G. (2001). A three-step approach for modelling tree mortality in Swedish forests. Scandinavian Journal of Forest Research 16: 455-466.


### USA

- Data set name: US
- Data type: NFI
- Plot size: 0.0014 to 0.017 ha
- Diameter at breast height threshold: 2.54 cm
- Number of plots: 97434
- Traits: Wood density, SLA, and Maximum height
- Source trait data: TRY
- Evidence of disturbances and succession dynamics: US forests monitored by the FIA experienced several types of natural disturbance (such as wind, forest fire, fungi and insects attacks) and harvesting. The age structure reconstructed by Pan et al.[@Pan-2011] shows that young forests represents a significant percentage of the forested area (see age distribution below).
- Contact of person in charge of data formatting: M. Vanderwel (Mark.Vanderwel@uregina.ca)
- Comments: FIA data are made up of clusters of 4 subplots of size 0.017 ha for tree dbh > 1.72 cm and nested within each subplot sampling plots of 0.0014 ha for trees dbh > 2.54 cm. The data for the four subplots were pooled
- References: 
	- http://www.fia.fs.fed.us/tools-data/


### Canada

- Data set name: Canada
- Data type: NFI
- Plot size: 0.02 to 0.18 ha
- Diameter at breast height threshold: 2 cm
- Number of plots: 15019
- Traits: Wood density, SLA, and Maximum height
- Source trait data: TRY
- Evidence of disturbances and succession dynamics: Canadian forests monitored by the regional forest monitoring programs experienced several types of natural disturbance (such as wind, forest fire, fungi and insect attacks) and harvesting. The age structure reconstructed by Pan et al.[@Pan-2011] shows that young forests represent a significant percentage of the forested area (see age distribution below).
- Contact of person in charge of data formatting: J. Caspersen (john.caspersen@utoronto.ca)
- Comments: Provinces included are Manitoba,  New Brunswick, Newfoundland and Labrador, Nova Scotia, Ontario, Quebec and Saskatchewan. The protocol is variable between Provinces. A large proportion of data is from the Quebec province and the plots are 10 m in radius in this Province.
- References: 


### New Zealand

- Data set name: NZ
- Data type: NFI
- Plot size: 0.04 ha
- Diameter at breast height threshold: 3 cm
- Number of plots: 1415
- Traits: Wood density, SLA, and Maximum height
- Source trait data: local
- Evidence of disturbances and succession dynamics: New Zealand forests are experiencing disturbance by earthquake, landslide, storm, volcanic eruptions, and other types. According to Holdaway et al.[@Holdaway-2014] the disturbance return interval on the plots is 63 years.
- Contact of person in charge of data formatting: D. Laughlin (d.laughlin@waikato.ac.nz)
- Comments: Plots are 20 x 20 m.
- References: 
	- Wiser, S.K., Bellingham, P.J. & Burrows, L.E. (2001) Managing biodiversity information: development of New Zealand's National Vegetation Survey databank. New Zealand Journal of Ecology, 25: 1-17.
	- https://nvs.landcareresearch.co.nz/


### Australia

- Data set name: NSW
- Data type: NFI
- Plot size: 0.075 to 0.36 ha
- Diameter at breast height threshold: 10 cm
- Number of plots: 30
- Traits: Wood density, and Maximum height
- Source trait data: local
- Evidence of disturbances and succession dynamics: The plot network was initially established in the 1960s with different levels of selection harvesting[@Kariuki-2006].
- Contact of person in charge of data formatting: R. M. Kooyman (robert@ecodingo.com.au) for plot and trait data
- Comments: Permanents plots established by the NSW Department of State Forests or by RMK
- References: 
	- Kooyman, R.M. and Westoby, M. (2009) Costs of height gain in rainforest saplings: main stem scaling, functional traits and strategy variation across 75 species. Annals of Botany 104: 987-993.
	- Kooyman, R.M., Rossetto, M., Allen, C. and Cornwell, W. (2012) Australian tropical and sub-tropical rainforest: phylogeny, functional biogeography and environmental gradients. Biotropica 44: 668-679.

\newpage

## Forests age distribution for Europe and North America.


![**Age distribution of forest area in 20-year age class for France, Switzerland and Sweden, estimated by Vilen et al.[@Vilen-2012].** The last class plotted at 150 years is for age > 140 years (except for Sweden where the last class 110 is age > 100 years).](../../figs/age_europe.pdf)

![**Age distribution of forest area in 20-year age class for North America (USA and Canada), estimated by Pan et al.[@Pan-2011].** The last class plotted at 150 years is for age > 140 years.](../../figs/age_na.pdf)

\newpage

# Supplementary Results

![**Trait-dependent and trait-independent effects on
maximum growth and competition across the globe and their variation among biomes for models with random effect in the parameters for data set and the Koppen-Geiger ecoregion for wood density (a), specific
leaf area (b) and maximum height (c).** See Figure 2 in the main text for parameters description and see Fig 1a in the main text for biome definition.](../../figs/figres12_ecocode_TP_intra.pdf)

\newpage

# Supplementary Discussion

## Trait effects and potential mechanisms

The most important driver of individual growth was individual tree size with a positive effect on basal area growth (see Extended Data Table 3). This is unsurprising as tree size is known to be a key driver of tree growth[@Stephenson-2014; @Enquist-1999]. Then there was a consistent negative effect of the total basal area of conspecific and heterospecific neighbouring competitors across all biomes. The dominance of a competitive effect for the growth of adult trees (diameter at breast height >= 10cm) agrees well with the idea that facilitation processes are generally limited to the regeneration phase rather than to the adult stage [@Callaway-1997]. Differences in $\alpha_{0 \, intra} \, \& \, \alpha_{0 \, inter}$ between biomes are not great with large overlap of their confidence intervals. Because the data coverage with different among traits, the data used to fit the models is not identical for all traits. This may explain variation between biomes for trait-independent parameters?

In terms of trait effects, wood density (WD) was strongly negatively associated with maximum growth, which is in agreement with the idea that shade-intolerant species with low wood density have faster growth in absence of competition (full light conditions) than shade tolerant species[@Nock-2009; @Wright-2010]. One advantage of low wood density is clearly that it is cheaper to build light than dense wood, thus for the same biomass growth low wood density species will have higher basal area increments than species with high wood density[@Enquist-1999]. Other advantages of low wood density may include higher xylem conductivity[@Chave-2009], though for angiosperms this is a correlated trait rather than a direct consequence. A countervailing advantage for high wood density species was their better tolerance of competition (less growth reduction per unit of basal area of competitors), which is in line with the idea that these species are more shade tolerant[@Chave-2009; @Nock-2009; @Wright-2010]. This has most often been related to the higher survival associated with high wood density[@Kraft-2010] via resistance to mechanical damage, herbivores and pathogens[@Chave-2009; @Kraft-2010]. Survival was not analysed here. The growth-response differences that were analysed might be related to lower maintenance respiration[@Larjavaara-2010], that may lead to a direct advantage in deep shade. This relationship might also arise through correlated selection for high survival and high growth in shade. Finally, high wood density was also weakly correlated with stronger competitive effects. This might possibly have been mediated by larger crowns (both in depth and radius)[@Poorter-2006a; @Aiba-2009], casting a deeper shade.

SLA appeared positively correlated with maximum basal area growth, though only significantly in three of five biomes and with weak effects. Previous studies have reported strong positive correlations among SLA, gas exchange (the 'leaf economic spectrum'[@Wright-2004]) and young seedling relative growth rate[@Shipley-2006; @Wright-2001]. Studies[@Poorter-2008; @Wright-2010] on adult trees have however generally reported weak and marginal correlation between SLA and maximum growth. In short, our results are in line with previous reports. Low SLA was also correlated with a stronger competitive effect. This may be related to a longer leaf life span characteristic of low SLA species because leaf longevity leads to a higher accumulation of leaf in the canopy and thus a higher light interception[@Niinemets-2010].

Maximum height was weakly positively correlated with maximum growth rate (confidence intervals spanned zero except for temperate rain forest). Previous studies[@Poorter-2006a; @Poorter-2008; @Wright-2010] have found mixed support for this relationship. Possible mechanisms are contradictory. Maximum height may be associated with greater access to light and thus faster growth, but at the same time life history strategies might select for slower growth in long-lived plants[@Poorter-2008]. Maximum height was weakly negatively correlated with tolerance to competition (confidence intervals spanned zero except for temperate rain forest, tropical forest and taiga), in line with the idea that sub-canopy trees are more shade-tolerant[@Poorter-2006a]. There was very little correlation between maximum height and competitive effects, despite the fact that taller trees are generally considered to have a greater light interception ability. It is however important to note that we are analysing the species maximum height and not the actual tree height of the individual. These weak effects of maximum height are probably explained by the fact that our analysis deals with short-term competition effects on tree growth. Size-structured population models[@Adams-2007] have in contrast shown that maximum height can be a key driver of long-term competitive success in terms of population growth rate.

Our results raise the question whether there is a coordination between trait values conferring strong competitive effect and trait values conferring high competitive tolerance. Competitive effect and tolerance are two central elements of a species' competitive ability[@Goldberg-1991]. One may expect that because of intraspecific competition, species with strong competitive effects should have evolved a high tolerance to competition. We found clear evidence for such coordination for wood density, but not for the other traits. High wood density conferred better competitive tolerance and also stronger competitive effects. High SLA conferred stronger competitive effects but not better tolerance of competition. For maximum height, as explained above, there was a tendency for short maximum height to lead to high tolerance of competition (see also the Figure 4 in Supplementary Results), but no link with competitive effect. These mixed results on coordination between tolerance and effects are important because they mean that competitive interactions are not well described as a trait hierarchy relating a focal species to its competitors (measured as $t_c -t_f$ and thus assuming $\alpha_e = \alpha_t$ as in @Kunstler-2012; @Kraft-2014; @Lasky-2014). Traits of competitors alone or of focal plants alone may convey more information than the trait hierarchy. These processes of traits linked to either competitive effects or competitive tolerance, nevertheless, still lead to some trait values having an advantage in competitive interactions. One key question regarding competitive effect and competitive response is their relative importance in terms of long-term effects on population level outcomes of competition. In some simple population models[@Tilman-1977; @Goldberg-1996] the population outcome of competition was not affected by the competitive effect. On this basis competitive tolerance would be more important than competitive effect. However more complex resource competition models[@Tilman-1990], or size-structured forest models of competition for light[@Adams-2007], do show that the competitive effect may influence the long-term population outcome of competition.

It is also important to note that trait dissimilarity effects appeared stronger when trait independent differences between intra and interspecies competition were not separated  (Figure 3 in Supplementary Results). In other words, competitive effects were distinctly stronger when traits were identical compared to when they were a little different.

Given that the effect sizes we report for effects of traits on competitive interactions are modest, the question arises whether the three traits available to us (wood density, SLA, and maximum height) were the best candidates. It is possible that traits more directly related to mechanisms of competition may be more powerful. For instance the leaf area index of the competitors or the light compensation point at leaf or whole-plant level might have superior predictive power in relation to competition for light. It is possible also that traits measured at the individual level rather than as species averages might have strengthened the predictive power of our analysis[@Kraft-2014].


## Variations between biomes

Overall, most results were rather consistent across biomes (Fig 2 main text), but some exceptions deserve comment.
For SLA, the sign of the tolerance of competition parameters differed substantially among biomes (Fig. 2 main text). High SLA species tended to be more competition-tolerant (tolerance to competition parameter $\alpha_t$) in temperate forests (confidence interval only marginally intercepted zero) while low SLA species were more competition-tolerant in tropical and temperate rain forests. These different outcomes may trace to the lack of deciduous species in tropical and temperate rain forests (see Extended Data Table 1), because the link between shade-tolerance and SLA is different for deciduous and evergreen species[@Lusk-2008]. In tropical forests shade-tolerant species often have long leaf lifespans, associated with low SLA. On the other hand in temperate deciduous forests the length of the growing season is fixed by temperature. Shade tolerant species cannot increase leaf longevity and instead reduce the cost of leaf production (high SLA) to offset the reduced income due to low light availability. The other noticeable difference between biomes was for taiga where the parameter relating wood density to competitive effect was negative, versus positive in the other biomes (Fig 2 main text). We do not have a mechanistic explanation for this discrepancy, but observe that taiga has relatively few species, many of which are conifers where the range of wood density is narrower than in angiosperms (see Extended Data Table 1).


# References

