
```{r,results="hide",echo=FALSE}
opts_chunk$set(warnings = FALSE)

# LOAD SITE DETAILS
json_file <- file.path(DIR,"_site_details.JSON")
details <- fromJSON(paste(readLines(json_file), collapse=""))

# FUNCTIONS FOR FORMATTING TABLES
pandify <- function(data.table){
	pandoc.table.return(data.table, styl="multiline", split.tables= 200, split.cells = 50, justify = "left")
}
```

```{r,results="asis",echo=FALSE}
writeLines(paste("\\newpage\n\n# Description of the", details$dataset$name, " dataset\n"))
```
## Contact information

```{r,results="asis",echo=FALSE}
for(l in names(details$contact))
	writeLines(paste0("**",l,"**: ", details$contact[[l]], "\n"))
```

## Dataset availability / usage agreement

```{r,results="asis",echo=FALSE}
	writeLines(paste0(details$agreement, "\n"))
```


## Dataset

```{r,results="asis",echo=FALSE}
for(l in names(details$dataset))
	writeLines(paste0("**",l,"**: ", details$dataset[[l]], "\n"))
```

## References

```{r,results="asis",echo=FALSE}
for(i in 1:length(details$references))
	writeLines(paste0("* ", details$references[[i]], "\n"))
```


## File descriptions

Below is a list of the various files provided for this dataset, and the variables within each.


```{r,results="asis",echo=FALSE}
for(n in names(details$files)){
	writeLines("\\newpage \n")
	filename <- file.path(DIR, "files", paste0(tools::file_path_sans_ext( gsub("/", "_", n)), ".csv"))
	mytext <- pandify(read.csv(filename, stringsAsFactors=FALSE))
	writeLines(paste0("### ", n,"\n"))
	writeLines(paste0( details$files[[n]]$contents, "\n"))
	writeLines(mytext)
}
```

## Calculations

Details on data manipulations required for use of this dataset in this study.

```{r,results="asis",echo=FALSE}
filename <- file.path(DIR,"calculations.md")
if(file.exists(filename))
	writeLines(readLines(filename))
```



