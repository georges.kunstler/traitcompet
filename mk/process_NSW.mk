
NSW:  $(D3)/NSW/Done.no.txt $(D3)/NSW/Done.global.txt

$(D3)/NSW/Done.no.txt: R/process.data/process-fun.R  $(D2)/NSW/traits.csv
	Rscript -e "source('$<'); process_inventory_dataset('NSW',std.traits='no');"

$(D3)/NSW/Done.global.txt: R/process.data/process-fun.R  $(D2)/NSW/traits.csv
	Rscript -e "source('$<'); process_inventory_dataset('NSW',std.traits='global');"

$(D2)/NSW/traits.csv: scripts/find.trait/NSW.R R/find.trait/trait-fun.R $(D2)/NSW/tree.csv $(D2)/TRY/data.TRY.std.rds
	Rscript $<

$(D2)/NSW/tree.csv: scripts/format.data/NSW.R R/format.data/format-fun.R $(shell find $(D1)/NSW -type f)
	Rscript $<

