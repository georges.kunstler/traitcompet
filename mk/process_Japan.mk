Japan:  $(D3)/Japan/Done.no.txt $(D3)/Japan/Done.global.txt

$(D3)/Japan/Done.no.txt: R/process.data/process-fun.R $(D2)/Japan/traits.csv
	Rscript -e "source('$<'); process_bigplot_dataset('Japan', Rlim=15,std.traits='no');"

$(D3)/Japan/Done.global.txt: R/process.data/process-fun.R $(D2)/Japan/traits.csv
	Rscript -e "source('$<'); process_bigplot_dataset('Japan', Rlim=15,std.traits='global');"

$(D2)/Japan/traits.csv: scripts/find.trait/Japan.R R/find.trait/trait-fun.R $(D2)/Japan/tree.csv
	Rscript $<

$(D2)/Japan/tree.csv: scripts/format.data/Japan.R R/format.data/format-fun.R $(shell find $(D1)/Japan -type f)
	Rscript $<
