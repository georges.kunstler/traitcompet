
France:  $(D3)/France/Done.no.txt $(D3)/France/Done.global.txt

$(D3)/France/Done.no.txt: R/process.data/process-fun.R  $(D2)/France/traits.csv
	Rscript -e "source('$<'); process_inventory_dataset('France',std.traits='no');"

$(D3)/France/Done.global.txt: R/process.data/process-fun.R  $(D2)/France/traits.csv
	Rscript -e "source('$<'); process_inventory_dataset('France',std.traits='global');"

$(D2)/France/traits.csv: scripts/find.trait/France.R R/find.trait/trait-fun.R $(D2)/France/tree.csv $(D2)/TRY/data.TRY.std.rds
	Rscript $<

$(D2)/France/tree.csv: scripts/format.data/France.R R/format.data/format-fun.R $(shell find $(D1)/France -type f)
	Rscript $<

