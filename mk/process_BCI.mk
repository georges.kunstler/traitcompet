BCI:  $(D3)/BCI/Done.no.txt $(D3)/BCI/Done.global.txt

$(D3)/BCI/Done.no.txt: R/process.data/process-fun.R $(D2)/BCI/traits.csv
	Rscript -e "source('$<'); process_bigplot_dataset('BCI', Rlim=15,std.traits='no');"

$(D3)/BCI/Done.global.txt: R/process.data/process-fun.R $(D2)/BCI/traits.csv
	Rscript -e "source('$<'); process_bigplot_dataset('BCI', Rlim=15,std.traits='global');"

$(D2)/BCI/traits.csv: scripts/find.trait/BCI.R R/find.trait/trait-fun.R $(D2)/BCI/tree.csv
	Rscript $<

$(D2)/BCI/tree.csv: scripts/format.data/BCI.R R/format.data/format-fun.R $(shell find $(D1)/BCI -type f)
	Rscript $<
