Paracou:  $(D3)/Paracou/Done.no.txt $(D3)/Paracou/Done.global.txt

$(D3)/Paracou/Done.no.txt: R/process.data/process-fun.R $(D2)/Paracou/traits.csv
	Rscript -e "source('$<'); process_bigplot_dataset('Paracou', Rlim=15,std.traits='no');"

$(D3)/Paracou/Done.global.txt: R/process.data/process-fun.R $(D2)/Paracou/traits.csv
	Rscript -e "source('$<'); process_bigplot_dataset('Paracou', Rlim=15,std.traits='global');"

$(D2)/Paracou/traits.csv: scripts/find.trait/Paracou.R R/find.trait/trait-fun.R $(D2)/Paracou/tree.csv
	Rscript $<

$(D2)/Paracou/tree.csv: scripts/format.data/Paracou.R R/format.data/format-fun.R $(shell find $(D1)/Paracou -type f)
	Rscript $<
