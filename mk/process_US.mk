
US:  $(D3)/US/Done.no.txt $(D3)/US/Done.global.txt

$(D3)/US/Done.no.txt: R/process.data/process-fun.R  $(D2)/US/traits.csv
	Rscript -e "source('$<'); process_inventory_dataset('US',std.traits='no');"

$(D3)/US/Done.global.txt: R/process.data/process-fun.R  $(D2)/US/traits.csv
	Rscript -e "source('$<'); process_inventory_dataset('US',std.traits='global');"

$(D2)/US/traits.csv: scripts/find.trait/US.R R/find.trait/trait-fun.R $(D2)/US/tree.csv $(D2)/TRY/data.TRY.std.rds
	Rscript $<

$(D2)/US/tree.csv: scripts/format.data/US.R R/format.data/format-fun.R $(shell find $(D1)/US -type f)
	Rscript $<

