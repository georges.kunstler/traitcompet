Fushan:  $(D3)/Fushan/Done.no.txt $(D3)/Fushan/Done.global.txt

$(D3)/Fushan/Done.no.txt: R/process.data/process-fun.R $(D2)/Fushan/traits.csv
	Rscript -e "source('$<'); process_bigplot_dataset('Fushan', Rlim=15,std.traits='no');"

$(D3)/Fushan/Done.global.txt: R/process.data/process-fun.R $(D2)/Fushan/traits.csv
	Rscript -e "source('$<'); process_bigplot_dataset('Fushan', Rlim=15,std.traits='global');"

$(D2)/Fushan/traits.csv: scripts/find.trait/Fushan.R R/find.trait/trait-fun.R $(D2)/Fushan/tree.csv
	Rscript $<

$(D2)/Fushan/tree.csv: scripts/format.data/Fushan.R R/format.data/format-fun.R $(shell find $(D1)/Fushan -type f)
	Rscript $<
