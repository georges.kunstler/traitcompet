
NVS:  $(D3)/NVS/Done.no.txt $(D3)/NVS/Done.global.txt

$(D3)/NVS/Done.no.txt: R/process.data/process-fun.R  $(D2)/NVS/traits.csv
	Rscript -e "source('$<'); process_inventory_dataset('NVS',std.traits='no');"

$(D3)/NVS/Done.global.txt: R/process.data/process-fun.R  $(D2)/NVS/traits.csv
	Rscript -e "source('$<'); process_inventory_dataset('NVS',std.traits='global');"

$(D2)/NVS/traits.csv: scripts/find.trait/NVS.R R/find.trait/trait-fun.R $(D2)/NVS/tree.csv $(D2)/TRY/data.TRY.std.rds
	Rscript $<

$(D2)/NVS/tree.csv: scripts/format.data/NVS.R R/format.data/format-fun.R $(shell find $(D1)/NVS -type f)
	Rscript $<

