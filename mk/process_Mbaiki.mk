Mbaiki:  $(D3)/Mbaiki/Done.no.txt $(D3)/Mbaiki/Done.global.txt

$(D3)/Mbaiki/Done.no.txt: R/process.data/process-fun.R $(D2)/Mbaiki/traits.csv
	Rscript -e "source('$<'); process_bigplot_dataset('Mbaiki', Rlim=15,std.traits='no');"

$(D3)/Mbaiki/Done.global.txt: R/process.data/process-fun.R $(D2)/Mbaiki/traits.csv
	Rscript -e "source('$<'); process_bigplot_dataset('Mbaiki', Rlim=15,std.traits='global');"

$(D2)/Mbaiki/traits.csv: scripts/find.trait/Mbaiki.R R/find.trait/trait-fun.R $(D2)/Mbaiki/tree.csv
	Rscript $<

$(D2)/Mbaiki/tree.csv: scripts/format.data/Mbaiki.R R/format.data/format-fun.R $(shell find $(D1)/Mbaiki -type f)
	Rscript $<
