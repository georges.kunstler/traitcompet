
Spain:  $(D3)/Spain/Done.no.txt $(D3)/Spain/Done.global.txt

$(D3)/Spain/Done.no.txt: R/process.data/process-fun.R  $(D2)/Spain/traits.csv
	Rscript -e "source('$<'); process_inventory_dataset('Spain',std.traits='no');"

$(D3)/Spain/Done.global.txt: R/process.data/process-fun.R  $(D2)/Spain/traits.csv
	Rscript -e "source('$<'); process_inventory_dataset('Spain',std.traits='global');"

$(D2)/Spain/traits.csv: scripts/find.trait/Spain.R R/find.trait/trait-fun.R $(D2)/Spain/tree.csv $(D2)/TRY/data.TRY.std.rds
	Rscript $<

$(D2)/Spain/tree.csv: scripts/format.data/Spain.R R/format.data/format-fun.R $(shell find $(D1)/Spain -type f)
	Rscript $<

