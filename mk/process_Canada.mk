
Canada:  $(D3)/Canada/Done.no.txt $(D3)/Canada/Done.global.txt

$(D3)/Canada/Done.no.txt: R/process.data/process-fun.R  $(D2)/Canada/traits.csv
	Rscript -e "source('$<'); process_inventory_dataset('Canada',std.traits='no');"

$(D3)/Canada/Done.global.txt: R/process.data/process-fun.R  $(D2)/Canada/traits.csv
	Rscript -e "source('$<'); process_inventory_dataset('Canada',std.traits='global');"

$(D2)/Canada/traits.csv: scripts/find.trait/Canada.R R/find.trait/trait-fun.R $(D2)/Canada/tree.csv $(D2)/TRY/data.TRY.std.rds
	Rscript $<

$(D2)/Canada/tree.csv: scripts/format.data/Canada.R R/format.data/format-fun.R $(shell find $(D1)/Canada -type f)
	Rscript $<

