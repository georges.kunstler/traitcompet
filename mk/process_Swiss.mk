
Swiss:  $(D3)/Swiss/Done.no.txt $(D3)/Swiss/Done.global.txt

$(D3)/Swiss/Done.no.txt: R/process.data/process-fun.R  $(D2)/Swiss/traits.csv
	Rscript -e "source('$<'); process_inventory_dataset('Swiss',std.traits='no');"

$(D3)/Swiss/Done.global.txt: R/process.data/process-fun.R  $(D2)/Swiss/traits.csv
	Rscript -e "source('$<'); process_inventory_dataset('Swiss',std.traits='global');"

$(D2)/Swiss/traits.csv: scripts/find.trait/Swiss.R R/find.trait/trait-fun.R $(D2)/Swiss/tree.csv $(D2)/TRY/data.TRY.std.rds
	Rscript $<

$(D2)/Swiss/tree.csv: scripts/format.data/Swiss.R R/format.data/format-fun.R $(shell find $(D1)/Swiss -type f)
	Rscript $<

