Luquillo:  $(D3)/Luquillo/Done.no.txt $(D3)/Luquillo/Done.global.txt

$(D3)/Luquillo/Done.no.txt: R/process.data/process-fun.R $(D2)/Luquillo/traits.csv
	Rscript -e "source('$<'); process_bigplot_dataset('Luquillo', Rlim=15,std.traits='no');"

$(D3)/Luquillo/Done.global.txt: R/process.data/process-fun.R $(D2)/Luquillo/traits.csv
	Rscript -e "source('$<'); process_bigplot_dataset('Luquillo', Rlim=15,std.traits='global');"

$(D2)/Luquillo/traits.csv: scripts/find.trait/Luquillo.R R/find.trait/trait-fun.R $(D2)/Luquillo/tree.csv
	Rscript $<

$(D2)/Luquillo/tree.csv: scripts/format.data/Luquillo.R R/format.data/format-fun.R $(shell find $(D1)/Luquillo -type f)
	Rscript $<
