
Sweden:  $(D3)/Sweden/Done.no.txt $(D3)/Sweden/Done.global.txt

$(D3)/Sweden/Done.no.txt: R/process.data/process-fun.R  $(D2)/Sweden/traits.csv
	Rscript -e "source('$<'); process_inventory_dataset('Sweden',std.traits='no');"

$(D3)/Sweden/Done.global.txt: R/process.data/process-fun.R  $(D2)/Sweden/traits.csv
	Rscript -e "source('$<'); process_inventory_dataset('Sweden',std.traits='global');"

$(D2)/Sweden/traits.csv: scripts/find.trait/Sweden.R R/find.trait/trait-fun.R $(D2)/Sweden/tree.csv $(D2)/TRY/data.TRY.std.rds
	Rscript $<

$(D2)/Sweden/tree.csv: scripts/format.data/Sweden.R R/format.data/format-fun.R $(shell find $(D1)/Sweden -type f)
	Rscript $<

