
### Competition index

We computed the sum of basal area (BA) per plot (including the weighting of each tree to have a basal area in $m^2/ha$) total and per species without the  BA of the target tree (see the R function BA.SP.FUN in the file format.function.R).

### Climatic data

For each plots the monthly temperature and total precipitation was taken from a GIS data base at $1 km_2$ resolution developed by Piedallu et al. (2012). The solar radiation accounting for cloudiness cover was also retrieved for each plots from a data base at 1 $km_2$ resolution (Piedallu & Gegout 2008). The temperature was corrected for the actual elevation of the plot using geospatial krigging of the temperature laps rate.

Based on this data we computed the sum of degree days above 5.56 $^\circ C$ ($SDD$) and a water stress index computed from a monthly water budget (using the model of Bugmann & Cramer 1998) ($WB$).

* To compute the $SDD$ we used a spline of the average monthly temperatures.
* The water stress index ($WS$) is based on the ratio of the actual evapotranspiration over the potential evapotranspiration (the details of the calculation is presented below).

#### Water budget model
The monthly potential evapotranspiration ($PET_m$) was computed using the Turc equation (Turc, 1961).
\begin{equation}
PET_m = n \times 0.0133333 \times (Rg_m +50) \times (t_m/(t_m+15)
\end{equation}
with $n$ = number of days of the month, $t_m$ = the monthly temperature and $Rg_m$ = the monthly radiation.
	The water budget computed monthly soil water content ($SWC_m$), with initial condition for January $SWC_m$ set as $SWC_{max}$ (the maximum soil water content). Then monthly soil water content was iteratively computed using the following equation.
\begin{equation}
SWC_{m+1} =  min(SWC_m+Ps_m - AET_m,SWC_{max})
\end{equation}

with $Ps_m$ = the infiltrating precipitation, $AET_m$ = the monthly actual evapotranspiration.
$AET_m = min(D_m,S_m)$
with $D_m = PET_m – Pi_m$ where $Pi_m$ is the the intercepted precipitation.
and $S_m = cw *SWC_m/SWC_{max}$ where $cw$ is a parameter denoting the maximum evapotranspiration from a saturated soil under conditions of high demand (as in Bugmann & Cramer 1998 we assume that $cw$ = 12 cm/month).

$Pi_m$ and $Ps_m$ are computed as:
$Pi_m = min(fi * P_m , PET_m)$
with $fi$ = a parameter denoting the fraction of precipitation that is intercepted and is set at a value of 0.3 following Bugmann & Cramer (1998), and $P_m$ = the monthly precipitation.
$Ps_m =  P_m - Pi_m$

The water stress index was computed as
\begin{equation}
WS =   \frac{\sum_{m=1}^{12} AET_m}{\sum_{m=1}^{12} PET_m}
\end{equation}

#### Maximum soil water content

$SWC_{max}$ was computed following (Piedallu et al. 2011) as
\begin{equation}
SWC_{max} =  (1-RO)\times (\sum_{i=1}^n {(1-{\sqrt{SC_i}}^3) \times (\theta_i^{2.0} - \theta_i^{4.2}) \times T_i})
\end{equation}
with $n$ the number of horizons in the soil profile. $SC_i$ is the stone proportional content in horizon $i$, $\theta_i^2.0$ and $\theta_i^4.2$ are the water content at respectively  -100 hPa and -15000hPa matric potential of horizon $i$ (according to Al Majou et al. 2008), $T_i$ is the thickness of the horizon $i$ in millimeters and $RO$ is the proportion of rock outcrop recoded for the plot.

### Status

####  Alive tree

(@) In 2005 and 2006 the Variable $veget$ had either the value 0 no damaged or $Z$ damaged.  From 2007 the damaged have been recorded in the variable $acci$ with the value 0 for no damage and 1 to 5 for different type of damage. A variable $vege4 with value 0 no damage or 1 damage have been created for all year.

(@) Variable $orir$ give the origin of the tree: recruit from seed (1) or from resprouting (0 only in 2005 and 2006 - but 0 for resprout and 2 for resprout from wind thrown tree from 2007 and onward).

(@) Variable $simplif$ show which the tree that were simplified only after 2009.

(@) Variables $sfgui$ $sfgeliv$ $sfpied$ $sfdorge$ $sfcoeur$ were provided only after 2009.


Note that radial growth was not measured for all tree in a plot. If the number of tree of given species and a given size class ($C_{130}$ classes 23.5-70.5, 70.5-117.5, 117.5-164.5, >164.5cm) is greater than 6, the radial growth is measured only on 6 individuals.

#### Dead tree

(@) Before 2008 only $C_0$ circumference at the base of the tree was provided for the dead tree, not $C_{130}$ circumference at 1m30. Using data from the previous French NFI in the Alps and the Jura (see Kunstler et al. 2011 for a description of the data) that was recording both $C_0$ and $C_{130}$ we fitted a RMA linear regression (with package $lmodel2$ in R) between $C_0$ and $C_{130}$ and then predicted the $C_{130}$ for each dead individual from 2005 to 2007. Note that the species description was not as accurate in the previous NFI data (code with only number grouping species together for rare species) so the prediction have been done at this taxonomic levels and when no data was available the model was fitted with all species together.

(@) The variable $datemort$ providing an estimation of the date of death was no recored before 2008.


Then data for alive and data for dead tree were merged.

### References

- Al Majou, H., Bruand, A., Duval, O., (2008) The use of in situ volumetric water content at field capacity to improve the prediction of soil water retention properties. Canadian Journal of Soil Science, **88**, 533-541.
- Bugmann, H. & Cramer, W. (1998) Improving the behaviour of forest gap models along drought gradients. Forest Ecology and Management,**103**, 247-263.
- Kunstler, G., Albert, C.H., Courbaud, B., Lavergne, S., Thuiller, W., Vieilledent, G., Zimmermann, N.E., Coomes, D.A. (2011) Effects of competition on tree radial-growth vary in importance but not in intensity along climatic gradients. Journal of Ecology, **99**, 300–312.
- Piedallu, C., and Gegout, G. (2008) Efficient Assessment of Topographic Solar Radiation to Improve Plant Distribution Models. Agricultural and Forest Meteorology, **148**, 1696–1706.
- Piedallu, C., J. C. Gégout, A. Bruand, & Seynave, I. (2011) Mapping Soil Water Holding Capacity over Large Areas to Predict Potential Production of Forest Stands. Geoderma, **160**, 355–366.
- Piedallu, C., J. C. Gégout, V. Perez, F. Lebourgeois, and Field, R. (2012) Soil Water Balance Performs Better Than Climatic Water Variables in Tree Species Distribution Modelling. Global Ecology and Biogeography, **22**, 478-482.
- Plummer, M. (2003) JAGS: A program for analysis of Bayesian graphical models using Gibbs sampling. In Proceedings of the 3rd International Workshop on Distributed Statistical Computing (DSC 2003). March, pp. 20–22.
- Turc, L. (1961) Evaluation des besoins en eau d’irrigation, évapotranspiration potentielle. Annales Agronomiques, **12**, 13-49.

