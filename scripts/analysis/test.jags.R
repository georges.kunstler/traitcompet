### test jags
source('R/analysis/jags.run.R')
library(runjags)


## test different models

for(trait in c('SLA', 'Wood.density', 'Max.height')){
 run.jags.b(model.files.jags.Tf.1[1], trait = trait,
                     init.TF = TRUE, var.sample = 'wwf',
                     sample.size = 20000, iter = 200,
                     warmup = 20, chains = 4,
                     thin = 2)
}

for(trait in c('Wood.density')){
 run.jags.b(model.files.jags.Tf.1[1], trait = trait,
                     init.TF = FALSE, var.sample = 'wwf',
                     sample.size = 10000, iter = 15000,
                     warmup = 5000, chains = 2,
                     thin = 15, method = 'parallel')
}



### COMPARE JAGS AND LMER
source('R/analysis/lmer.output-fun.R')

list.lmer.results.set.sample <-
    readRDS('output/list.lmer.out.all.NA.simple.sample.rds')

mean.l <- list.lmer.results.set.sample[[2]]$lmer.summary$fixed.coeff.E
sd.l <- list.lmer.results.set.sample[[2]]$lmer.summary$fixed.coeff.Std.Error
l.l <- mean.l -1.96*sd.l
h.l <- mean.l +1.96*sd.l
mean.l[c(4,6,7)] <- -mean.l[c(4,6,7)]
l.l[c(4,6,7)] <- -l.l[c(4,6,7)]
h.l[c(4,6,7)] <- -h.l[c(4,6,7)]
names(l.l) <- names(mean.l)
names(h.l) <- names(mean.l)
 

list.jags.results.set.sample <-
    readRDS('output/list.jags.out.all.NA.simple.set.sample.rds')

mean.j <- list.jags.results.set.sample[[2]]$list.summary$summary[, 'Mean']
l.j <- list.jags.results.set.sample[[2]]$list.summary$summary[, 'Lower95']
h.j <- list.jags.results.set.sample[[2]]$list.summary$summary[, 'Upper95']

mean.j <- mean.j[c(1,3,2,4,5,6,7)]
l.j <- l.j[c(1,3,2,4,5,6,7)]
h.j <- h.j[c(1,3,2,4,5,6,7)]
names(mean.j) <- names(mean.l)
names(l.j) <- names(mean.l)
names(h.j) <- names(mean.l)

mean.j[c(4,6,7)] <- -mean.j[c(4,6,7)]
l.j[c(4,6,7)] <- -l.j[c(4,6,7)]
h.j[c(4,6,7)] <- -h.j[c(4,6,7)]

param.vec <- c("sumTnTfBn.abs", "sumTfBn","sumTnBn",
                   "sumBn", "Tf")
col.names <- fun.col.param()

param.names <- c(expression('Trait sim '(alpha['s'])),
                expression('Tolerance '(alpha['t'])),
                expression('Effect '(alpha['e'])),
                expression('Trait indep'(alpha[0])),
                expression("Direct trait "(m[1])))




par(mai=c(1.2, 2.8,0.6,0.5), xpd = TRUE)
plot(mean.l[param.vec], (1:length(param.vec)+0.1),
         yaxt = 'n', xlab = NA, ylab = NA,
         pch = 16, cex = 2, cex.lab = 1.7, cex.axis = 1.5,
         ylim = range(1-0.21, length(param.vec)+0.21), xlim = c(-0.28, 0.27))
    mtext(traits.names[i], side=3,  cex =1.7, line = 1)
    box(lwd= 2)
    lines(c(0, 0), c(par()$usr[3], par()$usr[4]))
    lapply(1:length(param.vec),
           fun.axis.one.by.one,
           side = 2,
           labels = param.names,
           cols.vec = col.names[param.vec])
segments(l.l[param.vec], 1:5+0.1, h.l[param.vec], 1:5+0.1)
points(mean.j[param.vec], (1:length(param.vec)-0.1), cex = 2, col ='red')
segments(l.j[param.vec], 1:5-0.1, h.j[param.vec], 1:5-0.1, col = 'red')

# lmer vs jags
pdf('figs/lmer.vs.jags.pdf')
plot(mean.l[param.vec], mean.j[param.vec], cex = 2,
     xlab = 'lmer estimates', ylab = 'jags estimates', xlim = c(-0.28, 0.3), ylim = c(-0.28, 0.3))
segments(l.l[param.vec],mean.j[param.vec], h.l[param.vec], mean.j[param.vec])
segments(mean.l[param.vec], l.j[param.vec], mean.l[param.vec], h.j[param.vec])
lines(c(-1, 1), c(-1, 1), lty = 2)
dev.off()
