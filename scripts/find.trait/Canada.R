#!/usr/bin/env Rscript

##### FORMAT TRAIT FOR Canada
source("R/find.trait/trait-fun.R")

### read species names
data.tree <- read.csv("output/formatted/Canada/tree.csv",
                      stringsAsFactors = FALSE)
species.clean <-
    data.frame(sp = data.tree[!duplicated(data.tree[["sp"]]), "sp"],
               Latin_name = data.tree[!duplicated(data.tree[["sp"]])
                                                  , "sp.name"],
               Latin_name_syn = data.tree[!duplicated(data.tree[["sp"]]),
                                                     "sp.name"],
               stringsAsFactors = FALSE)


## read in data
data.TRY.std <- readRDS("output/formatted/TRY/data.TRY.std.rds")
## read us max height
max.height <- read.csv(file="output/formatted/US/max.height.csv",
                       stringsAsFactors = FALSE)

#manually assign max height to Alnus spp. (alder),
# from: http://plants.usda.gov/java/charProfile?symbol=ALINR
max.height[dim(max.height)[1]+1, ] <- c(dim(max.height)[1]+1, 350, 4.9, NA, NA)

max.height$sp <- paste("sp", max.height$sp, sep = ".")

data.traits <- fun.extract.format.sp.traits.TRY(sp = species.clean[["sp"]],
                                                sp.syno.table = species.clean,
                                                data = data.TRY.std)

data.traits <- merge(data.traits, subset(max.height,
                                        select = c("sp", "Max.height.mean",
                                                 "Max.height.sd")),
                     by = "sp", all.x = TRUE, all.y = FALSE)
data.traits$Max.height.genus <- FALSE

height.genus.DF <- do.call("rbind", lapply(data.traits$Latin_name,
                                          fun.compute.mean.genus, data.traits,
                                          "Max.height.mean"))
data.traits[is.na(data.traits[["Max.height.mean"]]),
            c("Max.height.mean", "Max.height.sd", "Max.height.genus")] <-
                    height.genus.DF[is.na(data.traits[["Max.height.mean"]]),  ]

#### GET THE ANGIO/CONIF AND EVERGREEN/DECIDUOUS
# read try categrocial data
try.cat <-
    read.csv("data/raw/TRY/TRY_Categorical_Traits_Lookup_Table_2012_03_17_TestRelease.csv",
             stringsAsFactors = FALSE, na.strings = "")
Pheno.Zanne <- read.csv("data/raw/ZanneNature/GlobalLeafPhenologyDatabase.csv",
         stringsAsFactors = FALSE)
# extract
data.cat.extract <- do.call("rbind", lapply(data.traits$sp ,
                                            fun.get.cat.var.from.try,
                                            data.traits, try.cat, Pheno.Zanne))
# change category
data.cat.extract <- fun.change.factor.pheno.try(data.cat.extract)
data.cat.extract <- fun.change.factor.angio.try(data.cat.extract)
data.cat.extract <- fun.fill.pheno.try.with.zanne(data.cat.extract)

## fix pheno for species with issue
data.cat.extract[data.cat.extract$Latin_name %in%
                 c('Betula spp.', 'Crataegus spp.', 'Fraxinus spp.',
                    'Malus spp.', 'Amelanchier spp.', 'Alnus spp.',
                   'Tilia spp.', 'Ulmus spp.', "Salix spp."), 'Pheno.T'] <- 'D'


data.traits <- merge(data.traits,
                     data.cat.extract[, c("sp", "Phylo.group",
                                          "Pheno.T", 'LeafType.T')],
                     by = "sp")

## create a vector with three categories C A_EV and A_D
## put EV_D in EV
data.traits$cat <- fun.three.cat.C.A_EV.A_D(data.traits$Pheno.T,
                                            data.traits$Phylo.group)

###
write.csv(data.traits, file = "output/formatted/Canada/traits.csv",
          row.names = FALSE)

