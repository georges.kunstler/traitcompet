#!/usr/bin/env Rscript
rm(list = ls())

##### FORMAT TRAIT FOR SPAIN
source("R/find.trait/trait-fun.R")

### read species names
data.tree <- read.csv("output/formatted/Spain/tree.csv",
                      stringsAsFactors = FALSE)
species.clean <-
    data.frame(sp = data.tree[!duplicated(data.tree[["sp"]]), "sp"], 
               Latin_name = data.tree[!duplicated(data.tree[["sp"]]), "sp.name"], 
               Latin_name_syn = data.tree[!duplicated(data.tree[["sp"]]),
                                          "sp.name"], 
               stringsAsFactors  = FALSE)

species.clean$Latin_name[species.clean$sp=='sp.243'] <- 'Quercus pubescens'
species.clean$Latin_name_syn[species.clean$sp=='sp.243'] <- 'Quercus pubescens'
species.clean$Latin_name[species.clean$sp=='sp.258'] <- 'Populus canadensis'
species.clean$Latin_name_syn[species.clean$sp=='sp.258'] <- 'Populus canadensis'

data.tree$sp.name[data.tree$sp=='sp.243'] <- 'Quercus pubescens'
data.tree$sp.name[data.tree$sp=='sp.258'] <- 'Populus canadensis'
write.csv(data.tree, "output/formatted/Spain/tree.csv",
                      row.names = FALSE)

## select column to keep

## read in data
data.TRY.std <- readRDS("output/formatted/TRY/data.TRY.std.rds")
max.height <- read.csv(file = "output/formatted/Spain/max.height.csv",
                       stringsAsFactors = FALSE)
max.height$sp <- paste("sp", max.height$sp, sep = ".")

## extract traits and height
data.traits <- fun.extract.format.sp.traits.TRY(sp = species.clean[["sp"]], 
                                                sp.syno.table = species.clean,
                                                data = data.TRY.std)
data.traits <- merge(data.traits, subset(max.height,
                                         select = c("sp", "Max.height.mean",
                                                    "Max.height.sd")), 
                     by = "sp", all.x = TRUE, all.y = FALSE)
data.traits$Max.height.genus <- FALSE
# genus mean for height
height.genus.DF <- do.call("rbind", lapply(data.traits$Latin_name, 
                                          fun.compute.mean.genus,
                                           data.traits,
                                           "Max.height.mean"))
data.traits[is.na(data.traits[["Max.height.mean"]]), 
            c("Max.height.mean", "Max.height.sd", "Max.height.genus")] <-
    height.genus.DF[is.na(data.traits[["Max.height.mean"]]), ]


#### GET THE ANGIO/CONIF AND EVERGREEN/DECIDUOUS
# read try categrocial data
try.cat <- read.csv("data/raw/TRY/TRY_Categorical_Traits_Lookup_Table_2012_03_17_TestRelease.csv", 
         stringsAsFactors = FALSE, na.strings = "")
Pheno.Zanne <- read.csv("data/raw/ZanneNature/GlobalLeafPhenologyDatabase.csv", 
         stringsAsFactors = FALSE)
# extract
data.cat.extract <- do.call("rbind", lapply(data.traits$sp ,
                                            fun.get.cat.var.from.try, 
                                            data.traits, try.cat, Pheno.Zanne))
# change category
data.cat.extract <- fun.change.factor.pheno.try(data.cat.extract)
data.cat.extract <- fun.change.factor.angio.try(data.cat.extract)
data.cat.extract <- fun.fill.pheno.try.with.zanne(data.cat.extract)

## fix pheno for species with issue
data.cat.extract[data.cat.extract$Latin_name %in% c('Crataegus spp.', 
                                                    'Crataegus laciniata', 
                                                    'Pyrus spp.', 'Morus spp.', 
                                                    'Salix spp.', 
                                                    'Betula spp.', 
                                                    'Salix elaegnos', 
                                                    'Tilia spp.', 
                                                    'Sorbus spp.', 
                                                    'Prunus spp.', 'Larix spp.',
                                                    "Salix elaeagnos"),
                 'Pheno.T'] <- 'D'

data.cat.extract[data.cat.extract$Latin_name %in% c('Juniperus turbinata',
                                                    'Otros pinos',
                                                    "Eucalyptus gomphocephalus",
                                                    "Otros eucaliptos",
                                                    "Otras laurisilvas",
                                                    "Phoenix spp."),
                 'Pheno.T']<-  'EV'



data.traits <- merge(data.traits, data.cat.extract[, c("sp", "Phylo.group", 
                                                     "Pheno.T", 'LeafType.T')], 
                     by = "sp")

## fill missing phylo missing value

data.traits[data.traits$Latin_name %in% c('Otros pinos'),
            'Phylo.group'] <- 'Gymnosperm'
data.traits[data.traits$Latin_name %in% c('Otros eucaliptos',
                                          'Otros arboles ripicolas',
                                          'Otras laurisilvas',
                                          'Otras frondosas'),
            'Phylo.group'] <- 'Angiosperm'

## create a vector with three categories C A_EV and A_D
## put EV_D in EV
data.traits$cat <- fun.three.cat.C.A_EV.A_D(data.traits$Pheno.T, 
                                            data.traits$Phylo.group)


# write
write.csv(data.traits, file = "output/formatted/Spain/traits.csv",
          row.names = FALSE)

