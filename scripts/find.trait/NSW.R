##### FORMAT TRAIT FOR NSW
source("R/find.trait/trait-fun.R")

### read species names
data.tree <- read.csv("output/formatted/NSW/tree.csv", stringsAsFactors = FALSE)
species.clean <- data.frame(sp=data.tree[!duplicated(data.tree[["sp"]]),"sp"],
                             Latin_name=data.tree[!duplicated(data.tree[["sp"]]),"sp.name"],
                             Latin_name_syn=data.tree[!duplicated(data.tree[["sp"]]),"sp.name"],
                             stringsAsFactors =FALSE)

species.clean$Latin_name <-  trim.trailing(species.clean$Latin_name)
                             
################ MASSAGE TRAIT DATA Obtain maximum height per species from data.trait no sd
################ available as we have only one observation for species
###################################### MASSAGE TRAIT DATA
data.trait <- read.csv("data/raw/NSW/NSW_traits.csv", header = TRUE, stringsAsFactors = FALSE)
data.trait$sp <- data.trait[["Species.all"]]; data.trait[["Species.all"]] <- NULL ## There is not sp.code in data.nsw; using spp name as code
data.trait$Latin_name <- trim.trailing(data.trait$sp)
data.trait$Leaf.N.mean <- NA
data.trait$Leaf.N.sd <- NA
data.trait$Seed.mass.mean <- exp(data.trait$SDM_log10_g)*1000; data.trait$SDM_log10_g <- NULL ## conversion from log10 g to mg POTENTIAL ERROR log10 or log VALUE TOO LAREG >5kg. ROB SAY NOT LOG10 but log !
data.trait$Seed.mass.sd <- NA
data.trait$SLA.mean <- NA
data.trait$SLA.sd <- NA
data.trait$Wood.density.mean <- data.trait[["WD_basic_kg.m3"]]/1000; data.trait[["WD_basic_kg.m3"]] <- NULL ## conversion from kg/m3 to mg/mm3
data.trait$Wood.density.sd <- NA
data.trait$Max.height.mean <- 10^(data.trait$Log10_Hmax_m); 
data.trait$Log10_Hmax_m <- NULL
data.trait$Max.height.sd <- NA

data.TRAITS.std <- data.trait
rm(data.trait)

## extract
data.traits <- fun.extract.format.sp.traits.NOT.TRY(sp=species.clean$sp, Latin_name=species.clean$Latin_name, data=data.TRAITS.std,name.match.traits="sp")


#### GET THE ANGIO/CONIF AND EVERGREEN/DECIDUOUS
# read try categrocial data
try.cat <- read.csv("data/raw/TRY/TRY_Categorical_Traits_Lookup_Table_2012_03_17_TestRelease.csv",
         stringsAsFactors=FALSE,na.strings = "")
Pheno.Zanne <- read.csv("data/raw/ZanneNature/GlobalLeafPhenologyDatabase.csv",
         stringsAsFactors=FALSE)
# extract
data.cat.extract <- do.call("rbind",lapply(data.traits$sp ,fun.get.cat.var.from.try,
                                           data.traits,try.cat,Pheno.Zanne))
# change category
data.cat.extract <- fun.change.factor.pheno.try(data.cat.extract)
data.cat.extract <- fun.change.factor.angio.try(data.cat.extract)
data.cat.extract <- fun.fill.pheno.try.with.zanne(data.cat.extract)


data.traits <- merge(data.traits,data.cat.extract[,c("sp","Phylo.group","Pheno.T",'LeafType.T')],by="sp")
## fix pheno for species with issue

data.traits[data.traits$Latin_name %in% c('Cyclophylum longipetalum'),
            'Phylo.group'] <- 'Angiosperm'
data.traits[data.traits$Latin_name %in% c("Cyclophylum longipetalum",
                                          "Syzygium ingens",
                                          "Ackama paniculata",
                                          "Daphnandra apatela",
                                          "Emmenosperma alphitinioides"),
            'Pheno.T'] <- 'EV'

## create a vector with three categories C A_EV and A_D
## put EV_D in EV
data.traits$cat <- fun.three.cat.C.A_EV.A_D(data.traits$Pheno.T,
                                            data.traits$Phylo.group)



### 
write.csv(data.traits,file="output/formatted/NSW/traits.csv",row.names = FALSE)




