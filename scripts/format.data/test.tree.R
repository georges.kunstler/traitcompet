################################
### RUN test tree
rm(list = ls())

source("R/format.data/test.tree-fun.R")
##################
## do the test
## run in GK_competition/data.format.workshop

sets.I <- c("Canada", "France", "NSW", "NVS",
            "Sweden", "Swiss",  "Spain", "US")
sets.B <- c("BCI", "Fushan", "Paracou", "Mbaiki", "Japan", "Luquillo")

filedir <- "output/formatted"
dir.create("figs/test.format.tree", recursive = TRUE, showWarnings = FALSE)

## test set
I <- lapply(sets.I, fun.test.set.I, filedir)
B <- lapply(sets.B, fun.test.set.B, filedir)

## compute summary for vars and plots
array.summary <- function.summary.table.sets(c(sets.I, sets.B),
                                             vars = c('D', 'G',
                                                      'BA.G', 'weights'),
                                             filedir)
pdf("figs/test.format.tree/range.D.G.BAG.pdf")
lapply(c('D', 'G', 'BA.G', 'weights'), fun.plot.m.q, array.summary)
dev.off()


lapply(c('MAT', 'MAP'), fun.hist.var.all.set, sets = sets.I, filedir)

fun.plot.xy.all.sets(sets = c(sets.B, sets.I), x = 'D', y = 'G',
                     filedir, xlim = c(10, 300), ylim = c(-100, 200))
fun.plot.xy.all.sets(sets = c(sets.B, sets.I), x = 'D', y = 'BA.G',
                     filedir, xlim = c(10, 300), ylim = c(-200, 500))

print("check figures in figs/test.format.tree")

cat("finished", file = file.path("figs/test.format.tree", "Done.txt"))
