#!/usr/bin/env Rscript

###### READ TRY AND FORMAT DATA AND CHECK ERROR
################ use AccSpeciesName because not author name

source("R/format.data/try-fun.R")

out.dir <- "output/formatted/TRY"
dir.create(out.dir, recursive = TRUE, showWarnings = FALSE)

## read TRY data
TRY.DATA <-
    read.table("data/raw/TRY/TRY_Proposal_177_DataRelease_2013_04_01.txt",
               sep = "\t",
               header = TRUE,
               na.strings = "",
               stringsAsFactors = FALSE)

TRY.DATA2 <-
    read.table("data/raw/TRY/TRY_Proposal_177_DataRelease_2013_07_23.txt",
                sep = "\t",
               header = TRUE,
               na.strings = "", stringsAsFactors = FALSE)

### combine both data set
TRY.DATA <- rbind(TRY.DATA, TRY.DATA2)
rm(TRY.DATA2)
######## ERROR FOUND IN THE DATA BASE
## 1 problem with the seed mass of this obs seed mass
################################## = 0 DELETE
TRY.DATA <- TRY.DATA[!(TRY.DATA$ObservationID == 1034196 &
                       TRY.DATA$DataName == "Seed dry mass"),   ]

## error found in the SLA not good range (above 100 mm2 mg-1)
TRY.DATA <- TRY.DATA[!(TRY.DATA$ObservationID %in%
                       c(950267, 950268, 950269, 950270, 950271, 950272)),   ]

#### IS 'Quercuscrispla sp' an error standing for Quercus crispula synonym of
#### Quercus mongolica subsp. crispula (Blume) Menitsky ? ask Jens
#### TRY.DATA[TRY.DATA$AccSpeciesName=='Quercuscrispla sp' ,]


## WRITE Lookup table for reference to LastName

Ref.Lookup.Table <-
data.frame(Reference = TRY.DATA$OrigValueStr[ TRY.DATA$DataName == 'Reference'],
           LastName = TRY.DATA$LastName[ TRY.DATA$DataName == 'Reference'],
           stringsAsFactors = FALSE)

Ref.Lookup.Table <-  Ref.Lookup.Table[!duplicated(Ref.Lookup.Table$Reference),]
write.csv(Ref.Lookup.Table,
          file = "output/formatted/TRY/Ref.lookup.table.csv",
          row.names = FALSE)

## first create a table with one row per Observation.id and
## column for each traits and variable
Non.Trait.Data <- c("Latitude", "Longitude", "Reference",
                    "Date of harvest / measurement",
                    "Altitude", "Mean annual temperature (MAT)",
                    "Mean sum of annual precipitation (PPT)",
                    "Plant developmental status / plant age",
                    "Maximum height reference", "Source in Glopnet",
                    "Number of replicates", "Sun vers. shade leaf qualifier")

Trait.Data <- sort(names(table(TRY.DATA$TraitName)))

library(data.table)
TRY.DT <- as.data.table(TRY.DATA[!is.na(TRY.DATA$DataName), ])
setkey(TRY.DT, 'ObservationID', 'DataName')
TRY.DT2 <- as.data.table(TRY.DATA[!is.na(TRY.DATA$TraitName), ])
setkey(TRY.DT2, 'ObservationID','TraitName')

########################## REFORMAT DATA from TRY
registerDoParallel(cores = 15)

getDoParWorkers()
system.time(
TRY.DATA.FORMATED <- foreach(ObservationID.t = unique(TRY.DATA$ObservationID),
                             .combine = rbind) %dopar%
    {
        fun.extract.try(ObservationID.t, data = TRY.DT, data2 = TRY.DT2,
                        Non.Trait.Data, Trait.Data)
    }
)


TRY.DATA.FORMATED <- data.frame(lapply(data.frame(TRY.DATA.FORMATED),
                                        function(x) (unlist(x))))


saveRDS(TRY.DATA.FORMATED, file = file.path(out.dir, "TRY.DATA.FORMATED.rds"))

######### READ RDS
TRY.DATA.FORMATED$AccSpeciesName <-
    as.character(TRY.DATA.FORMATED$AccSpeciesName)

### select only the five traits we will use
traits <- c("Leaf.nitrogen..N..content.per.dry.mass",
            "Seed.mass",
            "Leaf.specific.area..SLA.",
            "Stem.specific.density..SSD.",
            "Plant.height.vegetative")
data.TRY.std <- subset(as.data.frame(TRY.DATA.FORMATED),
                       select = c("ObservationID", "AccSpeciesName",
                                "TF.exp.data",
                                "Reference", "LastName",
                                "Sun.vers..shade.leaf.qualifier",
                                 traits))
names(data.TRY.std) <- c("obs.id", "Latin_name", "TF.exp.data", "Reference",
                         "LastName",
                         "Sun.vs.shade.leaf", "Leaf.N",  "Seed.mass", "SLA",
                         "Wood.density", "height")

saveRDS(data.TRY.std, file = file.path(out.dir, "data.TRY.std.rds"))
write.csv(data.TRY.std, file = file.path(out.dir, "data.TRY.std.csv"),
          row.names = FALSE)
