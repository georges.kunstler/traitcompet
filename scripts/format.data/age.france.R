################################ READ DATA
data.france <- read.csv("data/raw/France/dataIFN.FRANCE.csv",
                        stringsAsFactors = FALSE)

library(dplyr)

data.age <- data.france %>% group_by(idp) %>% summarise(age.max = max(age, na.rm = TRUE))



# plot age dist
library(ggplot2)
theme_simple <-  function(){
  theme_bw() +
  theme(
    plot.background = element_blank(),
    panel.grid.major = element_blank(),
    panel.grid.minor = element_blank(),
    legend.title=element_blank(),
    strip.background = element_blank(),
    legend.key = element_blank(),
    legend.position=c(.1,.15),
    strip.text = element_text(size=14),
   axis.title.x = element_text(size=14),
   axis.title.y = element_text(size=14)
  ) +
  #draws x and y axis line
  theme(axis.line = element_line(color = 'black'))
}

pdf('figs/age.france.pdf')
ggplot(data.age, aes(x = age.max)) + geom_histogram(aes(y = ..density..), colour = "black",  fill="grey") + geom_density(, col = 'red')+  scale_x_log10(limits=c(1, 1000))  +theme_simple() + ggtitle("France") + xlab("age (yr.)")
dev.off()



