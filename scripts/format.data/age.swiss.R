######################### READ DATA read individuals tree data
data.swiss1 <- read.csv("data/raw/Swiss/LFI12.csv",
                        header = TRUE,
                        stringsAsFactors = FALSE)
data.swiss2 <- read.csv("data/raw/Swiss/LFI23.csv",
                        header = TRUE, stringsAsFactors = FALSE)
data.swiss3 <- read.csv("data/raw/Swiss/LFI34.csv",
                        header = TRUE,
                        stringsAsFactors = FALSE)
data.swiss <- rbind(data.swiss1, data.swiss2, data.swiss3)
rm(data.swiss1, data.swiss2, data.swiss3)
data.swiss <- data.swiss[order(data.swiss$BANR), ]

library(dplyr)

data.age <- data.swiss %>% group_by(CLNR) %>% summarise(age.max = max(ALTERD1, na.rm = TRUE))



# plot age dist
library(ggplot2)
theme_simple <-  function(){
  theme_bw() +
  theme(
    plot.background = element_blank(),
    panel.grid.major = element_blank(),
    panel.grid.minor = element_blank(),
    legend.title=element_blank(),
    strip.background = element_blank(),
    legend.key = element_blank(),
    legend.position=c(.1,.15),
    strip.text = element_text(size=14),
   axis.title.x = element_text(size=14),
   axis.title.y = element_text(size=14)
  ) +
  #draws x and y axis line
  theme(axis.line = element_line(color = 'black'))
}

pdf('figs/age.swiss.pdf')
ggplot(data.age, aes(x = age.max)) + geom_histogram(aes(y = ..density..), colour = "black",  fill="grey") +
    geom_density(, col = 'red')+  scale_x_log10(limits=c(1, 1000))  +theme_simple() +
    ggtitle("Swiss") + xlab("age (yr.)")
dev.off()



