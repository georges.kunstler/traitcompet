#!/usr/bin/env Rscript


### MERGE BCI DATA
rm(list = ls())
source("R/format.data/format-fun.R")
dir.create("output/formatted/BCI", recursive = TRUE, showWarnings = FALSE)

library(reshape, quietly = TRUE)
### READ DATA read individuals tree data Requires careful formatting of 7 census
## datasets The raw data is such that, once a tree dies in census X, then it no
## longer exists in census X+1, X+2 etc...
data.bci1 <- read.csv("data/raw/BCI/stem.data/BCI.all.plots.census.allstems.1.csv",
                      header = TRUE,
                      stringsAsFactors = FALSE)



data.bci1$Date1 <- data.bci1$Date; data.bci1$Date <- NULL
data.bci1$DBH1 <- data.bci1$DBH; data.bci1$DBH <- NULL
data.bci1$HOM1 <- data.bci1$HOM; data.bci1$HOM <- NULL
data.bci1$Codes1 <- data.bci1$Codes; data.bci1$Codes <- NULL
data.bci1$Status1 <- data.bci1$Status
data.bci1$Stem1 <- data.bci1$Stem

big.bci <- NULL

max_census = 7 

for (k in 2:max_census) {
    new.directory <- paste("data/raw/BCI/stem.data/BCI.all.plots.census.allstems", k, "csv",
                           sep = ".")
    data.bci2 <- read.csv(new.directory, header = TRUE,
                          stringsAsFactors = FALSE);
    data.bci2$ID <- paste(data.bci2$Plot, data.bci2$TreeID, data.bci2$StemID, 
                          sep = "")
    print(table(data.bci2$Stem))
    print(sum(is.na(data.bci2$Stem)))
    if (!is.null(big.bci)) {
        data.bci1$ID <- paste(data.bci1$Plot, data.bci1$TreeID, data.bci1$StemID,
                              sep = "")
        sub.bci <- merge(data.bci1[, c("Latin", "Plot", "Quadrat", "Census",
                                       "gx", "gy", "TreeID", 
                                       "StemID", "ID", "Stem1", "Date1", "DBH1",
                                       "HOM1", "Codes1", "Status1")], 
                         data.frame(ID = data.bci2[["ID"]], 
                                    DBH2 = data.bci2[["DBH"]],
                                    HOM2 = data.bci2[["HOM"]],
                                    Codes2 = data.bci2[["Codes"]], 
                                    Date2 = data.bci2[["Date"]],
                                    dead = as.numeric(data.bci2[["Status"]] ==
                                                      "dead"),
                                    Status2=data.bci2[["Status"]],
                                    Stem2=data.bci2[["Stem"]],
                                    stringsAsFactors = F),
                         sort = T, by = "ID", all.x = TRUE)
        ## Uses the Date1 as the census number
        big.bci <- rbind(big.bci,  sub.bci)
    }
    if (is.null(big.bci)) {
         data.bci1$ID <- paste(data.bci1$Plot, data.bci1$TreeID, data.bci1$StemID,
                                sep = "")
       big.bci <- merge(data.bci1[,  c("Latin", "Plot", "Quadrat", "Census",
                                       "gx", "gy", "TreeID", 
                                      "StemID", "ID", "Stem1", "Date1", "DBH1",
                                       "HOM1", "Codes1", "Status1")], 
                        data.frame(ID = data.bci2[["ID"]], 
                                   DBH2 = data.bci2[["DBH"]],
                                   HOM2 = data.bci2[["HOM"]],
                                   Codes2 = data.bci2[["Codes"]], 
                                   Date2 = data.bci2[["Date"]],
                                   dead = as.numeric(data.bci2[["Status"]] ==
                                            "dead"),
                                   Status2 = data.bci2[["Status"]],
                                   Stem2 = data.bci2[["Stem"]],
                                   stringsAsFactors = F),
                        sort = T, by = "ID", all.x = TRUE)
    }
	data.bci1 <- data.bci2
	data.bci1$Date1 <- data.bci1$Date; data.bci1$Date <- NULL
	data.bci1$DBH1 <- data.bci1$DBH; data.bci1$DBH <- NULL
	data.bci1$HOM1 <- data.bci1$HOM; data.bci1$HOM <- NULL
        data.bci1$Codes1 <- data.bci1$Codes; data.bci1$Codes <- NULL
        data.bci1$Status1 <- data.bci1$Status
        data.bci1$Stem1 <- data.bci1$Stem
   cat("Census",  k,  "now included. Dim big.bci", dim(big.bci), "\n")
#	print(summary(big.bci$DBH1)); print(summary(big.bci$DBH2))
}
rm(data.bci1, data.bci2, sub.bci)
big.bci <- big.bci[order(big.bci$ID), ]
data.bci <- big.bci
rm(big.bci)
### read species names
species.clean <- read.table("data/raw/BCI/TaxonomyDataReport.txt",
                            stringsAsFactors = FALSE,
                            header = T, sep = "\t")
species.clean$Latin_name <- paste(species.clean[["Genus"]],
    species.clean[["species"]], sep = " ")

## GET PALM AND TREE FERN TO REMOVE THEIR GROWTH
sp.palm.fern <- species.clean$Latin_name[species.clean$Family %in%
                                         c('Arecaceae', 'Cyatheaceae',
                                           'Dicksoniaceae', 
                                           'Metaxyaceae',  'Cibotiaceae', 
                                           'Loxomataceae',  'Culcitaceae', 
                                           'Plagiogyriaceae',
                                           'Thyrsopteridaceae')]


## ## relate SpeciesID in species.clean species names in data.bci
#unique(data.bci$Latin) %in% species.clean$Latin_name
data.bci <- merge(data.bci, data.frame(Latin = species.clean$Latin_name,
                                       sp = species.clean$SpeciesID,
                                       genus = species.clean$Genus,
                                       stringsAsFactors = F),
                  by = "Latin", sort = F)

## format sp as sp.
data.bci$sp <- paste("sp", data.bci$sp, sep = ".")

## select plot and census


## remove plot sherman
data.bci <- subset(data.bci, subset = data.bci[["Plot"]] != 'sherman')

## remove bci census 1 and 2 because too much error
data.bci <- subset(data.bci, subset=!(data.bci[["Plot"]] == 'bci' &
                                      data.bci[["Census"]] %in% c(1,2)))
# rename census from 1
data.bci[['Census']][data.bci[['Plot']]=='bci'] <-
    data.bci[['Census']][data.bci[['Plot']]=='bci'] -2



######## DEFINE THE PLOTS  AS DIFFERENT CLUSTER 
data.bci$cluster <- data.bci$Plot

########################################## FORMAT INDIVIDUAL TREE DATA
data.bci <- data.bci[order(data.bci[["TreeID"]]), ]
data.bci$Date1 <- as.Date(data.bci$Date1)
data.bci$Date2 <- as.Date(data.bci$Date2, format = '%Y-%m-%d')
# data.bci$yr1 <- format(strptime(data.bci$Date1, format = '%Y-%m-%d'),'%Y')
# data.bci$yr2 <- format(strptime(data.bci$Date2, format = '%Y-%m-%d'),'%Y')
data.bci$year <- as.numeric(difftime(data.bci$Date2,
                                     data.bci$Date1, units = "weeks")/52)
## Not rounded
data.bci$obs.id <- 1:nrow(data.bci) 
data.bci$tree.id <- data.bci$ID; 
data.bci$x <- data.bci$gx; data.bci$gx <- NULL
data.bci$y <- data.bci$gy; data.bci$gy <- NULL
data.bci$G <-  (data.bci$DBH2 - data.bci$DBH1)/data.bci$year
## diameter growth in mm per year - BASED ON UNROUNDED YEARS
data.bci$BA.G <- (pi*(data.bci$DBH2/20)^2 -
                  pi*(data.bci$DBH1/20)^2)/data.bci$year ## BA growth in cm2/yr
data.bci$G[data.bci$Status1=="missing" | data.bci$Status2=="missing"] <- NA
data.bci$G[abs(data.bci$HOM2-data.bci$HOM1)>0.005 &
           !is.na(data.bci$HOM2-data.bci$HOM1)] <- NA
data.bci$G[grepl('M', data.bci$Codes2)] <- NA
data.bci$G[grepl('R', data.bci$Codes2)] <- NA
data.bci$G[grepl('M', data.bci$Codes1)] <- NA
data.bci$G[grepl('R', data.bci$Codes1)] <- NA
data.bci$G[data.bci$Stem1 != 'main'] <- NA

data.bci$BA.G[data.bci$Status1=="missing" | data.bci$Status2=="missing"] <- NA
data.bci$BA.G[abs(data.bci$HOM2-data.bci$HOM1)>0.005 &
              !is.na(data.bci$HOM2-data.bci$HOM1)] <- NA
data.bci$BA.G[grepl('M', data.bci$Codes2)] <- NA
data.bci$BA.G[grepl('R', data.bci$Codes2)] <- NA
data.bci$BA.G[grepl('M', data.bci$Codes1)] <- NA
data.bci$BA.G[grepl('R', data.bci$Codes1)] <- NA
data.bci$BA.G[data.bci$Stem1 != 'main'] <- NA
data.bci$G[data.bci$Latin %in% sp.palm.fern] <-  NA ## remove fern and palm
data.bci$BA.G[data.bci$Latin %in% sp.palm.fern] <-  NA ## remove fern and palm
data.bci$dead[data.bci$Status1=="missing" | data.bci$Status2=="missing"] <- NA
data.bci$dead[grepl('M', data.bci$Codes2)] <- NA
data.bci$dead[grepl('R', data.bci$Codes2)] <- NA
data.bci$dead[grepl('M', data.bci$Codes1)] <- NA
data.bci$dead[grepl('R', data.bci$Codes1)] <- NA
data.bci$dead[data.bci$Stem1 != 'main'] <- NA
data.bci$D <- data.bci[["DBH1"]]/10 ## diameter in cm
data.bci$plot <- paste(data.bci[['cluster']], data.bci[["Quadrat"]])
data.bci$htot <- rep(NA, nrow(data.bci)) ## height of tree in m
data.bci$sp.name <- data.bci$Latin
data.bci$Latin <- NULL
data.bci$census <- data.bci$Census
data.bci$Census <- NULL



## LIMIT TO TREES > 10CM dbh
data.bci <- subset(data.bci, subset = data.bci[["D"]]>=10 &
                                     !is.na(data.bci[["D"]]))
### select only tree above 10cm of dbh at census 1

## read plots coordinates
coord <- read.csv('data/raw/BCI/Cndt_1ha_coordinates.csv',
                  header = TRUE, stringsAsFactors = FALSE)
coord <- coord[coord$plot %in% data.bci$cluster, ] 

#################
#### change coordinates system of x y to be in lat long WGS84
library(sp, quietly = TRUE)
library(dismo, quietly = TRUE)
library(rgdal, quietly = TRUE)
data.sp <- coord[, c("plot", "utm_x", "utm_y")]
coordinates(data.sp) <- c("utm_x", "utm_y")  # define x y
proj4string(data.sp) <- CRS("+init=epsg:32617")
# define projection system of our data ## EPSG CODE 32617  WGS84 / UTM zone 17N
summary(data.sp)
detach(package:rgdal)
data.sp2 <- spTransform(data.sp, CRS("+init=epsg:4326"))
## change projection in WGS84 lat lon
coord2 <- as.data.frame(data.sp2, stringsAsFactors = FALSE)
names(coord2) <- c( 'Lon', 'Lat', 'cluster')

## merge with bci
data.bci <-  merge(data.bci, coord2, by = 'cluster')


## ## plot on world map
## library(rworldmap, quietly = TRUE)
## newmap <- getMap(resolution = 'coarse')
## # different resolutions available
## plot(newmap, xlim = c(-85, -75), ylim = c(5, 15))
## points(data.sp2, cex = 0.2,
## col = c('red', 'blue')[1+as.numeric(data.sp2$plot %in% c('bci', 'sherman'))])

#### get wc climate 
source("R/utils/climate.R")
clim <- GetClimate(data.bci$Lat, data.bci$Lon)
data.bci$MAT <- clim$MAT
data.bci$MAP <- clim$MAP
data.bci[["ecocode"]] <- "tropical"

##  ADD BIOMES
### biomes from Whittaker
source("R/utils/plot.R")
biomes <- fun.overly.plot.on.biomes(MAP = data.bci$MAP/10,
                                        MAT = data.bci$MAT,
                          names.vec = 1:nrow(data.bci))
# plot
 library(BIOMEplot)
 plot_biome()
 points(data.bci$MAP/10, data.bci$MAT, col = 1 + unclass(is.na(biomes)))
# change factor
biomes <-  as.character(biomes$biomes)
data.bci$biomes <-  biomes

# koppen
source("R/utils/ecoregions.R")
data.bci$koppen <- GetKoppen(data.bci$Lon, data.bci$Lat)
data.bci$koppen <- as.character(data.bci$koppen)
data.bci$wwf <- GetEcoregions(data.bci$Lon, data.bci$Lat)
data.bci$wwf <- as.character(data.bci$wwf) 

###################### PLOT SELECTION FOR THE ANALYSIS
vec.basic.var <- c("obs.id", "tree.id", "sp", "sp.name", "cluster", "plot",
                   "ecocode", "koppen", "wwf", "D", "G","BA.G", "year", "dead",
                   'Lon', 'Lat', "x",  "y", "census", 'biomes', 'MAT', 'MAP')

data.tree <- subset(data.bci, select = c(vec.basic.var))
data.tree <- subset(data.tree,subset=!is.na(data.tree$x) & !is.na(data.tree$y))
## convert var factor in character or numeric
data.tree <- fun.convert.type.B(data.tree)
write.csv(data.tree, file = "output/formatted/BCI/tree.csv", row.names = FALSE)
  
### write data plot with variables only at the plot level. 
vec.basic.var.p <- c("plot", "cluster", "Lon", "Lat", "ecocode", "koppen", "wwf", "MAT", "MAP")
data.plot <- subset(data.tree, subset=!duplicated(data.tree$cluster),
                    select = c(vec.basic.var.p))
write.csv(data.plot, file = "output/formatted/BCI/plot.csv", row.names = FALSE)
