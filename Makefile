D1 := data/raw
D2 := output/formatted
D3 := output/processed
D4 := figs/test.format.tree
D5 := figs/test.traits
D6 := figs/test.processed
sites:= Fushan NSW Paracou BCI Mbaiki Luquillo Japan Spain Sweden Canada France Swiss NVS US 
D3Done :=  $(addsuffix /Done.no.txt,$(addprefix $(D3)/, $(sites) )) 
D3Doneg :=  $(addsuffix /Done.global.txt,$(addprefix $(D3)/, $(sites) )) 
D2traits :=  $(addsuffix /traits.csv,$(addprefix $(D2)/, $(sites) )) 
GD2tree :=  $(addsuffix /tree.csv,$(addprefix $(D2)/, $(sites) )) 


all: TRY sites

sites: $(sites)

#-------------------------------------------------------
include mk/process_BCI.mk
include mk/process_Fushan.mk
include mk/process_Paracou.mk
include mk/process_Mbaiki.mk
include mk/process_Luquillo.mk
include mk/process_Japan.mk
include mk/process_Canada.mk
include mk/process_Spain.mk
include mk/process_Sweden.mk
include mk/process_France.mk
include mk/process_Swiss.mk
include mk/process_NSW.mk
include mk/process_NVS.mk
include mk/process_US.mk

include mk/cible.tree.traits.mk

#-------------------------------------------------------

TRY: $(D2)/TRY/data.TRY.std.rds

$(D2)/TRY/data.TRY.std.rds:  
	Rscript scripts/format.data/TRY.R

#-------------------------------------------------------
GLOBAL: $(D3)/Done.txt TEST.CWM

$(D3)/Done.txt: scripts/process.data/remove.out.R scripts/process.data/summarise.data.R $(D3)/Done.t.txt
	Rscript $< 
	Rscript scripts/process.data/summarise.data.R
	Rscript scripts/process.data/plot.data.all.R; convert figs/test.processed/*.p* figs/test.processed/all.CWM.pdf

$(D3)/Done.t.txt: scripts/process.data/merge.all.processed.data.R 
	Rscript $<

#-------------------------------------------------------
TEST.TREE: $(D4)/Done.txt tree.all.sites

$(D4)/Done.txt:	scripts/format.data/test.tree.R $(D3tree) 
	Rscript $< ;  convert figs/test.format.tree/*.png figs/test.format.tree/all.tree.pdf

#-------------------------------------------------------
TEST.TRAITS: $(D5)/Done.txt traits.all.sites

$(D5)/Done.txt:	scripts/find.trait/test.traits.R $(D3traits)
	Rscript $< ; convert figs/test.traits/*.pdf figs/test.traits/all.traits.pdf

#-------------------------------------------------------
TEST.CWM: $(D6)/Done.txt sites TEST.TRAITS TEST.TREE scripts/process.data/test.tree.CWM.R

$(D6)/Done.txt:	scripts/process.data/test.tree.CWM.R
	Rscript $<  

#-------------------------------------------------------

# 

# This susbtitution rule should work as rule, but not, why not?
# docs/output/formatted/%/tree.csv: $(D1)/%/* %.R
# 	Rscript %.R   

# Is based on this which does work
# %-tree.csv: %/* %/*/* %.R
#         Rscript $*.R

# code to generate makefile contents
# s = dir("data/metadata/sites")
# cat((sprintf("%s: output/formatted/%s/traits.csv\n
# output/formatted/%s/traits.csv: output/formatted/%s/tree.csv R/find.trait/%s.R
# \tRscript R/find.trait/%s.R\n
# output/formatted/%s/tree.csv: $(shell find data/raw/%s -type f) %s.R
# \tRscript %s.R\n\n",s,s,s,s,s,s,s,s,s,s,s)), file="temp.txt")

