#!/bin/bash

mkdir -p Rscript_temp

for trait in "'SLA'" "'Wood.density'" "'Max.height'"; do

Rscript -e "source('R/analysis/lmer.run.R'); load.and.save.data.for.lmer($trait);print('done')"
# Rscript -e "source('R/analysis/lmer.run.R'); load.and.save.data.for.lmer($trait, data.type = 'all.census');print('done')"&
done

# Rscript -e "source('R/analysis/lmer.run.R'); load.and.save.data.for.lmer('SLA', data.type = 'Multi');print('done')"&
