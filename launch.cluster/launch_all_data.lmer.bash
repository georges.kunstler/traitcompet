#!/bin/bash

export LD_LIBRARY_PATH=/usr/lib64/R/library

mkdir -p Rscript_temp

for trait in "'SLA'" "'Wood.density'" "'Max.height'"; do

  	echo "/usr/local/R/R-3.1.1/bin/Rscript -e \"source('R/analysis/lmer.run.R'); load.and.save.data.for.lmer($trait);print('done')\"" > Rscript_temp/data1$trait.sh
	qsub Rscript_temp/data1$trait.sh -d ~/trait.competition.workshop -l nodes=1:ppn=1,mem=8gb -N "data1$trait" -q opt32G -j oe

  	echo "/usr/local/R/R-3.1.1/bin/Rscript -e \"source('R/analysis/lmer.run.R'); load.and.save.data.for.lmer($trait, data.type = 'intra');print('done')\"" > Rscript_temp/data1I$trait.sh
	qsub Rscript_temp/data1I$trait.sh -d ~/trait.competition.workshop -l nodes=1:ppn=1,mem=8gb -N "data1I$trait" -q opt32G -j oe

done


