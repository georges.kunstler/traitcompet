#!/bin/bash
# Georges Kunstler 14/05/2014
# read one variable
export LD_LIBRARY_PATH=/usr/lib64/R/library

mkdir -p Rscript_temp


# test parameter
nbargs=$#
echo "number of arguments="$nbargs
if [ $nbargs -ne 1 ]
then
  echo "need one and only one argument"
  echo " usage :"
  echo " ./launch_all_lmer.sh  sample.size"
  exit 100
fi


samplesize=$1

  	# echo "/usr/local/R/R-3.1.1/bin/Rscript -e \"source('R/analysis/lmer.run.R'); run.multiple.model.for.set.one.trait(c(model.files.lmer.Tf.Multi[1]), run.lmer,'Multi', data.type ='Multi');print('done')\"" > Rscript_temp/Multi1.sh
	# qsub Rscript_temp/Multi1.sh -d ~/trait.competition.workshop -l nodes=1:ppn=1,mem=8gb -N "Multi1" -q opt32G -j oe

 	echo "/usr/local/R/R-3.1.1/bin/Rscript -e \"source('R/analysis/lmer.run.R'); run.multiple.model.for.set.one.trait(c(model.files.lmer.Tf.Multi[2]), run.lmer,'Multi', data.type ='Multi');print('done')\"" > Rscript_temp/Multi2.sh
	qsub Rscript_temp/Multi2.sh -d ~/trait.competition.workshop -l nodes=1:ppn=1,mem=8gb -N "Multi2" -q opt32G -j oe

  	# echo "/usr/local/R/R-3.1.1/bin/Rscript -e \"source('R/analysis/lmer.run.R'); run.multiple.model.for.set.one.trait(c(model.files.lmer.Tf.Multi[3]), run.lmer,'Multi', data.type ='Multi');print('done')\"" > Rscript_temp/Multi3.sh
	# qsub Rscript_temp/Multi3.sh -d ~/trait.competition.workshop -l nodes=1:ppn=1,mem=8gb -N "Multi3" -q opt32G -j oe
