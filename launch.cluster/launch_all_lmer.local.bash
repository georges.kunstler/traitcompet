#!/bin/bash
# Georges Kunstler 14/05/2014
# read one variable

mkdir -p Rscript_temp


# test parameter
nbargs=$#
echo "number of arguments="$nbargs
if [ $nbargs -ne 1 ]
then
  echo "need one and only one argument"
  echo " usage :"
  echo " ./launch_all_lmer.local.sh  sample.size"
  exit 100
fi


samplesize=$1
for trait in  "'SLA'""'Max.height'" ; do


# ALL data
  	Rscript -e "source('R/analysis/lmer.run.R'); run.multiple.model.for.set.one.trait(model.files.lmer.Tf.1[1], run.lmer,$trait, sample.size =10, sample.vec.TF = TRUE) "


done


wait
