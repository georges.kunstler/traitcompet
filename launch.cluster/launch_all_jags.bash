#!/bin/bash
# Georges Kunstler 14/05/2014
# read one variable
export LD_LIBRARY_PATH=/usr/lib64/R/library

mkdir -p Rscript_temp

iter=6000
warmup=2000
thin=18
# test parameter
nbargs=$#
echo "number of arguments="$nbargs
if [ $nbargs -ne 1 ]
then
  echo "need one and only one argument"
  echo " usage :"
  echo " ./launch_all_lmer.sh  sample.size"
  exit 100
fi


samplesize=$1


for trait in "'SLA'" "'Wood.density'" "'Max.height'"; do

  	echo "/usr/local/R/R-3.1.1/bin/Rscript -e \"source('R/analysis/jags.run.R'); run.multiple.model.for.set.one.trait(model.files.jags.Tf.1[1], run.jags.b, trait = $trait,data.type='simple', sample.size = NA, var.sample = 'wwf',iter = $iter, warmup = $warmup, chains = 3, thin = $thin, init.TF = FALSE);print('done')\"" > Rscript_temp/speciesjags${trait}.sh
	qsub Rscript_temp/speciesjags${trait}.sh -d ~/trait.competition.workshop -l nodes=1:ppn=4,mem=16gb -N "jags${trait}" -q opt32G -j oe

  	echo "/usr/local/R/R-3.1.1/bin/Rscript -e \"source('R/analysis/jags.run.R'); run.multiple.model.for.set.one.trait(model.files.jags.Tf.1[2], run.jags.b, trait = $trait,data.type='simple', sample.size = NA, var.sample = 'wwf',iter = $iter, warmup = $warmup, chains = 3, thin = $thin, init.TF = FALSE, merge.biomes.TF = TRUE);print('done')\"" > Rscript_temp/speciesjags2${trait}.sh
	qsub Rscript_temp/speciesjags2${trait}.sh -d ~/trait.competition.workshop -l nodes=1:ppn=4,mem=16gb -N "jags2${trait}" -q opt32G -j oe

done
