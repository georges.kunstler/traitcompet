#!/bin/bash
# Georges Kunstler 14/05/2014
# read one variable
export LD_LIBRARY_PATH=/usr/lib64/R/library

mkdir -p Rscript_temp


# test parameter
nbargs=$#
echo "number of arguments="$nbargs
if [ $nbargs -ne 1 ]
then
  echo "need one and only one argument"
  echo " usage :"
  echo " ./launch_all_lmer.sh  sample.size"
  exit 100
fi


samplesize=$1
# "'Seed.mass'" "'Leaf.N'"
 for trait in "'Wood.density'" ; do


# # ALL data 1
  	echo "/usr/local/R/R-3.1.1/bin/Rscript -e \"source('R/analysis/lmer.run.R'); run.multiple.model.for.set.one.trait(model.files.lmer.Tf.1[1], run.lmer,$trait, sample.vec.TF = TRUE);print('done')\"" > Rscript_temp/allf${trait}.sh
	qsub Rscript_temp/allf${trait}.sh -d ~/trait.competition.workshop -l nodes=1:ppn=1,mem=8gb -N "lmerall2all.f${trait}" -q opt32G -j oe

  	# echo "/usr/local/R/R-3.1.1/bin/Rscript -e \"source('R/analysis/lmer.run.R'); run.multiple.model.for.set.one.trait(model.files.lmer.Tf.1[2], merge.biomes.TF = TRUE, run.lmer,$trait, sample.vec.TF = TRUE);print('done')\"" > Rscript_temp/allf2${trait}.sh
	# qsub Rscript_temp/allf2${trait}.sh -d ~/trait.competition.workshop -l nodes=1:ppn=1,mem=8gb -N "lmerall2all.f2${trait}" -q opt32G -j oe



done

