#!/bin/bash
# Georges Kunstler 14/05/2014
# read one variable
export LD_LIBRARY_PATH=/usr/lib64/R/library

mkdir -p Rscript_temp


# test parameter
nbargs=$#
echo "number of arguments="$nbargs
if [ $nbargs -ne 1 ]
then
  echo "need one and only one argument"
  echo " usage :"
  echo " ./launch_all_lmer.sh  sample.size"
  exit 100
fi


samplesize=$1
# "'Seed.mass'" "'Leaf.N'"
 for trait in "'SLA'" "'Wood.density'" "'Max.height'" ; do


# # INTRA 0
  	echo "/usr/local/R/R-3.1.1/bin/Rscript -e \"source('R/analysis/lmer.run.R'); run.multiple.model.for.set.one.trait(model.files.lmer.Tf.intra.0[1], run.lmer,$trait, data.type = 'intra');print('done')\"" > Rscript_temp/allINTRA0${trait}.sh
	qsub Rscript_temp/allINTRA0${trait}.sh -d ~/trait.competition.workshop -l nodes=1:ppn=1,mem=8gb -N "lmerall2all.INTRA0${trait}" -q opt32G -j oe


  	echo "/usr/local/R/R-3.1.1/bin/Rscript -e \"source('R/analysis/lmer.run.R'); run.multiple.model.for.set.one.trait(model.files.lmer.Tf.intra.0[2], merge.biomes.TF = TRUE, run.lmer,$trait,data.type = 'intra');print('done')\"" > Rscript_temp/allINTRA02${trait}.sh
	qsub Rscript_temp/allINTRA02${trait}.sh -d ~/trait.competition.workshop -l nodes=1:ppn=1,mem=8gb -N "lmerall2all.INTRA02${trait}" -q opt32G -j oe


# # INTRA 2
  	echo "/usr/local/R/R-3.1.1/bin/Rscript -e \"source('R/analysis/lmer.run.R'); run.multiple.model.for.set.one.trait(model.files.lmer.Tf.intra.2[1], run.lmer,$trait, data.type = 'intra');print('done')\"" > Rscript_temp/allINTRAB${trait}.sh
	qsub Rscript_temp/allINTRAB${trait}.sh -d ~/trait.competition.workshop -l nodes=1:ppn=1,mem=8gb -N "lmerall2all.INTRAB${trait}" -q opt32G -j oe


  	echo "/usr/local/R/R-3.1.1/bin/Rscript -e \"source('R/analysis/lmer.run.R'); run.multiple.model.for.set.one.trait(model.files.lmer.Tf.intra.2[2], merge.biomes.TF = TRUE, run.lmer,$trait,data.type = 'intra');print('done')\"" > Rscript_temp/allINTRAB2${trait}.sh
	qsub Rscript_temp/allINTRAB2${trait}.sh -d ~/trait.competition.workshop -l nodes=1:ppn=1,mem=8gb -N "lmerall2all.INTRAB2${trait}" -q opt32G -j oe

# # INTRA 3
  	echo "/usr/local/R/R-3.1.1/bin/Rscript -e \"source('R/analysis/lmer.run.R'); run.multiple.model.for.set.one.trait(model.files.lmer.Tf.intra.3[1], run.lmer,$trait, data.type = 'intra');print('done')\"" > Rscript_temp/allINTRAE${trait}.sh
	qsub Rscript_temp/allINTRAE${trait}.sh -d ~/trait.competition.workshop -l nodes=1:ppn=1,mem=8gb -N "lmerall2all.INTRAE${trait}" -q opt32G -j oe


  	echo "/usr/local/R/R-3.1.1/bin/Rscript -e \"source('R/analysis/lmer.run.R'); run.multiple.model.for.set.one.trait(model.files.lmer.Tf.intra.3[2], merge.biomes.TF = TRUE, run.lmer,$trait,data.type = 'intra');print('done')\"" > Rscript_temp/allINTRAE2${trait}.sh
	qsub Rscript_temp/allINTRAE2${trait}.sh -d ~/trait.competition.workshop -l nodes=1:ppn=1,mem=8gb -N "lmerall2all.INTRAE2${trait}" -q opt32G -j oe



done

