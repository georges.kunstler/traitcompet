
set.inv:  $(D3)/set.inv/Done.no.txt $(D3)/set.inv/Done.global.txt

$(D3)/set.inv/Done.no.txt: R/process.data/process-fun.R  $(D2)/set.inv/traits.csv
	Rscript -e "source('$<'); process_inventory_dataset('set.inv',std.traits='no');"

$(D3)/set.inv/Done.global.txt: R/process.data/process-fun.R  $(D2)/set.inv/traits.csv
	Rscript -e "source('$<'); process_inventory_dataset('set.inv',std.traits='global');"

$(D2)/set.inv/traits.csv: scripts/find.trait/set.inv.R R/find.trait/trait-fun.R $(D2)/set.inv/tree.csv $(D2)/TRY/data.TRY.std.rds
	Rscript $<

$(D2)/set.inv/tree.csv: scripts/format.data/set.inv.R R/format.data/format-fun.R $(shell find $(D1)/set.inv -type f)
	Rscript $<

