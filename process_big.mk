set.big:  $(D3)/set.big/Done.no.txt $(D3)/set.big/Done.global.txt

$(D3)/set.big/Done.no.txt: R/process.data/process-fun.R $(D2)/set.big/traits.csv
	Rscript -e "source('$<'); process_bigplot_dataset('set.big', Rlim=15,std.traits='no');"

$(D3)/set.big/Done.global.txt: R/process.data/process-fun.R $(D2)/set.big/traits.csv
	Rscript -e "source('$<'); process_bigplot_dataset('set.big', Rlim=15,std.traits='global');"

$(D2)/set.big/traits.csv: scripts/find.trait/set.big.R R/find.trait/trait-fun.R $(D2)/set.big/tree.csv
	Rscript $<

$(D2)/set.big/tree.csv: scripts/format.data/set.big.R R/format.data/format-fun.R $(shell find $(D1)/set.big -type f)
	Rscript $<
