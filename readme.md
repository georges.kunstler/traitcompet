# 'How are competitive interactions influenced by traits? A global analysis based on tree radial growth',

**Workshop at Macquarie University September 2013**

This repository contains material for an analysis based on following paper, for forest inventory data from around the world.

Kunstler *et al.* (2012) "Competitive interactions between forest trees are driven by species’ trait hierarchy, not phylogenetic or functional similarity: implications for forest community assembly". Ecology Letters, 15, 831–840. [DOI: 10.1111/j.1461-0248.2012.01803.x]( http://doi.org/10.1111/j.1461-0248.2012.01803.x) [PDF](http://www.wsl.ch/staff/niklaus.zimmermann/papers/EcolLett_Kunstler_2012.pdf).

## Folder structure

The repository has the following basic directories:

- `data`: contains raw data and meta data. Treat this as read-only, i.e. do not save output from R scripts here, these go in outputs.
- `R`: where all R code used in analysis is saved. The only exception is `*.Rmd` files used in creating some of the documents, which are stored in `docs` directory.
- `output`: a temporary directory that is used to hold all process data. Material in this directory is not tracked because it can be recreated from scripts.
- `docs`: contains documents developed for the project.

Some specific folders of likely interest:

- `docs/meeting.agenda`: agenda for workshop
- `docs/analysis.outline`: theoretical description of analysis
- `docs/analysis.pipeline`: outline of work flow (code, data etc).
- `data/raw`: Directory containing all the raw data from different sites. By default, this directory is not available.
- `docs/metadata`: Contains information about the dataset from each site
- `ms`: eventual home for manuscript materials

## Tools

We are making use of the following tools:

- [*R stats program*](http://www.r-project.org/), for all analyses
- A number R libraries. We have embedded a function in the code to load missing packages where possible (see `R/packages.R`)
- [*markdown*](http://daringfireball.net/projects/markdown/)
- The [*knitr*](http://cran.r-project.org/web/packages/knitr/index.html) package in R, for generating documents with R and markdown. For a n intro see [here](http://nicercode.github.io/guides/reports/)
- version control via git (see next section)
- JAGS - Just another Gibbs Sampler. Install [here](http://sourceforge.net/projects/mcmc-jags/files/)

### Git

Documents are kept under version control using git, and stored on github. To access:

1. Sign up to github and send your username to Georges so he can add you to the repository for this workshop, called `trait.competition.workshop`
2. Download repository via github software
	- [Github for mac](http://mac.github.com/)
	- [Github for windows](http://windows.github.com/)
3. You also view the material online [here](https://github.com/kunstler/trait.competition.workshop)

For an intro to git, see [http://nicercode.github.io/git/](http://nicercode.github.io/git/)

Once downloaded, you can interact with the material via your github software, the shell, or [Rstudio](http://www.rstudio.com/) - an R editor with built in git capabilities.

## Key points for intro

- Internet access
- Library access
- Dropbox folder for temporary terms.
- Intro to git and github
	- version control
		- problems with dropbox: multiple versions, see what changed, conflicts, instant (feature branches)
	- text files
	- set up:
		- install github, rstudio
- R project
	- Ideal: run on anyone's computer, never use setwd()
	- break into parts
	- knitr
	- makefiles
- data
	- what are we willing to share

